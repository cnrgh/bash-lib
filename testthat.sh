# shellcheck disable=SC1091
source "$(dirname "${BASH_SOURCE[0]}")/deprecate.sh"
dp_deprecation_warning "Module testthat.sh is deprecated. You must include"\
    "now test.sh instead. Note that function names have not changed." \
    "No code update is required apart changing module name to include."
source "$(dirname "${BASH_SOURCE[0]}")/test.sh"
