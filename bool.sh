# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is BO (BOolean):
#  * `bo_`  for the public functions.
#  * `BO_`  for the public global variables or constants.
#  * `_bo_` for the private functions.
#  * `_BO_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_BO_SOURCED ]] || return 0
    _BO_SOURCED=1
fi

# shellcheck disable=SC2034
BO_TRUE=1
# shellcheck disable=SC2034
BO_FALSE=0

function bo_is_true {

    local -l val="$1"
    local res=1 # False

    case $val in 
        
        # Keyword
        true|on|yes) res=0 ;;
        false|off|no) res=1 ;;

        # Number
        *)
            if [[ $val =~ ^-?[0-9]+$ ]] ; then
                if [[ $val -ne 0 ]] ; then
                    res=0
                else
                    res=1
                fi
            elif [[ -n $val ]] ; then
                res=0
            else
                res=1
            fi ;;
    esac

    return $res
}

function bo_is_false {
    ! bo_is_true "$@"
}

function bo_is_on {
    bo_is_true "$@"
}

function bo_is_off {
    bo_is_false "$@"
}

function bo_not {

    local v="$1"
    local ret=

    # Empty string in input == false
    if [[ -z $v ]] ; then
        ret="true"

    else
        # Convert keeping case
        case "$v" in
            true)  ret="false" ;;
            True)  ret="False" ;;
            TRUE)  ret="FALSE" ;;
            false) ret="true" ;;
            False) ret="True" ;;
            FALSE) ret="TRUE" ;;
            on)    ret="off" ;;
            On)    ret="Off" ;;
            ON)    ret="OFF" ;;
            off)   ret="on" ;;
            Off)   ret="On" ;;
            OFF)   ret="ON" ;;
            no)    ret="yes" ;;
            No)    ret="Yes" ;;
            NO)    ret="YES" ;;
            yes)   ret="no" ;;
            Yes)   ret="No" ;;
            YES)   ret="NO" ;;
        esac

        # Convert in case-insensitive mode
        if [[ -z $ret ]] ; then
            local -l w="$v"

            case "$w" in

                true) ret="false" ;;
                false) ret="true" ;;
                off) ret="on" ;;
                on) ret="off" ;;
                yes) ret="no" ;;
                no) ret="yes" ;;

                # Number or string
                *)
                    if [[ $w =~ ^-?[0-9]+$ ]] ; then
                        if [[ $w -ne 0 ]] ; then
                            ret=0
                        else
                            ret=1
                        fi
                    elif [[ -n $w ]] ; then
                        ret=""
                    else
                        ret="X"
                    fi ;;
            esac
        fi
    fi

    echo "$ret"
    return 0
}

function bo_and {

    if bo_all_true "$@" ; then
        echo "$1"
    else
        bo_not "$1"
    fi

    return 0
}

function bo_or {

    if bo_any_true "$@" ; then
        if bo_is_true "$1" ; then
            echo "$1"
        else
            bo_not "$1"
        fi
    else
        if bo_is_true "$1" ; then
            bo_not "$1"
        else
            echo "$1"
        fi
    fi

    return 0
}

function bo_xor {

    if bo_all_true "$@" || bo_all_false "$@" ; then
        if bo_is_true "$1" ; then
            bo_not "$1"
        else
            echo "$1"
        fi
    else
        if bo_is_true "$1" ; then
            echo "$1"
        else
            bo_not "$1"
        fi
    fi

    return 0
}

function bo_all_false {
    
    local res=1 # False
    
    for v in "$@" ; do
        if bo_is_true "$v" ; then
            res=1 # False
            break
        else
            res=0 # True
        fi
    done

    return $res
}

function bo_all_true {
    
    local res=1 # False
    local v
    
    for v in "$@" ; do
        if bo_is_false "$v" ; then
            res=1 # False
            break
        else
            res=0 # True
        fi
    done

    return $res
}

function bo_any_false {
    
    local res=1 # False
    
    for v in "$@" ; do
        if bo_is_false "$v" ; then
            res=0 # True
            break
        fi
    done

    return $res
}

function bo_any_true {
    
    local res=1 # False
    
    for v in "$@" ; do
        if bo_is_true "$v" ; then
            res=0 # True
            break
        fi
    done

    return $res
}

function bo_as_ucfirst_bool {

    local -r b="$1"

    if bo_is_true "$b" ; then
        echo -n "True"
    else
        echo -n "False"
    fi

    return 0
}

function bo_as_lc_bool {

    local -l b
    b="$(bo_as_ucfirst_bool "$1")" || lg_fatal "Cannot convert \"$1\"."
    echo "$b"

    return 0
}

function bo_as_uc_bool {

    local -u b
    b="$(bo_as_ucfirst_bool "$1")" || lg_fatal "Cannot convert \"$1\"."
    echo "$b"

    return 0
}

function bo_as_ucfirst_onoff {

    local -r b="$1"

    if bo_is_true "$b" ; then
        echo -n "On"
    else
        echo -n "Off"
    fi

    return 0
}

function bo_as_lc_onoff {

    local -l b
    b="$(bo_as_ucfirst_onoff "$1")" || lg_fatal "Cannot convert \"$1\"."
    echo "$b"

    return 0
}

function bo_as_uc_onoff {

    local -u b
    b="$(bo_as_ucfirst_onoff "$1")" || lg_fatal "Cannot convert \"$1\"."
    echo "$b"

    return 0
}

function bo_as_ucfirst_yesno {

    local -r b="$1"

    if bo_is_true "$b" ; then
        echo -n "Yes"
    else
        echo -n "No"
    fi

    return 0
}

function bo_as_lc_yesno {

    local -l b
    b="$(bo_as_ucfirst_yesno "$1")" || lg_fatal "Cannot convert \"$1\"."
    echo "$b"

    return 0
}

function bo_as_uc_yesno {

    local -u b
    b="$(bo_as_ucfirst_yesno "$1")" || lg_fatal "Cannot convert \"$1\"."
    echo "$b"

    return 0
}

function bo_as_str {

    local -r b="$1"

    if bo_is_true "$b" ; then
        echo -n "1"
    else
        echo -n ""
    fi

    return 0
}

function bo_as_bit {

    local -r b="$1"

    if bo_is_true "$b" ; then
        echo -n "1"
    else
        echo -n "0"
    fi

    return 0
}
