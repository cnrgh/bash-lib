# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is RT (TestThat):
#  * `rt_`  for the public functions.
#  * `RT_`  for the public global variables or constants.
#  * `_rt_` for the private functions.
#  * `_RT_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_RT_SOURCED ]] || return 0
    declare _RT_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/array.sh"
source "$(dirname "${BASH_SOURCE[0]}")/fs.sh"
source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"
source "$(dirname "${BASH_SOURCE[0]}")/ver.sh"
vr_bash_ge 4.4.0 || lg_warning "Module runtime.sh does not work properly" \
    "for versions of bash lower than 4.4.0."

if [[ -z $RT_SC_SIGINT ]] ; then
    declare -r RT_SC_SIGINT="$((0x82))"
    declare -r RT_SC_SIGQUIT="$((0x83))"
    declare -r RT_SC_SIGTERM="$((0x8f))"
    declare -r _RT_PRE_CMD_END="BASHLIB_TRAP_PRE_CMD_END"
    declare -r _RT_POST_CMD_START="BASHLIB_TRAP_POST_CMD_START"
    declare -r _RT_SEMICOLON=";"
fi

function rt_robustify {
    set -Ceu -o pipefail
    rt_enable_err_trap
    return 0
}

function rt_enable_err_trap {
    trap '_rt_stop_on_err "${BASH_SOURCE[0]}" "${LINENO}" "$?"' ERR
    set -o errtrace
    return 0
}

function rt_disable_err_trap {
    trap - ERR
    return 0
}

function _rt_stop_on_err {
  local -r  file="$1"
  local -ri line="${2}"
  local -ri status="${3:-1}"
  echo "Error at $file:$line with status code $status" >&2
  exit "${exit_code}"
}

function rt_print_call_stack {

    declare frame=0
    while caller $frame >&2 ; do
        ((frame++));
    done

    return 0
}

function _rt_exit_trap_done {
    # On EXIT, quit with right exit code
    declare pid="${1:-$$}"
    declare rc="${2:-0}"
    trap - SIGINT SIGTERM EXIT
    case "$rc" in
      130) kill -s INT  "$pid" ;;
      131) kill -s QUIT "$pid" ;;
      143) kill -s TERM "$pid" ;;
    esac
    exit "$rc"
}

function rt_get_trap_cmd {

    declare signal="$1"

    declare cmd
    cmd="$(trap -p "$signal")" || return 1
    if [[ $cmd =~ ^trap\ --\ \'(.*)\'\ (SIG)?$signal$ ]] ; then
        cmd="${BASH_REMATCH[1]}"
    fi
    cmd="${cmd//\'\\\'\'/\'}"
    echo -n "$cmd"

    return 0
}

function _rt_add_trap_cmd {

    declare -r new_cmd="$1"
    declare -ur signal="$2"
    declare -ir insert="${3:-0}" # Default is to append
    declare -ir safe="${4:0}"
    declare current_cmd
    declare whole_cmd
    declare pre_cmd=
    declare post_cmd=

    # Get current trap command for this signal
    current_cmd="$(rt_get_trap_cmd "$signal")" || return 1

    # Does new command already defined inside current command?
    [[ $safe -ne 0 && -n $new_cmd && $current_cmd =~ (^|.*;)$new_cmd(;.*|$) ]] \
        && return 0

    # Append as last command
    if [[ $insert -eq 0 ]] ; then

        # Check post-command
        if [[ $current_cmd =~ \
            (^.*)$_RT_SEMICOLON([(][(]${_RT_POST_CMD_START}[)][)].*$) ]] ; then
            current_cmd="${BASH_REMATCH[1]}"
            post_cmd="${BASH_REMATCH[2]}"
        fi

        # Recompose commands
        whole_cmd="${current_cmd:+$current_cmd;}$new_cmd${post_cmd:+;$post_cmd}"

    # Insert as first command
    else

        # Check pre-commands
        pre_cmd=
        if [[ $current_cmd =~ \
            (^.*[(][(]${_RT_PRE_CMD_END}[)][)])$_RT_SEMICOLON(.*$) ]] ; then
            pre_cmd="${BASH_REMATCH[1]}"
            current_cmd="${BASH_REMATCH[2]}"
        fi

        # Recompose commands
        whole_cmd="${pre_cmd:+$pre_cmd;}$new_cmd${current_cmd:+;$current_cmd}"
    fi

    # Set safeguard pre-commamds and post-commands
    if [[ $safe -ne 0 && ! $whole_cmd =~ .*[(][(]${_RT_PRE_CMD_END}[)][)].* ]] \
        ; then
        pre_cmd=
        post_cmd=
        case $signal in

            SIGINT|INT)
                pre_cmd="trap - INT" # Avoid multiple triggers of CTRL-C
                post_cmd="exit $RT_SC_SIGINT" ;;

            SIGTERM|TERM) exit_code="$RT_SC_SIGTERM" ;;&
            SIGQUIT|QUIT) exit_code="$RT_SC_SIGQUIT" ;;&
            SIGTERM|TERM|SIGQUIT|QUIT)
                # Redirect to EXIT
                if [[ -n "$(trap -p EXIT)" ]] ; then
                    post_cmd="exit $exit_code"

                # Propagate the signal
                else
                    post_cmd="kill -s $signal \$\$"
                fi
                ;;

            EXIT)
                pre_cmd="rc=\$?" # Save original exit code
                post_cmd="_rt_exit_trap_done \$\$ \$rc" ;;
        esac
        pre_cmd="${pre_cmd:+$pre_cmd;}set +ex"
        whole_cmd="$pre_cmd;(($_RT_PRE_CMD_END))${whole_cmd:+;$whole_cmd}"
        [[ -z $post_cmd ]] || whole_cmd+=";(($_RT_POST_CMD_START));$post_cmd"
    fi

    # Set whole command
    # shellcheck disable=SC2064
    trap "$whole_cmd" "$signal" || return 1
    lg_debug 1 "Trapping signal $signal with command \"$whole_cmd\"."

    return 0
}

function rt_append_trap_cmd {

    declare -r new_cmd="$1"
    shift

    if [[ -n $new_cmd ]] ; then

        # Loop on all signals
        for signal in "$@" ; do
            _rt_add_trap_cmd "$new_cmd" "$signal"
        done
    fi

    return 0
}

function rt_safe_append_trap_cmd {

    declare -r new_cmd="$1"
    shift
    declare -au signals=("$@")
    declare -au extra_signals=()
    declare sig

    if [[ -n $new_cmd ]] ; then

        # EXIT is defined
        if ar_contains EXIT "${signals[@]}" ; then

            # Make sure EXIT is defined first
            signals=("$(ar_move_first EXIT "${signals[@]}")")

            # Add missing signals if they are not already defined.
            for sig in SIGINT SIGTERM SIGQUIT ; do
                [[ -z $(rt_get_trap_cmd "$sig") ]] \
                    && ! ar_contains "$sig" "${signals[@]}" \
                    && extra_signals+=("$sig")
            done
        fi

        # Loop on all signals
        for signal in "${signals[@]}" ; do
            _rt_add_trap_cmd "$new_cmd" "$signal" 0 1
        done

        # Loop on all extra signals
        for signal in "${extra_signals[@]}" ; do
            _rt_add_trap_cmd "" "$signal" 0 1
        done
    fi

    return 0
}

function rt_insert_trap_cmd {

    declare -r new_cmd="$1"
    shift

    if [[ -n $new_cmd ]] ; then

        # Loop on all signals
        for signal in "$@" ; do
            _rt_add_trap_cmd "$new_cmd" "$signal" 1
        done
    fi

    return 0
}

true
