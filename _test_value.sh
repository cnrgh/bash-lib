# vi: ft=bash
## @title Value assertions
##
## Assertions testing values or comparing values.

## Tests if a string is empty.
##
## Checks that the passed value is an empty string.
##
## @arg v str A string.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_str_null {

    local v=$1
    local msg="$2"

    if [[ -n $v ]] ; then
        rt_print_call_stack >&2
        echo "String \"$v\" is not null ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a string is not empty.
##
## Checks that the passed value is not an empty string.
##
## @arg v str A string.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_str_not_null {

    local v=$1
    local msg="$2"

    if [[ -z $v ]] ; then
        rt_print_call_stack >&2
        echo "String is null ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a string has a specific length.
##
## Checks that the passed value is a string of `len` characters.
##
## @arg v str A string.
## @arg len int The expected length of the string.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_str_len {
    # Test length of a string

    local s="$1"
    local len="$2"
    shift 2
    local msg="$*"

    if [[ ${#s} -ne $len ]] ; then
        rt_print_call_stack >&2
        echo "Length of string \"$s\" is not $len ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if two strings are equal.
##
## Checks that the passed strings are the same.
##
## @arg a str A string.
## @arg b str Another string.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_str_eq {

    local a=$1
    local b=$2
    local msg="$3"

    if [[ $a != "$b" ]] ; then
        rt_print_call_stack >&2
        echo "\"$a\" == \"$b\" not true ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if two strings are different.
##
## Checks that the passed strings are different.
##
## @arg a str A string.
## @arg b str Another string.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_str_ne {

    local a=$1
    local b=$2
    local msg="$3"

    if [[ $a == "$b" ]] ; then
        rt_print_call_stack >&2
        echo "\"$a\" != \"$b\" not true ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a string matches a regular expression.
##
## Checks that a string matches a provided regular expression.
##
## @arg s str A string.
## @arg re str An extended regular expression.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_str_re {

    local str="$1"
    local re="$2"
    local msg="$3"

    local s
    s=$(grep -E "$re" <<<"$str") || lg_fatal "Failed running grep."
    if [[ -z $s ]] ; then
        rt_print_call_stack >&2
        echo "\"$str\" not matched by regular expression \"$re\" ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that two integer values are equal.
##
## Checks that two integer values are equal.
##
## @arg a int An integer.
## @arg b int Another integer.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_num_eq {

    local -r a="$1"
    local -r b="$2"
    local -r msg="$3"

    if [[ ! $a -eq $b ]] ; then
        rt_print_call_stack >&2
        echo "\"$a == $b\" not true ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that two integer values are different.
##
## Checks that two integer values are different.
##
## @arg a int An integer.
## @arg b int Another integer.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_num_ne {

    local -r a=$1
    local -r b=$2
    local -r msg="$3"

    if [[ ! $a -ne $b ]] ; then
        rt_print_call_stack >&2
        echo "\"$a != $b\" not true ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that an integer value is lesser than another.
##
## Compares two integer values and returns 0 if the first one is lesser than
## or equal to the other one.
##
## @arg a int An integer.
## @arg b int Another integer.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_num_le {

    local -r a=$1
    local -r b=$2
    local -r msg="$3"

    if [[ ! $a -le $b ]] ; then
        rt_print_call_stack >&2
        echo "\"$a <= $b\" not true ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that an integer value is lesser than another.
##
## Compares two integer values and returns 0 if the first one is lesser than
## the other one.
##
## @arg a int An integer.
## @arg b int Another integer.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_num_lt {

    local -r a=$1
    local -r b=$2
    local -r msg="$3"

    if [[ ! $a -lt $b ]] ; then
        rt_print_call_stack >&2
        echo "\"$a < $b\" not true ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that an integer value is greater than another.
##
## Compares two integer values and returns 0 if the first one is greater than
## the other one.
##
## @arg a int An integer.
## @arg b int Another integer.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_num_gt {

    local -r a=$1
    local -r b=$2
    local -r msg="$3"

    if [[ ! $a -gt $b ]] ; then
        rt_print_call_stack >&2
        echo "\"$a > $b\" not true ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that an integer value is greater than another.
##
## Compares two integer values and returns 0 if the first one is greater than
## or equal to the other one.
##
## @arg a int An integer.
## @arg b int Another integer.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_num_ge {

    local -r a=$1
    local -r b=$2
    local -r msg="$3"

    if [[ ! $a -ge $b ]] ; then
        rt_print_call_stack >&2
        echo "\"$a >= $b\" not true ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that an environment variable is defined.
##
## Checks that an environment variable is defined (i.e.: not empty).
##
## @arg v str The name of the environment variable.
## @arg msg str A message to print on failure.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_def_env_var {

    local varname="$1"
    local msg="$2"

    if [[ -z "${!varname}" ]] ; then
        rt_print_call_stack >&2
        echo "Env var $varname is not defined or is empty ! $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}
