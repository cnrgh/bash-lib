# vi: ft=bash
# shellcheck disable=SC1091
## @title Test module
##
## Test framework for testing command line programs (not only bash scripts) and
## bash functions.
##
## The name/prefix of this module is TT (TesT):
##  * `tt_`  for the public functions.
##  * `TT_`  for the public global variables or constants.
##  * `_tt_` for the private functions.
##  * `_TT_` for the private global variables or constants.
##
## ## runtests script
##
## The `runtests` script provided with `bash-lib` facilitates the use of the
## `test.sh` module to test your command line program.
##
## Put all your test scripts inside a `tests` folder, naming them
## `test-<some_text>.sh`, and simply run `runtests tests`.
##
## You may also run `runtests` on individual files with
## `runtests tests/test-myfile.sh`.
##
## See `runtests` help page: `runtests -h`.
##
## ## Context
##
## Each test script calls the `tt_context`
## function to set the context.
## The context is only a text message that will be printed 
## and then each method inside the test script that start with `test_`
## will be executed by `test.sh`.
## ```
## tt_context "Data reading tests"
## ```
## During tests execution the text of the context will be printed on the screen,
## followed by dots representing succeeded assertions.
##
## ## Assertions
##
## In each test method, you use `tt_expect_*` functions (assertions)
## to test values,
## outputs, status, files, etc. See below for a full list of `tt_expect_*`
## functions.
## When using an assertion, you need to test for assertion failure explicitly
## and return from your function with an error code. See example below:
## ```
## function test_true {
##   tt_expect_success true || return 1
## }
## ```
## Do not forget to append ` || return 1` to the assertion call, otherwise no
## error will be reported in case of failure.
##
## Categories of assertions:
##  * [Test command execution](_test_execution.sh).
##  * [Test command output](_test_output.sh).
##  * [Test CSV file](_test_csv_file.sh).
##  * [Test file content](_test_file_content.sh).
##  * [Test a value](_test_value.sh).
##  * [Test file system](_test_fs.sh).
##
## ## Writing a test script
##
## When running `runtests` on a folder, you must name your scripts according
## to a pattern in order for `runtests` to select them.
## The default pattern is `[Tt][Ee][Ss][Tt][-._].*\.sh` and can be modified
## using the `-f` option.
##
## Inside the file, a good practice is to define a context in order to split
## your tests in different groups using the `tt_context` function.
##
## Then you write your test function, naming them with the function prefix
## (default is `[Tt][Ee][Ss][Tt][-_.]?`). You can change the prefix with the
## `-x` option.
##
## Inside your test function, you use assertion functions (`tt_expect_*`
## functions) to control values, files, commands, etc.
##
## As an example, you can create a test file named `test_001.sh` with the
## following content:
## ```
## tt_context "Running some test for an example"
##
## function test_01_my_test {
##     tt_expect_num_eq 1 2 || return 1
## }
## ```
##
## When running, each assertion function will print either a dot (`.`) character
## in case of success or a letter in case of error.
## A report will then print the full stderr content.
##
## ## Manual execution of tests
##
## As we have seen, the standard way to use the test framework is to let it
## search for test files dans test functions and let it execute them.
##
## Another approach is to run explicitly each test function yourself.
## Then you call `tt_test_that` for each test function you have written:
## ```
## function my_test {
##     tt_expect_num_eq 1 2 || return 1
## }
## 
## tt_context "Running some test for an example"
## tt_test_that "Some stuff is running correctly." my_test
## ```

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_TT_SOURCED ]] || return 0
    _TT_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/array.sh"
source "$(dirname "${BASH_SOURCE[0]}")/deprecate.sh"
source "$(dirname "${BASH_SOURCE[0]}")/fs.sh"
source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"
source "$(dirname "${BASH_SOURCE[0]}")/runtime.sh"
source "$(dirname "${BASH_SOURCE[0]}")/textfile.sh"
source "$(dirname "${BASH_SOURCE[0]}")/_test_execution.sh"
source "$(dirname "${BASH_SOURCE[0]}")/_test_csv_file.sh"
source "$(dirname "${BASH_SOURCE[0]}")/_test_file_content.sh"
source "$(dirname "${BASH_SOURCE[0]}")/_test_fs.sh"
source "$(dirname "${BASH_SOURCE[0]}")/_test_output.sh"
source "$(dirname "${BASH_SOURCE[0]}")/_test_value.sh"

TT_REPORT_ON_THE_SPOT=on.the.spot
TT_REPORT_AT_THE_END=at.the.end
_TT_REPORTER=$TT_REPORT_AT_THE_END
_TT_DEFAULT_FILE_PATTERN='[Tt][Ee][Ss][Tt][-._].*\.sh'
_TT_DEFAULT_FCT_PREFIX='[Tt][Ee][Ss][Tt][-_.]?'
_TT_LIVE_PRINTING=
_TT_QUIT_ON_FIRST_ERROR=
declare -i _TT_DRYRUN=0
declare -gi _TT_ERR_NUMBER=0

declare -a _TT_ERR_MSGS=()
declare -a _TT_ERR_STDERR_FILES=()
declare -a _TT_FCTS_RUN_IN_TEST_FILE=()
declare -a _TT_CONTEXTS=()

## Gets the default file pattern.
##
## This pattern is used to filter files that contain test.
##
## @stdout The default file pattern.
## @status Returns 0.
function tt_get_default_file_pattern {
    echo "$_TT_DEFAULT_FILE_PATTERN"
    return 0
}

## Enable dry-run mode.
##
## Print what test file would be run, but do not source them.
##
## @status Returns 0.
function tt_enable_dryrun {
    _TT_DRYRUN=1
    return 0
}

## Gets the default function prefix.
##
## This pattern is used to filter test functions inside test script.
##
## @stdout The default function prefix.
## @status Returns 0.
function tt_get_default_fct_prefix {
    echo "$_TT_DEFAULT_FCT_PREFIX"
    return 0
}

## Gets the current reporter.
##
## @stdout The reporter currently configured.
## @status Returns 0.
function tt_get_reporter {
    echo "$_TT_REPORTER"
    return 0
}

## Enables live printing of output.
##
## By default the module hides all outputs (stdout and stderr) of tests during
## execution.
## This configuration function enables all outputs during tests.
##
## @status Returns 0.
function tt_enable_live_printing {
    _TT_LIVE_PRINTING=1
    return 0
}

## Enables quiting of first error.
##
## When this option is enabled the module quits at the first error. No other
## tests are exexcuted.
##
## @status Returns 0.
function tt_enable_quit_on_first_error {
    _TT_QUIT_ON_FIRST_ERROR=1
    return 0
}

## Sets the reporter to use.
##
## Accepted reporters are: `$TT_REPORT_ON_THE_SPOT` and `$TT_REPORT_AT_THE_END`.
##
## @arg reporter str [$TT_REPORT_ON_THE_SPOT, $TT_REPORT_AT_THE_END] The
## reporter to set.
## @status Returns 0 if reporter is accepted, and non-zero if the given reporter
## is not recognized..
function tt_set_reporter {

    local reporter="$1"

    ar_contains "$reporter" "$TT_REPORT_ON_THE_SPOT" "$TT_REPORT_AT_THE_END" ||
        lg_fatal "The reporter must be one of $TT_REPORT_ON_THE_SPOT or"\
        "$TT_REPORT_AT_THE_END."

    _TT_REPORTER="$reporter"

    return 0
}

## Sets the context.
##
## Sets the context for the next tests.
## The context is only a text message/title that will be printed for information
## during the tests execution. It allows to gather multiple tests under the same
## title.
##
## @arg msg str The message to print.
## @status Returns 0.
function tt_context {

    local msg="$1"

    # Is this a new context?
    [[ "${_TT_CONTEXTS[*]}" == "$msg" ]] && return 0

    # Print new line if this is not first context
    [[ ${#_TT_CONTEXTS[@]} -gt 0 ]] && echo

    # Print context
    echo -n "$msg "

    # Store context
    _TT_CONTEXTS+=("$msg")

    return 0
}

function _tt_print_error {
    n=$1
    msg="$2"
    output_file="$3"

    echo
    echo '----------------------------------------------------------------'
    printf "%x. " "$n"
    echo "Failure while asserting that \"$msg\"."
    echo '---'
    if [[ -f $output_file ]] ; then
        cat "$output_file"
        rm "$output_file"
    fi
    echo '----------------------------------------------------------------'

    return 0
}

## Finalizes the tests.
##
## This function must be called at the end of test.
## If the selected reporter was `$TT_REPORT_AT_THE_END`, the function will print
## the report.
##
## @stdout The tests report, if the reporter was `$TT_REPORT_AT_THE_END`.
## @status 0 if no errors were detected during the tests, non-zero otherwise.
function tt_finalize_tests {

    # Print new line
    [[ ${#_TT_CONTEXTS[@]} -eq 0 ]] || echo

    # Print end report
    [[ $_TT_REPORTER == "$TT_REPORT_AT_THE_END" ]] && _tt_print_end_report

    # Exit
    return "$_TT_ERR_NUMBER"
}

function _tt_fct_is_allowed {

    local fct="$1" # Function
    local inc_fcts="$2" # Functions to include
    local exc_fcts="$3" # Functions to exclude

    [[ -n $inc_fcts && ",$inc_fcts," != *",$test_fct,"* ]] && return 1
    [[ -n $exc_fcts && ",$exc_fcts," == *",$test_fct,"* ]] && return 1

    return 0
}

## Executes a test function.
##
## This function is automatically called by `tt_run_test_file`, but may be
## called directly.
## It executes a particular test function with a set of arguments.
##
## @arg msg      str A message to print on stderr if the function failed.
## @arg test_fct str The test function to run.
## @arg inc_fcts str A comma separated list of test functions that are allowed
## to run. If the list is not empty, then the function will be run only if it is
## in this list.
## @arg exc_fcts str A comma separated list of test functions that are not
## allowed to run. If the list is not empty, then the function will be run only
## if it is not in this list.
## @arg params   str ... The arguments to pass to the test function.
## @stdout Prints a dot (`.`) without newline character if the test succeeded,
## otherwise prints a lowercase hexadecimal character or the letter E if no more
## characters are available.
## @stderr Prints error messages if the selected reporter is
## `$_TT_LIVE_PRINTING`.
function tt_run_test_fct {

    declare msg="$1"
    declare test_fct="$2"
    declare inc_fcts="$3" # Functions to include
    declare exc_fcts="$4" # Functions to exclude
    shift 4
    declare params="$*"
    declare tmp_stderr_file
    tmp_stderr_file="$(mktemp -t bashlib-test-stderr.XXXXXX)" || \
        lg_fatal "Failed creating temporary file."

    # Set default values
    [[ -n $msg ]] || msg="Tests pass in function $test_fct"

    # Filtering
    _tt_fct_is_allowed "$test_fct" "$inc_fcts" "$exc_fcts" || return 0

    # Checking
    declare fct_type
    fct_type=$(type -t "$test_fct") || lg_fatal "Failed getting object type."
    [[ $fct_type == function || $fct_type == file ]] || lg_fatal \
        "\"$test_fct\" is neither a function nor an external command."

    # Run test
    _TT_FCTS_RUN_IN_TEST_FILE+=("$test_fct")
    lg_debug 1 "Running test function \"$test_fct\" and redirecting error"\
        "messages to \"$tmp_stderr_file\"."
    ( "$test_fct" "$params" 2>"$tmp_stderr_file" ) # Run in a subshell to catch
                                               # exit interruptions.
    exit_code=$?
    lg_debug 1 "Exit code of \"$test_fct\" was $exit_code."

    # Print stderr now
    [[ -n $_TT_LIVE_PRINTING && -f "$tmp_stderr_file" ]] && \
        cat "$tmp_stderr_file"

    # Failure
    if [[ "$exit_code" -gt 0 ]] ; then

        # Increment error number
        ((++_TT_ERR_NUMBER))

        # Print error number
        if [[ _TT_ERR_NUMBER -lt 16 ]] ; then
            printf %x "$_TT_ERR_NUMBER"
        else
            echo -n E
        fi

        # Print error now
        if [[ $_TT_REPORTER == "$TT_REPORT_ON_THE_SPOT" ]] ; then
            _tt_print_error "$_TT_ERR_NUMBER" "$msg" "$tmp_stderr_file"

        # Store error message for later
        else
            _TT_ERR_MSGS+=("$msg")
            _TT_ERR_STDERR_FILES+=("$tmp_stderr_file")
        fi

        # Quit on first error
        [[ -z $_TT_QUIT_ON_FIRST_ERROR ]] || tt_finalize_tests || exit $?

    # Success
    else
        rm "$tmp_stderr_file"
    fi

    return 0
}

## Executes tests inside a test script.
##
## @arg file str The path to the test script to run.
## @arg autorun str If not empty, searches automatically for test functions
## inside the file and executes them.
## @arg prefix str The prefix with which test functions start.
## @arg inc_fcts str A comma separated list of test functions that are allowed
## to run. If the list is not empty, then functions will be run only if they are
## in this list.
## @arg exc_fcts str A comma separated list of test functions that are not
## allowed to run. If the list is not empty, then functions will be run only
## if they are not in this list.
## @stdout Prints a dot (`.`) without newline character for each test passed,
## and a lowercase hexadecimal character or the letter E for each test failed.
## @stderr Prints error messages if the selected reporter is
## `$_TT_LIVE_PRINTING`.
function tt_run_test_file {

    local file="$1"
    local autorun="$2"
    local prefix="$3"
    local inc_fcts="$4"
    local exc_fcts="$5"

    [[ _TT_DRYRUN -gt 0 ]] && lg_info "Would run tests inside \"$file\"." \
        && return 0

    # Set default values
    [[ -n $autorun ]] || autorun=1
    [[ -n $prefix ]] || prefix="$_TT_DEFAULT_FCT_PREFIX"

    lg_debug 1 "Running tests in file \"$file\"."

    _TT_FCTS_RUN_IN_TEST_FILE=()
    # shellcheck disable=SC1090
    source "$file"

    # Run all test_.* functions not run explicitly by test_that
    if [[ -n $autorun ]] ; then

        # Get functions to run
        local -a fcts
        read -ra fcts <<<"$( \
            grep -E '^ *(function +'"$prefix"'[^ ]+|'"$prefix"'[^ ]+\(\)) *\{' \
            "$file" | \
            sed -E 's/^ *(function +)?('"$prefix"'[^ {(]+).*$/\2/' | \
            tr "\n" " ")"

        # Loop on all functions
        for fct in "${fcts[@]}" ; do

            lg_debug 1 "Found test function \"$fct\"."

            # Ignore some reserved names
            [[ $fct == tt_test_context || $fct == tt_run_test_fct ]] && continue

            # Run function
            [[ " ${_TT_FCTS_RUN_IN_TEST_FILE[*]} " == *" $fct "* ]] || \
                tt_run_test_fct "" "$fct" "$inc_fcts" "$exc_fcts"
        done
    fi

    return 0
}

function _tt_print_end_report {

    if [[ $_TT_ERR_NUMBER -gt 0 ]] ; then
        echo '================================================================'
        echo "$_TT_ERR_NUMBER error(s) encountered."

        # Loop on all errors
        for ((i = 0 ; i < _TT_ERR_NUMBER ; ++i)) ; do
            _tt_print_error $((i+1)) "${_TT_ERR_MSGS[$i]}" \
                "${_TT_ERR_STDERR_FILES[$i]}"
        done
    fi
}

## Runs tests on files and folders.
##
## Selects test script files by exploring recursively the provided list of
## paths.
## Runs the tests inside the selected files.
##
## @arg pattern str The regex pattern used to select files.
## @arg inc_files str A comma separated list of files. Only the files included
## in this list will be run, if they are selected.
## @arg autorun str If not empty, searches automatically for test functions
## inside the file and executes them.
## @arg fct_prefix str The prefix with which test functions start.
## @arg inc_fcts str A comma separated list of test functions that are allowed
## to run. If the list is not empty, then functions will be run only if they are
## in this list.
## @arg exc_fcts str A comma separated list of test functions that are not
## allowed to run. If the list is not empty, then functions will be run only
## if they are not in this list.
## @arg paths str ... A list of files and folders in which to search for test
## scripts to run.
function tt_run_tests {

    local pattern="$1"
    local inc_files="$2"
    local autorun="$3"
    local fct_prefix="$4"
    local inc_fcts="$5"
    local exc_fcts="$6"
    shift 6
    # Files and folders: $*

    # Set default values
    [[ -n $pattern ]] || pattern="$_TT_DEFAULT_FILE_PATTERN"
    [[ -n $autorun ]] || autorun=1
    [[ -n $fct_prefix ]] || fct_prefix="$_TT_DEFAULT_FCT_PREFIX"

    lg_debug 1 "fct_prefix=$fct_prefix"
    lg_debug 1 "Running tests of following files and folders: $*."
    lg_debug 1 "File pattern: $pattern"

    # Loop on folders and files to test
    for e in "$@" ; do
        lg_debug 1 "Looking at test path \"$e\"."
        
        [[ -f $e || -d $e ]] || \
            lg_fatal "\"$e\" is neither a file nor a folder."

        # File
        [[ -f $e ]] && tt_run_test_file "$e" "$autorun" "$fct_prefix" \
            "$inc_fcts" "$exc_fcts"

        # Folder
        if [[ -d $e ]] ; then
            lg_debug 1 "Looking for test files in folder \"$e\"."
            declare -a folder_content
            readarray -t folder_content <<<"$(find "$e" -mindepth 1 \
                -maxdepth 1 | sort)"
            declare f
            for f in "${folder_content[@]}" ; do

                # Check file pattern
                lg_debug 1 "Check if file \"$f\" is a test file."
                local filename
                filename="$(basename "$f")" || \
                    lg_fatal "Cannot get base name of \"$f\"."
                [[ -f $f && $filename =~ ^$pattern$ ]] || continue

                # Filter
                lg_debug 1 "Filtering filename \"$filename\" on list of files"\
                    "to include \"$inc_files\"."
                [[ -z $inc_files || ",$inc_files," == *",$filename,"* ]] \
                    || continue

                # Run tests in file
                lg_debug 1 "Run test file \"$f\"."
                tt_run_test_file "$f" "$autorun" "$fct_prefix" "$inc_fcts" \
                    "$exc_fcts"
            done
        fi

    done

    return 0
}
