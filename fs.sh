# vi: ft=bash
# shellcheck disable=SC1091
## @title FS (File System) module
##
## Functions interacting with the file system.
##
## The name/prefix of this module is FS (File System):
##  * `fs_`  for the public functions.
##  * `FS_`  for the public global variables or constants.
##  * `_fs_` for the private functions.
##  * `_FS_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_FS_SOURCED ]] || return 0
    _FS_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"
source "$(dirname "${BASH_SOURCE[0]}")/os.sh"
source "$(dirname "${BASH_SOURCE[0]}")/runtime.sh"

declare -a _FS_SAFE_RM_PATHS=()

## Checks if a file exists.
##
## Checks that a path points to an existing regular file.
##
## Exits program with an error message if file does not exist or is not a
## regular file.
## @arg path str A valid file system path.
## @status Returns 0.
## @exit Exists program with non-zero status if file does not exist or is not a
## regular file.
function fs_check_file {

    local -r path="$1"

    [[ -f $path ]] || lg_fatal "\"$path\" does not exist or is not a regular"\
        "file."
    
    return 0
}

## Gets a file's size.
##
## Returns the size of a file.
## @arg path str A file system path.
## @stdout int The size of the file, in Bytes.
function fs_get_file_size {

    local -r path="$1"
    fs_check_file "$path"

    # macOS
    if os_is_macos ; then
        stat -f "%z" "$path" || return 1

    # Linux
    else
        stat -c "%s" "$path" || return 1
    fi

    return 0
}

function fs_is_empty {
    [[ $(fs_get_file_size "$1") -eq 0 ]]
}

function _fs_rm_paths_on_exit {

    rm -rf "${_FS_SAFE_RM_PATHS[@]}" || return 1
    _FS_SAFE_RM_PATHS=()

    return 0
}

function fs_safe_rm_push {

    # Loop on paths
    declare path=
    for path in "$@" ; do

        # Check path existence
        [[ -e "$path" ]] || return 1

        # Convert to absolute/canonical path
        path="$(fs_absolute "$path")" || return 1

        # Push path to remove onto stack
        _FS_SAFE_RM_PATHS+=("$path")
    done

    # Register trap function
    rt_safe_append_trap_cmd _fs_rm_paths_on_exit EXIT || return 1
    
    return 0
}

function fs_safe_rm_count {
    echo "${#_FS_SAFE_RM_PATHS[@]}"
    return 0
}

function fs_safe_rm_pop {
    # Pop last paths on stack, but do not remove them from disk.

    declare -ri n="${1:-1}"

    # Pop n last paths
    declare -ri m="${#_FS_SAFE_RM_PATHS[@]}"
    declare -ri n_keep="$((n < m ? m - n : 0))"
    _FS_SAFE_RM_PATHS=("${_FS_SAFE_RM_PATHS[@]:0:$n_keep}")

    return 0
}

# shellcheck disable=SC2120
function fs_safe_rm_pop_del {
    # Pop last paths on stack, and remove them from disk.

    declare -ri n="${1:-1}"

    # Pop n last paths
    declare -ri m="${#_FS_SAFE_RM_PATHS[@]}"
    declare -ri n_keep="$((n < m ? m - n : 0))"
    rm -rf "${_FS_SAFE_RM_PATHS[@]:$n_keep:$n}" || return 1
    _FS_SAFE_RM_PATHS=("${_FS_SAFE_RM_PATHS[@]:0:$n_keep}")

    return 0
}

function _fs_tmp_get_dir {

    declare -i dir="${1:-0}"
    declare prefix="${2:-tmp}"
    declare folder="${3}"

    declare -a flags=()
    [[ $dir -eq 0 ]] || flags+=("-d")
    [[ -z $folder ]] || flags+=("-p" "$folder")
    flags+=('-t' "$prefix.XXXXXX")

    os_exec mktemp "${flags[@]}" || lg_fatal "Command mktemp failed."

    return 0
}

function fs_tmp_get_dir {
    _fs_tmp_get_dir 1 "$@"
}

function fs_tmp_get_file {
    _fs_tmp_get_dir 0 "$@"
}

function fs_tmp_acquire_file {
    declare file=
    file=$(fs_tmp_get_file "$@") || return 1
    fs_safe_rm_push "$file" || return 1
    return 0
}

function fs_tmp_acquire_dir {
    declare dir=
    dir=$(fs_tmp_get_dir "$@") || return 1
    fs_safe_rm_push "$dir" || return 1
    return 0
}

function fs_tmp_last_path {
    declare -ri n="${#_FS_SAFE_RM_PATHS[@]}"
    [[ $n -gt 0 ]] && echo "${_FS_SAFE_RM_PATHS[$((n-1))]}"
    return 0
}

function fs_tmp_release_last {
    fs_safe_rm_pop_del "$1" || return 1
    return 0
}

function fs_absolute {
    # Converst a path into an absolute & canonical path

    declare -r path="$1" # Path to convert into absolute/canonical path
    declare -r wd="${2:-$PWD}" # Working directory
    declare -i skip_next=0
    declare absolute_path=
    declare canonical_path=

    [[ ${wd:0:1} != / ]] \
        && lg_fatal "Working directory \"$wd\" is not absolute."

    if [[ -n $path ]] ; then

        # Make absolute
        if [[ ${path:0:1} == / ]] ; then
            absolute_path="$path"
        else
            absolute_path="$wd/$path"
        fi

        # Remove ending slash
        absolute_path="${absolute_path%/}"

        # Reverse path items
        declare -r reversed_path="$(echo -n "$absolute_path" | tac -bs/)"

        # Loop on path items in reverse order
        declare -r old_IFS="$IFS"
        IFS=/
        set -f # disable globbing so that ${rpath:1} does not expand patterns
        declare i=
        for i in ${reversed_path:1} ; do

            # Skip next item
            if [[ $i == ".." ]] ; then
                ((++skip_next))

            # Process item
            elif [[ -n $i && $i != . ]] ; then

                # Skip
                if [[ $skip_next -gt 0 ]] ; then
                    ((--skip_next))

                # Keep
                else
                    canonical_path="/$i$canonical_path"
                fi
            fi
        done
        set +f
        IFS="$old_IFS"
    fi

    # Remaining parent folders (`..`)
    if [[ $skip_next -gt 0 ]] ; then
        lg_fatal "Bad number of \"..\" parent folders in path \"$path\"," \
            "using working directory \"$wd\"."
        return 1

    # Up to root folder
    elif [[ -z $canonical_path ]] ; then
        canonical_path="/"
    fi

    # Test inodes. If different this means that canonical_path is not on the
    # same disk as absolute_path, we suppose the provided path is a symbolic
    # link that we simply resolve with readlink.
    if [[ -e $absolute_path ]] ; then
        declare -r x="$(stat -L -c '%d:%i' -- "$canonical_path")"
        declare -r y="$(stat -L -c '%d:%i' -- "$absolute_path")"
        [[ $x != "$y" ]] && canonical_path="$(readlink -f -n -- "$absolute_path")"
    fi

    # Output path
    echo -n "$canonical_path"

    return 0
}

true
