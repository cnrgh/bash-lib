# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is VR (VeRsion):
#  * `vr_`  for the public functions.
#  * `VR_`  for the public global variables or constants.
#  * `_vr_` for the private functions.
#  * `_VR_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_VR_SOURCED ]] || return 0
    _VR_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/str.sh"

function vr_bash_lt {
    local -r ver="$1"
    local -a triplet
    read -ra triplet <<<"$(vr_split "$ver")"
    cmp=$(vr_cmp_triplets "${BASH_VERSINFO[@]:0:3}" "${triplet[@]}")
    test "$cmp" -lt 0
}

function vr_bash_le {
    local -r ver="$1"
    local -a triplet
    read -ra triplet <<<"$(vr_split "$ver")"
    cmp=$(vr_cmp_triplets "${BASH_VERSINFO[@]:0:3}" "${triplet[@]}")
    test "$cmp" -le 0
}

function vr_bash_eq {
    local -r ver="$1"
    local -a triplet
    read -ra triplet <<<"$(vr_split "$ver")"
    cmp=$(vr_cmp_triplets "${BASH_VERSINFO[@]:0:3}" "${triplet[@]}")
    test "$cmp" -eq 0
}

function vr_bash_gt {
    local -r ver="$1"
    local -a triplet
    read -ra triplet <<<"$(vr_split "$ver")"
    cmp=$(vr_cmp_triplets "${BASH_VERSINFO[@]:0:3}" "${triplet[@]}")
    test "$cmp" -gt 0
}

function vr_bash_ge {
    local -r ver="$1"
    local -a triplet
    read -ra triplet <<<"$(vr_split "$ver")"
    cmp=$(vr_cmp_triplets "${BASH_VERSINFO[@]:0:3}" "${triplet[@]}")
    test "$cmp" -ge 0
}

## Split version number (major.minor.patch)
function vr_split {
    
    local -r v="$1"

    local -a arr
    read -ra arr <<<"$(st_split '.' "$v")"
    [[ ${#arr[@]} -lt 4 ]] || \
        lg_fatal "Too many parts found while parsing version number \"$v\"."
    echo "${arr[@]}"

    return 0
}

function vr_get_major {
    _vr_get_ver_number 0 "$@"
    return 0
}

function vr_get_minor {
    _vr_get_ver_number 1 "$@"
    return 0
}

function vr_get_patch {
    _vr_get_ver_number 2 "$@"
    return 0
}

function vr_cmp_triplets {
    # Compare version numbers as triplets major minor patch
    local major1="$1"
    local minor1="$2"
    local patch1="$3"
    local major2="$4"
    local minor2="$5"
    local patch2="$6"
    local res=0

    if [[ $major1 -lt $major2 ]] ; then
        res=-1
    elif [[ $major1 -gt $major2 ]] ; then
        res=+1
    else
        if [[ $minor1 -lt $minor2 ]] ; then
            res=-1
        elif [[ $minor1 -gt $minor2 ]] ; then
            res=+1
        else
            if [[ $patch1 -lt $patch2 ]] ; then
                res=-1
            elif [[ $patch1 -gt $patch2 ]] ; then
                res=+1
            fi
        fi
    fi
        
    echo $res
    return 0
}

## Compare version numbers
## Argument 1 : first version number
## Argument 2 : second version number
## Prints 0 if both version numbers are equal, -1 if first version number is
## lower thant the second and +1 otherwise.
function vr_cmp {

    local -r v1="$1"
    local -r v2="$2"

    local major1
    local major2
    local minor1
    local minor2
    local patch1
    local patch2
    major1="$(vr_get_major "$v1")" || lg_fatal "Failed parsing version."
    major2="$(vr_get_major "$v2")" || lg_fatal "Failed parsing version."
    minor1="$(vr_get_minor "$v1")" || lg_fatal "Failed parsing version."
    minor2="$(vr_get_minor "$v2")" || lg_fatal "Failed parsing version."
    patch1="$(vr_get_patch "$v1")" || lg_fatal "Failed parsing version."
    patch2="$(vr_get_patch "$v2")" || lg_fatal "Failed parsing version."

    vr_cmp_triplets "$major1" "$minor1" "$patch1" "$major2" "$minor2" "$patch2"
}

function _vr_get_ver_number {

    local -r i="$1"
    local -r v="$2"
    local -a arr
    read -ra arr <<<"$(vr_split "$v")" || \
        lg_fatal "Failed splitting version \"$v\"."
    local n="${arr[$i]}"
    
    # Remove leading zeros so it is not confused with an octal number
    n=$(sed -E 's/^0*(.+)$/\1/' <<<"$n")
    [[ -n $n ]] || n=0
    echo "$n"

    return 0
}

true
