# bash-lib

A bash library to ease script development.

The library is divided in module files to be loaded using the `source` command.

Public functions and variables are all prefixed with a namespace in order to
avoid collisions with your own script.

Here a non-exhaustive list of the modules you will find in this library:
 * A test framework for testing bash functions and any command line program.
 * Command line arguments parsing.
 * Logging.
 * YAML.
 * String.
 * File system.
 * Action loop.

Please, see [documentation](https://cnrgh.gitlab.io/bash-lib) for more information.

## NEWS

### 0.6.0 - 2024-11-19

 * Define version in `VERSION` file.
 * Implement `fs_safe_rm_*` functions in `fs.sh`.
 * Implement trap handling functions in `runtime.sh`.
