PREFIX   ?= /usr/local
DESTDIR  ?=
LIBDIR   ?= $(PREFIX)/lib/
SHAREDIR ?= $(PREFIX)/share/
SH_FILES = $(filter-out testthat.sh,$(wildcard *.sh))
BASH_VERSIONS=4.2 4.4 5.1 5.2

old_sh_files = $(wildcard src/*.sh)

all:

check:
	for f in *.sh ; do shellcheck -s bash $$f || return $$? ; done
	shellcheck runtests gendoc testthat embedlibs || return $$?
	for f in tests/*.sh ; do shellcheck -s bash $$f || return $$? ; done

npm/bin/csslint:
	npm install -g --prefix npm csslint

doc: npm/bin/csslint
	npm/bin/csslint doc.css
	./gendoc -i index.md -O public $(SH_FILES)
	chmod a+rX public
	for f in public/*.html ; do echo "Processing $$f with tidy..." ; tidy -eq "$$f" ; done

test:
	./runtests tests

dockers: $(addprefix docker_,$(BASH_VERSIONS))

docker_%:
	docker build --progress=plain --build-arg "DOCKER_BASH_VERSION=$(patsubst docker_%,%,$@)" -t "bash-lib:$(patsubst docker_%,%,$@)" .
	docker run "bash-lib:$(patsubst docker_%,%,$@)"

base_dockers: $(addprefix base_docker_,$(BASH_VERSIONS))

base_docker_%:
	docker build --progress=plain -f base_docker.dockerfile --build-arg "DOCKER_BASH_VERSION=$(patsubst base_docker_%,%,$@)" -t "registry.gitlab.com/cnrgh/bash-lib:$(patsubst base_docker_%,%,$@)" .

push_base_dockers: $(addprefix push_base_docker_,$(BASH_VERSIONS))

push_base_docker_%:
	docker push "registry.gitlab.com/cnrgh/bash-lib:$(patsubst push_base_docker_%,%,$@)"

test.doc:
	docker build -t bash-lib-doc -f doc.dockerfile .
	docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$(DISPLAY) -h $(shell hostname) -v $(HOME)/.Xauthority:/home/john/.Xauthority bash-lib-doc

install: install_lib_files

install_libdir:
	mkdir -p "$(DESTDIR)/$(LIBDIR)/cnrgh_bash_library"

install_lib_files: install_libdir
	install -m 0644 $(old_sh_files) "$(DESTDIR)/$(LIBDIR)/cnrgh_bash_library"

clean:
	$(RM) -r tests/output
