# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is LG (LoGging):
#  * `lg_`  for the public functions.
#  * `LG_`  for the public global variables or constants.
#  * `_lg_` for the private functions.
#  * `_LG_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_LG_SOURCED ]] || return 0
    _LG_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/bool.sh"
source "$(dirname "${BASH_SOURCE[0]}")/deprecate.sh"

[[ $LG_LEVEL_OFF == 0 ]] || declare -ri LG_LEVEL_OFF=0
[[ $LG_LEVEL_FATAL == 1 ]] || declare -ri LG_LEVEL_FATAL=1
[[ $LG_LEVEL_ERROR == 2 ]] || declare -ri LG_LEVEL_ERROR=2
[[ $LG_LEVEL_WARNING == 3 ]] || declare -ri LG_LEVEL_WARNING=3
[[ $LG_LEVEL_INFO == 4 ]] || declare -ri LG_LEVEL_INFO=4
[[ $LG_LEVEL_DEBUG == 5 ]] || declare -ri LG_LEVEL_DEBUG=5
[[ $LG_LEVEL_TRACE == 6 ]] || declare -ri LG_LEVEL_TRACE=6

declare -i _LG_LEVEL="$LG_LEVEL_INFO"
declare -i _LG_MAX_LEVEL=100
_LG_FILE_ENABLED=false
_LG_FILE="$HOME/.$(basename "$0").log"
_LG_FILE_MAX_SIZE=10000 # 10 KB
[[ -z $LG_ERROR_IS_FATAL ]] && LG_ERROR_IS_FATAL=true
_LG_COLOR=1
_LG_COLOR_TAG_DELIMITERS=1
_LG_COLOR_TAG_CONTENT=1
_LG_COLOR_MSG=1
_LG_FATAL_COLOR=$'\e[1;31m'
_LG_ERROR_COLOR=$'\e[0;31m'
_LG_WARNING_COLOR=$'\e[0;35m'
_LG_INFO_COLOR=$'\e[0;32m'
_LG_DEBUG_COLOR=$'\e[0;33m'
_LG_TRACE_COLOR=$'\e[0;37m'
_LG_TIMESTAMP=
_LG_CODE_LOCATION=

function lg_set_level {

    declare -ri level="$1"

    [[ $level -lt $LG_LEVEL_OFF || $level -gt $LG_LEVEL_TRACE ]] && \
        echo "Loggin level must be between $LG_LEVEL_OFF and $LG_LEVEL_TRACE." \
        && return 1

    _LG_LEVEL=$level

    return 0
}

function lg_inc_level {
    [[ $_LG_LEVEL -lt $LG_LEVEL_TRACE ]] && ((++_LG_LEVEL))
    return 0
}

function lg_get_level {
    declare -i lvl="$_LG_LEVEL"
    [[ $lvl -gt "$_LG_MAX_LEVEL" ]] && lvl="$_LG_MAX_LEVEL"
    echo -n "$lvl"
    return 0
}

function lg_level_ge {
    declare -ri lvl="$1"
    [[ $(lg_get_level) -ge "$lvl" ]]
}

function lg_level_le {
    declare -ri lvl="$1"
    [[ $(lg_get_level) -le "$lvl" ]]
}

function lg_ensure_max_level {
    _LG_MAX_LEVEL="$1"
    return 0
}

function lg_enable_code_location {
    _LG_CODE_LOCATION=1
    return 0
}

function lg_disable_code_location {
    _LG_CODE_LOCATION=
    return 0
}

function lg_enable_epoch_timestamp {
    _LG_TIMESTAMP=epoch
    return 0
}

function lg_enable_i18n_timestamp {
    _LG_TIMESTAMP=i18n
    return 0
}

function lg_disable_timestamp {
    _LG_TIMESTAMP=
    return 0
}

function lg_set_quiet {
    lg_ensure_max_level "$LG_LEVEL_WARNING"
    return 0
}

function lg_unset_quiet {
    lg_ensure_max_level 100
    return 0
}

function lg_is_quiet {
    [[ $_LG_MAX_LEVEL -le "$LG_LEVEL_WARNING" ]]
}

function lg_set_debug_level {
    # DEPRECATED
    declare -ri lvl="$1"
    if [[ $lvl -eq 1 ]] && lg_level_le "$LG_LEVEL_DEBUG" ; then
        lg_set_level "$LG_LEVEL_DEBUG"
    elif [[ $lvl -ge 2 ]] && lg_level_le "$LG_LEVEL_TRACE" ; then
        lg_set_level "$LG_LEVEL_TRACE"
    fi
    return 0
}

function lg_get_debug_level {
    # DEPRECATED
    if [[ $(lg_get_level) -eq $LG_LEVEL_DEBUG ]] ; then
        echo "1"
    elif [[ $(lg_get_level) -eq $LG_LEVEL_TRACE ]] ; then
        echo "2"
    else
        echo "0"
    fi
    return 0
}

function lg_debug_is_enabled {
    [[ $(lg_get_level) -ge $LG_LEVEL_DEBUG ]]
}

function lg_disable_color {
    _LG_COLOR=0
    return 0
}

function lg_enable_color {
    _LG_COLOR=1
    return 0
}

function lg_disable_color_tag_delimiters {
    _LG_COLOR_TAG_DELIMITERS=0
    return 0
}

function lg_enable_color_tag_delimiters {
    _LG_COLOR_TAG_DELIMITERS=1
    return 0
}

function lg_get_color_tag_delimiters {
    echo "$_LG_COLOR_TAG_DELIMITERS"
    return 0
}

function lg_set_color_tag_delimiters {
    _LG_COLOR_TAG_DELIMITERS="$1"
    return 0
}

function lg_disable_color_tag_content {
    _LG_COLOR_TAG_CONTENT=0
    return 0
}

function lg_enable_color_tag_content {
    _LG_COLOR_TAG_CONTENT=1
    return 0
}

function lg_get_color_tag_content {
    echo "$_LG_COLOR_TAG_CONTENT"
    return 0
}

function lg_set_color_tag_content {
    _LG_COLOR_TAG_CONTENT="$1"
    return 0
}

function lg_disable_color_msg {
    _LG_COLOR_MSG=0
    return 0
}

function lg_enable_color_msg {
    _LG_COLOR_MSG=1
    return 0
}

function lg_get_color_msg {
    echo "$_LG_COLOR_MSG"
    return 0
}

function lg_set_color_msg {
    _LG_COLOR_MSG="$1"
    return 0
}

function lg_enable_file {
    _LG_FILE_ENABLED=true
    return 0
}

function lg_disable_file {
    _LG_FILE_ENABLED=false
    return 0
}

function lg_get_file {
    echo "$_LG_FILE"
    return 0
}

function lg_set_file {
    _LG_FILE="$1"
    return 0
}

function lg_get_file_max_size {
    echo "$_LG_FILE_MAX_SIZE"
    return 0
}

function lg_set_file_max_size {
    _LG_FILE_MAX_SIZE=$1
    _LG_FILE_MAX_SIZE=$((_LG_FILE_MAX_SIZE)) # Make sure it is an integer value
    return 0
}

function lg_set_color {

    declare -l tag="$1"
    declare -l color="$2"
    local color_code

    # Get color code
    case $color in
        black)               color_code='0;30' ;;
        dark_gray)           color_code='1;30' ;;
        red|dark_red)        color_code='0;31' ;;
        light_red)           color_code='1;31' ;;
        green|dark_green)    color_code='0;32' ;;
        brown|dark_yellow)   color_code='0;33' ;;
        blue|dark_blue)      color_code='0;34' ;;
        purple|dark_purple)  color_code='0;35' ;;
        cyan|dark_cyan)      color_code='0;36' ;;
        light_gray)          color_code='0;37' ;;
        light_green)         color_code='1;32' ;;
        yellow|light_yellow) color_code='1;33' ;;
        light_blue)          color_code='1;34' ;;
        light_purple)        color_code='1;35' ;;
        light_cyan)          color_code='1;36' ;;
        white)               color_code='1;37' ;;
        *)            lg_fatal "Unknown color \"$color\"." ;;
    esac

    # Set to corresponding tag
    case $tag in
        info)    _LG_INFO_COLOR=$'\e['$color_code'm'    ;;
        debug)   _LG_DEBUG_COLOR=$'\e['$color_code'm'   ;;
        warning) _LG_WARNING_COLOR=$'\e['$color_code'm' ;;
        error)   _LG_ERROR_COLOR=$'\e['$color_code'm'   ;;
        *)       lg_fatal "Unknown message tag \"$tag\"." ;;
    esac

    return 0
}

function lg_quit {

    local status=$1
    shift
    local msg="$*"

    lg_info "$msg"

    exit "$status"
}

function _lg_write_in_file {

    local msg="$*"

    if [[ -f $_LG_FILE ]] ; then
        local sz
        sz=$(stat --printf="%s" "$_LG_FILE") || \
            lg_fatal "Impossible to stat file \"$_LG_FILE\"."
        [[ $sz -le $_LG_FILE_MAX_SIZE ]] || rm "$_LG_FILE"
    fi
    bo_is_true "$_LG_FILE_ENABLED" && [[ -n $_LG_FILE ]] && \
        echo "$msg" >>"$_LG_FILE"

    return 0
}

function lg_file {

    local msg="$*"

    _lg_write_in_file "[FILE]" "$msg"

    return 0
}

function _lg_msg {

    local tag="$1"
    local -r color="$2"
    shift 2

    # Add timestamp
    case $_LG_TIMESTAMP in
        epoch) tag=$(date +%s)" $tag" ;;
        i18n) tag=$(date +%Y-%m-%d-%H:%M:%S)" $tag" ;;
    esac

    # Add code location
    bo_is_true "$_LG_CODE_LOCATION" \
        && tag="$tag, ${FUNCNAME[2]}@${BASH_SOURCE[2]}:${BASH_LINENO[1]}"
    
    # Write to log file
    _lg_write_in_file "[$tag]" "$*"
    
    # Print to console
    local msg=
    local color_on=
    bo_all_true "$_LG_COLOR" "$_LG_COLOR_TAG_DELIMITERS" && \
        echo -n "$color" >&2
    echo -n "[" >&2
    if bo_all_true "$_LG_COLOR" "$_LG_COLOR_TAG_CONTENT" ; then
        bo_is_false "$color_on" && echo -n "$color" >&2
        color_on=1
    else
        bo_is_true "$color_on" && echo -n $'\e[0m' >&2
        color_on=
    fi
    echo -n "$tag" >&2
    if bo_all_true "$_LG_COLOR" "$_LG_COLOR_TAG_DELIMITERS" ; then
        bo_is_false "$color_on" && echo -n "$color" >&2
        color_on=1
    else
        bo_is_true "$color_on" && echo -n $'\e[0m' >&2
        color_on=
    fi
    echo -n "]" >&2
    if bo_all_true "$_LG_COLOR" "$_LG_COLOR_MSG" ; then
        bo_is_false "$color_on" && echo -n "$color" >&2
        color_on=1
    else
        bo_is_true "$color_on" && echo -n $'\e[0m' >&2
        color_on=
    fi
    echo -ne " $*" >&2
    bo_all_true "$color_on" "$_LG_COLOR" && echo -n $'\e[0m' >&2
    echo >&2

    return 0
}

function lg_msg {
    declare -r tag="$1"
    shift 2
    _lg_msg "$@"
}

function lg_error_no_fail {
    # DEPRECATED New behaviour of lg_error never fails
    lg_level_ge "$LG_LEVEL_ERROR" && \
        _lg_msg ERROR "$_LG_ERROR_COLOR" "$@"
    return 0
}

function lg_error {

    # Write
    lg_level_ge "$LG_LEVEL_ERROR" && \
        _lg_msg ERROR "$_LG_ERROR_COLOR" "$@"

    # Exit
    bo_is_true "$LG_ERROR_IS_FATAL" && exit 1

    return 0
}

function lg_fatal {

    local status=1
    
    # Is exit status specified in first argument?
    if [[ $1 =~ ^[0-9]+$ ]] ; then
        status="$1"
        shift
    fi
 
    lg_level_ge "$LG_LEVEL_FATAL" && \
        _lg_msg FATAL "$_LG_FATAL_COLOR" "$@"

    exit "$status"
}

function lg_warning {
    lg_level_ge "$LG_LEVEL_WARNING" && \
        _lg_msg WARNING "$_LG_WARNING_COLOR" "$@"
    return 0
}

function lg_debug {

    declare -r arg1=$1 ; shift

    # Debug level in argument is deprecated
    [[ $arg1 == 1 ]] && lg_debug "$@" && return 0
    [[ $arg1 == 2 || $arg1 == 3 ]] && lg_trace "$@" && return 0

    # Write
    lg_level_ge "$LG_LEVEL_DEBUG" && \
        _lg_msg DEBUG "$_LG_DEBUG_COLOR" "$arg1" "$@"

    return 0
}

function lg_trace {
    lg_level_ge "$LG_LEVEL_TRACE" && \
        _lg_msg TRACE "$_LG_TRACE_COLOR" "$@"
    return 0
}

function lg_info {
    lg_level_ge "$LG_LEVEL_INFO" && \
        _lg_msg INFO "$_LG_INFO_COLOR" "$@"
    return 0
}

function lg_info_from_file {

    local -r file="$1"

    local -r old_ifs="$IFS"
    IFS=$'\n'
    while read -r line ; do
        lg_info "$line"
    done < "$file"
    IFS="$old_ifs"

    return 0
}

# Check deprecated global variables
if [[ -n $LG_LOG_TO_FILE ]] ; then
    dp_deprecation_warning "Variable LG_LOG_TO_FILE is deprecated. Please use function lg_enable_file instead."
    _LG_FILE_ENABLED="$LG_LOG_TO_FILE"
fi
if [[ -n $LG_FILE ]] ; then
    dp_deprecation_warning "Variable LG_FILE is deprecated. Please use function lg_set_file instead."
    _LG_FILE="$LG_FILE"
fi
if [[ -n $LG_FILE_MAX_SIZE ]] ; then
    dp_deprecation_warning "Variable LG_FILE_MAX_SIZE is deprecated. Please use function lg_set_file_max_size instead."
    _LG_FILE_MAX_SIZE="$LG_FILE_MAX_SIZE"
fi
if [[ -n $LG_DEBUG ]] ; then
    dp_deprecation_warning "Variable LG_DEBUG is deprecated. Please use function lg_set_level instead."
    _LG_LEVEL="$LG_LEVEL_DEBUG"
fi
if [[ -n $LG_QUIET ]] ; then
    dp_deprecation_warning "Variable LG_QUIET is deprecated. Please use function lg_set_quiet instead."
    _LG_QUIET="$LG_DEBUG"
fi

# Check deprecation of old lg_error behaviour
bo_is_true "$LG_ERROR_IS_FATAL" && dp_deprecation_warning \
    "lg_error has been replaced by lg_fatal." \
    " Please, replace all your calls to lg_error with lg_fatal." \
    "Once done, set LG_ERROR_IS_FATAL=false." \
    "This will enable the new default behaviour of lg_error, which will not" \
    "exit program anymore, but only print a message."

true
