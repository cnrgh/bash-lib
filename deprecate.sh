# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is DP (DePrecate):
#  * `dp_`  for the public functions.
#  * `DP_`  for the public global variables or constants.
#  * `_dp_` for the private functions.
#  * `_DP_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_DP_SOURCED ]] || return 0
    _DP_SOURCED=1
fi

# shellcheck disable=SC2034
LG_ERROR_IS_FATAL=false
source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"

_DP_DEPRECATION_ENABLED=1

function dp_disable_depreaction_messages {

    _DP_DEPRECATION_ENABLED=
    return 0
}

function dp_deprecation_warning {
    # Print a deprecation warning message.

    if [[ -n $_DP_DEPRECATION_ENABLED ]] ; then
        lg_warning "DEPRECATION - $*"
    fi

    return 0
}

function dp_deprecated {
    # To be used inside the deprecated function.

    if [[ -n $_DP_DEPRECATION_ENABLED ]] ; then
        local -r new_fct="$1"
        local -r new_lib="$2"
        local from_lib=
        [[ -z $new_lib ]] || from_lib=", from $new_lib"
     
        dp_deprecation_warning "Function ${FUNCNAME[1]} is deprecated." \
            "Use $new_fct instead${from_lib}."
    fi
    
    return 0
}

true
