# vi: ft=bash
# shellcheck disable=SC2034
# shellcheck disable=SC1091
# The name/prefix of this module is OP (OutPut):
#  * `op_`  for the public functions.
#  * `OP_`  for the public global variables or constants.
#  * `_op_` for the private functions.
#  * `_OP_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_OP_SOURCED ]] || return 0
    _OP_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"
source "$(dirname "${BASH_SOURCE[0]}")/maths.sh"
source "$(dirname "${BASH_SOURCE[0]}")/ver.sh"

# Define constants
if [[ -z $OP_RESET ]] ; then

    # Style
    declare -gr OP_RESET='\e[0m'
    declare -gr OP_PLAIN=$OP_RESET
    declare -gr OP_DARK=$OP_RESET
    declare -gr OP_BOLD='\e[1m'
    declare -gr OP_LIGHT=$OP_BOLD
    declare -gr OP_FAINT='\e[2m'
    declare -gr OP_ITALIC='\e[3m'
    declare -gr OP_UNDERLINE='\e[4m'
    declare -gr OP_SLOW_BLINK='\e[5m'
    declare -gr OP_BLINK=$OP_SLOW_BLINK
    declare -gr OP_RAPID_BLINK='\e[6m'
    declare -gr OP_REVERSE='\e[7m'
    declare -gr OP_CONCEAL='\e[8m'
    declare -gr OP_CROSSEDOUT='\e[9m'
    declare -gr OP_CROSSED=$OP_CROSSEDOUT
    declare -gr OP_STRUCKTHROUGH=$OP_CROSSED
    declare -gr OP_STRUCK=$OP_CROSSED

    # 3 bits colors
    declare -gri OP_BLACK=0
    declare -gri OP_RED=1
    declare -gri OP_GREEN=2
    declare -gri OP_YELLOW=3
    declare -gri OP_BROWN=3
    declare -gri OP_BLUE=4
    declare -gri OP_PURPLE=5
    declare -gri OP_CYAN=6
    declare -gri OP_GRAY=7
    declare -gri OP_WHITE=7

    # 8 bits colors (256 colors) TO RGB HEX
    declare -gra OP_256TORGBHEX=('000000' '800000' '008000' '808000' '000080'
    '800080' '008080' 'c0c0c0' '808080' 'ff0000' '00ff00' 'ffff00' '0000ff'
    'ff00ff' '00ffff' 'ffffff' '000000' '00005f' '000087' '0000af' '0000d7'
    '0000ff' '005f00' '005f5f' '005f87' '005faf' '005fd7' '005fff' '008700'
    '00875f' '008787' '0087af' '0087d7' '0087ff' '00af00' '00af5f' '00af87'
    '00afaf' '00afd7' '00afff' '00d700' '00d75f' '00d787' '00d7af' '00d7d7'
    '00d7ff' '00ff00' '00ff5f' '00ff87' '00ffaf' '00ffd7' '00ffff' '5f0000'
    '5f005f' '5f0087' '5f00af' '5f00d7' '5f00ff' '5f5f00' '5f5f5f' '5f5f87'
    '5f5faf' '5f5fd7' '5f5fff' '5f8700' '5f875f' '5f8787' '5f87af' '5f87d7'
    '5f87ff' '5faf00' '5faf5f' '5faf87' '5fafaf' '5fafd7' '5fafff' '5fd700'
    '5fd75f' '5fd787' '5fd7af' '5fd7d7' '5fd7ff' '5fff00' '5fff5f' '5fff87'
    '5fffaf' '5fffd7' '5fffff' '870000' '87005f' '870087' '8700af' '8700d7'
    '8700ff' '875f00' '875f5f' '875f87' '875faf' '875fd7' '875fff' '878700'
    '87875f' '878787' '8787af' '8787d7' '8787ff' '87af00' '87af5f' '87af87'
    '87afaf' '87afd7' '87afff' '87d700' '87d75f' '87d787' '87d7af' '87d7d7'
    '87d7ff' '87ff00' '87ff5f' '87ff87' '87ffaf' '87ffd7' '87ffff' 'af0000'
    'af005f' 'af0087' 'af00af' 'af00d7' 'af00ff' 'af5f00' 'af5f5f' 'af5f87'
    'af5faf' 'af5fd7' 'af5fff' 'af8700' 'af875f' 'af8787' 'af87af' 'af87d7'
    'af87ff' 'afaf00' 'afaf5f' 'afaf87' 'afafaf' 'afafd7' 'afafff' 'afd700'
    'afd75f' 'afd787' 'afd7af' 'afd7d7' 'afd7ff' 'afff00' 'afff5f' 'afff87'
    'afffaf' 'afffd7' 'afffff' 'd70000' 'd7005f' 'd70087' 'd700af' 'd700d7'
    'd700ff' 'd75f00' 'd75f5f' 'd75f87' 'd75faf' 'd75fd7' 'd75fff' 'd78700'
    'd7875f' 'd78787' 'd787af' 'd787d7' 'd787ff' 'd7af00' 'd7af5f' 'd7af87'
    'd7afaf' 'd7afd7' 'd7afff' 'd7d700' 'd7d75f' 'd7d787' 'd7d7af' 'd7d7d7'
    'd7d7ff' 'd7ff00' 'd7ff5f' 'd7ff87' 'd7ffaf' 'd7ffd7' 'd7ffff' 'ff0000'
    'ff005f' 'ff0087' 'ff00af' 'ff00d7' 'ff00ff' 'ff5f00' 'ff5f5f' 'ff5f87'
    'ff5faf' 'ff5fd7' 'ff5fff' 'ff8700' 'ff875f' 'ff8787' 'ff87af' 'ff87d7'
    'ff87ff' 'ffaf00' 'ffaf5f' 'ffaf87' 'ffafaf' 'ffafd7' 'ffafff' 'ffd700'
    'ffd75f' 'ffd787' 'ffd7af' 'ffd7d7' 'ffd7ff' 'ffff00' 'ffff5f' 'ffff87'
    'ffffaf' 'ffffd7' 'ffffff' '080808' '121212' '1c1c1c' '262626' '303030'
    '3a3a3a' '444444' '4e4e4e' '585858' '626262' '6c6c6c' '767676' '808080'
    '8a8a8a' '949494' '9e9e9e' 'a8a8a8' 'b2b2b2' 'bcbcbc' 'c6c6c6' 'd0d0d0'
    'dadada' 'e4e4e4' 'eeeeee')

    # 8 bits colors (256 colors) TO RGB DEC
    declare -gra OP_256TORGBDEC=("$((16#00)) $((16#00)) $((16#00))"
    "$((16#80)) $((16#00)) $((16#00))" "$((16#00)) $((16#80)) $((16#00))"
    "$((16#80)) $((16#80)) $((16#00))" "$((16#00)) $((16#00)) $((16#80))"
    "$((16#80)) $((16#00)) $((16#80))" "$((16#00)) $((16#80)) $((16#80))"
    "$((16#c0)) $((16#c0)) $((16#c0))" "$((16#80)) $((16#80)) $((16#80))"
    "$((16#ff)) $((16#00)) $((16#00))" "$((16#00)) $((16#ff)) $((16#00))"
    "$((16#ff)) $((16#ff)) $((16#00))" "$((16#00)) $((16#00)) $((16#ff))"
    "$((16#ff)) $((16#00)) $((16#ff))" "$((16#00)) $((16#ff)) $((16#ff))"
    "$((16#ff)) $((16#ff)) $((16#ff))" "$((16#00)) $((16#00)) $((16#00))"
    "$((16#00)) $((16#00)) $((16#5f))" "$((16#00)) $((16#00)) $((16#87))"
    "$((16#00)) $((16#00)) $((16#af))" "$((16#00)) $((16#00)) $((16#d7))"
    "$((16#00)) $((16#00)) $((16#ff))" "$((16#00)) $((16#5f)) $((16#00))"
    "$((16#00)) $((16#5f)) $((16#5f))" "$((16#00)) $((16#5f)) $((16#87))"
    "$((16#00)) $((16#5f)) $((16#af))" "$((16#00)) $((16#5f)) $((16#d7))"
    "$((16#00)) $((16#5f)) $((16#ff))" "$((16#00)) $((16#87)) $((16#00))"
    "$((16#00)) $((16#87)) $((16#5f))" "$((16#00)) $((16#87)) $((16#87))"
    "$((16#00)) $((16#87)) $((16#af))" "$((16#00)) $((16#87)) $((16#d7))"
    "$((16#00)) $((16#87)) $((16#ff))" "$((16#00)) $((16#af)) $((16#00))"
    "$((16#00)) $((16#af)) $((16#5f))" "$((16#00)) $((16#af)) $((16#87))"
    "$((16#00)) $((16#af)) $((16#af))" "$((16#00)) $((16#af)) $((16#d7))"
    "$((16#00)) $((16#af)) $((16#ff))" "$((16#00)) $((16#d7)) $((16#00))"
    "$((16#00)) $((16#d7)) $((16#5f))" "$((16#00)) $((16#d7)) $((16#87))"
    "$((16#00)) $((16#d7)) $((16#af))" "$((16#00)) $((16#d7)) $((16#d7))"
    "$((16#00)) $((16#d7)) $((16#ff))" "$((16#00)) $((16#ff)) $((16#00))"
    "$((16#00)) $((16#ff)) $((16#5f))" "$((16#00)) $((16#ff)) $((16#87))"
    "$((16#00)) $((16#ff)) $((16#af))" "$((16#00)) $((16#ff)) $((16#d7))"
    "$((16#00)) $((16#ff)) $((16#ff))" "$((16#5f)) $((16#00)) $((16#00))"
    "$((16#5f)) $((16#00)) $((16#5f))" "$((16#5f)) $((16#00)) $((16#87))"
    "$((16#5f)) $((16#00)) $((16#af))" "$((16#5f)) $((16#00)) $((16#d7))"
    "$((16#5f)) $((16#00)) $((16#ff))" "$((16#5f)) $((16#5f)) $((16#00))"
    "$((16#5f)) $((16#5f)) $((16#5f))" "$((16#5f)) $((16#5f)) $((16#87))"
    "$((16#5f)) $((16#5f)) $((16#af))" "$((16#5f)) $((16#5f)) $((16#d7))"
    "$((16#5f)) $((16#5f)) $((16#ff))" "$((16#5f)) $((16#87)) $((16#00))"
    "$((16#5f)) $((16#87)) $((16#5f))" "$((16#5f)) $((16#87)) $((16#87))"
    "$((16#5f)) $((16#87)) $((16#af))" "$((16#5f)) $((16#87)) $((16#d7))"
    "$((16#5f)) $((16#87)) $((16#ff))" "$((16#5f)) $((16#af)) $((16#00))"
    "$((16#5f)) $((16#af)) $((16#5f))" "$((16#5f)) $((16#af)) $((16#87))"
    "$((16#5f)) $((16#af)) $((16#af))" "$((16#5f)) $((16#af)) $((16#d7))"
    "$((16#5f)) $((16#af)) $((16#ff))" "$((16#5f)) $((16#d7)) $((16#00))"
    "$((16#5f)) $((16#d7)) $((16#5f))" "$((16#5f)) $((16#d7)) $((16#87))"
    "$((16#5f)) $((16#d7)) $((16#af))" "$((16#5f)) $((16#d7)) $((16#d7))"
    "$((16#5f)) $((16#d7)) $((16#ff))" "$((16#5f)) $((16#ff)) $((16#00))"
    "$((16#5f)) $((16#ff)) $((16#5f))" "$((16#5f)) $((16#ff)) $((16#87))"
    "$((16#5f)) $((16#ff)) $((16#af))" "$((16#5f)) $((16#ff)) $((16#d7))"
    "$((16#5f)) $((16#ff)) $((16#ff))" "$((16#87)) $((16#00)) $((16#00))"
    "$((16#87)) $((16#00)) $((16#5f))" "$((16#87)) $((16#00)) $((16#87))"
    "$((16#87)) $((16#00)) $((16#af))" "$((16#87)) $((16#00)) $((16#d7))"
    "$((16#87)) $((16#00)) $((16#ff))" "$((16#87)) $((16#5f)) $((16#00))"
    "$((16#87)) $((16#5f)) $((16#5f))" "$((16#87)) $((16#5f)) $((16#87))"
    "$((16#87)) $((16#5f)) $((16#af))" "$((16#87)) $((16#5f)) $((16#d7))"
    "$((16#87)) $((16#5f)) $((16#ff))" "$((16#87)) $((16#87)) $((16#00))"
    "$((16#87)) $((16#87)) $((16#5f))" "$((16#87)) $((16#87)) $((16#87))"
    "$((16#87)) $((16#87)) $((16#af))" "$((16#87)) $((16#87)) $((16#d7))"
    "$((16#87)) $((16#87)) $((16#ff))" "$((16#87)) $((16#af)) $((16#00))"
    "$((16#87)) $((16#af)) $((16#5f))" "$((16#87)) $((16#af)) $((16#87))"
    "$((16#87)) $((16#af)) $((16#af))" "$((16#87)) $((16#af)) $((16#d7))"
    "$((16#87)) $((16#af)) $((16#ff))" "$((16#87)) $((16#d7)) $((16#00))"
    "$((16#87)) $((16#d7)) $((16#5f))" "$((16#87)) $((16#d7)) $((16#87))"
    "$((16#87)) $((16#d7)) $((16#af))" "$((16#87)) $((16#d7)) $((16#d7))"
    "$((16#87)) $((16#d7)) $((16#ff))" "$((16#87)) $((16#ff)) $((16#00))"
    "$((16#87)) $((16#ff)) $((16#5f))" "$((16#87)) $((16#ff)) $((16#87))"
    "$((16#87)) $((16#ff)) $((16#af))" "$((16#87)) $((16#ff)) $((16#d7))"
    "$((16#87)) $((16#ff)) $((16#ff))" "$((16#af)) $((16#00)) $((16#00))"
    "$((16#af)) $((16#00)) $((16#5f))" "$((16#af)) $((16#00)) $((16#87))"
    "$((16#af)) $((16#00)) $((16#af))" "$((16#af)) $((16#00)) $((16#d7))"
    "$((16#af)) $((16#00)) $((16#ff))" "$((16#af)) $((16#5f)) $((16#00))"
    "$((16#af)) $((16#5f)) $((16#5f))" "$((16#af)) $((16#5f)) $((16#87))"
    "$((16#af)) $((16#5f)) $((16#af))" "$((16#af)) $((16#5f)) $((16#d7))"
    "$((16#af)) $((16#5f)) $((16#ff))" "$((16#af)) $((16#87)) $((16#00))"
    "$((16#af)) $((16#87)) $((16#5f))" "$((16#af)) $((16#87)) $((16#87))"
    "$((16#af)) $((16#87)) $((16#af))" "$((16#af)) $((16#87)) $((16#d7))"
    "$((16#af)) $((16#87)) $((16#ff))" "$((16#af)) $((16#af)) $((16#00))"
    "$((16#af)) $((16#af)) $((16#5f))" "$((16#af)) $((16#af)) $((16#87))"
    "$((16#af)) $((16#af)) $((16#af))" "$((16#af)) $((16#af)) $((16#d7))"
    "$((16#af)) $((16#af)) $((16#ff))" "$((16#af)) $((16#d7)) $((16#00))"
    "$((16#af)) $((16#d7)) $((16#5f))" "$((16#af)) $((16#d7)) $((16#87))"
    "$((16#af)) $((16#d7)) $((16#af))" "$((16#af)) $((16#d7)) $((16#d7))"
    "$((16#af)) $((16#d7)) $((16#ff))" "$((16#af)) $((16#ff)) $((16#00))"
    "$((16#af)) $((16#ff)) $((16#5f))" "$((16#af)) $((16#ff)) $((16#87))"
    "$((16#af)) $((16#ff)) $((16#af))" "$((16#af)) $((16#ff)) $((16#d7))"
    "$((16#af)) $((16#ff)) $((16#ff))" "$((16#d7)) $((16#00)) $((16#00))"
    "$((16#d7)) $((16#00)) $((16#5f))" "$((16#d7)) $((16#00)) $((16#87))"
    "$((16#d7)) $((16#00)) $((16#af))" "$((16#d7)) $((16#00)) $((16#d7))"
    "$((16#d7)) $((16#00)) $((16#ff))" "$((16#d7)) $((16#5f)) $((16#00))"
    "$((16#d7)) $((16#5f)) $((16#5f))" "$((16#d7)) $((16#5f)) $((16#87))"
    "$((16#d7)) $((16#5f)) $((16#af))" "$((16#d7)) $((16#5f)) $((16#d7))"
    "$((16#d7)) $((16#5f)) $((16#ff))" "$((16#d7)) $((16#87)) $((16#00))"
    "$((16#d7)) $((16#87)) $((16#5f))" "$((16#d7)) $((16#87)) $((16#87))"
    "$((16#d7)) $((16#87)) $((16#af))" "$((16#d7)) $((16#87)) $((16#d7))"
    "$((16#d7)) $((16#87)) $((16#ff))" "$((16#d7)) $((16#af)) $((16#00))"
    "$((16#d7)) $((16#af)) $((16#5f))" "$((16#d7)) $((16#af)) $((16#87))"
    "$((16#d7)) $((16#af)) $((16#af))" "$((16#d7)) $((16#af)) $((16#d7))"
    "$((16#d7)) $((16#af)) $((16#ff))" "$((16#d7)) $((16#d7)) $((16#00))"
    "$((16#d7)) $((16#d7)) $((16#5f))" "$((16#d7)) $((16#d7)) $((16#87))"
    "$((16#d7)) $((16#d7)) $((16#af))" "$((16#d7)) $((16#d7)) $((16#d7))"
    "$((16#d7)) $((16#d7)) $((16#ff))" "$((16#d7)) $((16#ff)) $((16#00))"
    "$((16#d7)) $((16#ff)) $((16#5f))" "$((16#d7)) $((16#ff)) $((16#87))"
    "$((16#d7)) $((16#ff)) $((16#af))" "$((16#d7)) $((16#ff)) $((16#d7))"
    "$((16#d7)) $((16#ff)) $((16#ff))" "$((16#ff)) $((16#00)) $((16#00))"
    "$((16#ff)) $((16#00)) $((16#5f))" "$((16#ff)) $((16#00)) $((16#87))"
    "$((16#ff)) $((16#00)) $((16#af))" "$((16#ff)) $((16#00)) $((16#d7))"
    "$((16#ff)) $((16#00)) $((16#ff))" "$((16#ff)) $((16#5f)) $((16#00))"
    "$((16#ff)) $((16#5f)) $((16#5f))" "$((16#ff)) $((16#5f)) $((16#87))"
    "$((16#ff)) $((16#5f)) $((16#af))" "$((16#ff)) $((16#5f)) $((16#d7))"
    "$((16#ff)) $((16#5f)) $((16#ff))" "$((16#ff)) $((16#87)) $((16#00))"
    "$((16#ff)) $((16#87)) $((16#5f))" "$((16#ff)) $((16#87)) $((16#87))"
    "$((16#ff)) $((16#87)) $((16#af))" "$((16#ff)) $((16#87)) $((16#d7))"
    "$((16#ff)) $((16#87)) $((16#ff))" "$((16#ff)) $((16#af)) $((16#00))"
    "$((16#ff)) $((16#af)) $((16#5f))" "$((16#ff)) $((16#af)) $((16#87))"
    "$((16#ff)) $((16#af)) $((16#af))" "$((16#ff)) $((16#af)) $((16#d7))"
    "$((16#ff)) $((16#af)) $((16#ff))" "$((16#ff)) $((16#d7)) $((16#00))"
    "$((16#ff)) $((16#d7)) $((16#5f))" "$((16#ff)) $((16#d7)) $((16#87))"
    "$((16#ff)) $((16#d7)) $((16#af))" "$((16#ff)) $((16#d7)) $((16#d7))"
    "$((16#ff)) $((16#d7)) $((16#ff))" "$((16#ff)) $((16#ff)) $((16#00))"
    "$((16#ff)) $((16#ff)) $((16#5f))" "$((16#ff)) $((16#ff)) $((16#87))"
    "$((16#ff)) $((16#ff)) $((16#af))" "$((16#ff)) $((16#ff)) $((16#d7))"
    "$((16#ff)) $((16#ff)) $((16#ff))" "$((16#08)) $((16#08)) $((16#08))"
    "$((16#12)) $((16#12)) $((16#12))" "$((16#1c)) $((16#1c)) $((16#1c))"
    "$((16#26)) $((16#26)) $((16#26))" "$((16#30)) $((16#30)) $((16#30))"
    "$((16#3a)) $((16#3a)) $((16#3a))" "$((16#44)) $((16#44)) $((16#44))"
    "$((16#4e)) $((16#4e)) $((16#4e))" "$((16#58)) $((16#58)) $((16#58))"
    "$((16#62)) $((16#62)) $((16#62))" "$((16#6c)) $((16#6c)) $((16#6c))"
    "$((16#76)) $((16#76)) $((16#76))" "$((16#80)) $((16#80)) $((16#80))"
    "$((16#8a)) $((16#8a)) $((16#8a))" "$((16#94)) $((16#94)) $((16#94))"
    "$((16#9e)) $((16#9e)) $((16#9e))" "$((16#a8)) $((16#a8)) $((16#a8))"
    "$((16#b2)) $((16#b2)) $((16#b2))" "$((16#bc)) $((16#bc)) $((16#bc))"
    "$((16#c6)) $((16#c6)) $((16#c6))" "$((16#d0)) $((16#d0)) $((16#d0))"
    "$((16#da)) $((16#da)) $((16#da))" "$((16#e4)) $((16#e4)) $((16#e4))"
    "$((16#ee)) $((16#ee)) $((16#ee))")

    # 8 bits color names
    declare -gra OP_256COLORNAME=('Black' 'Maroon' 'Green' 'Olive' 'Navy'
    'Purple' 'Teal' 'Silver' 'Grey' 'Red' 'Lime' 'Yellow' 'Blue' 'Fuchsia'
    'Aqua' 'White' 'Grey0' 'NavyBlue' 'DarkBlue' 'Blue3' 'Blue3' 'Blue1'
    'DarkGreen' 'DeepSkyBlue4' 'DeepSkyBlue4' 'DeepSkyBlue4' 'DodgerBlue3'
    'DodgerBlue2' 'Green4' 'SpringGreen4' 'Turquoise4' 'DeepSkyBlue3'
    'DeepSkyBlue3' 'DodgerBlue1' 'Green3' 'SpringGreen3' 'DarkCyan'
    'LightSeaGreen' 'DeepSkyBlue2' 'DeepSkyBlue1' 'Green3' 'SpringGreen3'
    'SpringGreen2' 'Cyan3' 'DarkTurquoise' 'Turquoise2' 'Green1' 'SpringGreen2'
    'SpringGreen1' 'MediumSpringGreen' 'Cyan2' 'Cyan1' 'DarkRed' 'DeepPink4'
    'Purple4' 'Purple4' 'Purple3' 'BlueViolet' 'Orange4' 'Grey37'
    'MediumPurple4' 'SlateBlue3' 'SlateBlue3' 'RoyalBlue1' 'Chartreuse4'
    'DarkSeaGreen4' 'PaleTurquoise4' 'SteelBlue' 'SteelBlue3' 'CornflowerBlue'
    'Chartreuse3' 'DarkSeaGreen4' 'CadetBlue' 'CadetBlue' 'SkyBlue3'
    'SteelBlue1' 'Chartreuse3' 'PaleGreen3' 'SeaGreen3' 'Aquamarine3'
    'MediumTurquoise' 'SteelBlue1' 'Chartreuse2' 'SeaGreen2' 'SeaGreen1'
    'SeaGreen1' 'Aquamarine1' 'DarkSlateGray2' 'DarkRed' 'DeepPink4'
    'DarkMagenta' 'DarkMagenta' 'DarkViolet' 'Purple' 'Orange4' 'LightPink4'
    'Plum4' 'MediumPurple3' 'MediumPurple3' 'SlateBlue1' 'Yellow4' 'Wheat4'
    'Grey53' 'LightSlateGrey' 'MediumPurple' 'LightSlateBlue' 'Yellow4'
    'DarkOliveGreen3' 'DarkSeaGreen' 'LightSkyBlue3' 'LightSkyBlue3' 'SkyBlue2'
    'Chartreuse2' 'DarkOliveGreen3' 'PaleGreen3' 'DarkSeaGreen3'
    'DarkSlateGray3' 'SkyBlue1' 'Chartreuse1' 'LightGreen' 'LightGreen'
    'PaleGreen1' 'Aquamarine1' 'DarkSlateGray1' 'Red3' 'DeepPink4'
    'MediumVioletRed' 'Magenta3' 'DarkViolet' 'Purple' 'DarkOrange3' 'IndianRed'
    'HotPink3' 'MediumOrchid3' 'MediumOrchid' 'MediumPurple2' 'DarkGoldenrod'
    'LightSalmon3' 'RosyBrown' 'Grey63' 'MediumPurple2' 'MediumPurple1' 'Gold3'
    'DarkKhaki' 'NavajoWhite3' 'Grey69' 'LightSteelBlue3' 'LightSteelBlue'
    'Yellow3' 'DarkOliveGreen3' 'DarkSeaGreen3' 'DarkSeaGreen2' 'LightCyan3'
    'LightSkyBlue1' 'GreenYellow' 'DarkOliveGreen2' 'PaleGreen1' 'DarkSeaGreen2'
    'DarkSeaGreen1' 'PaleTurquoise1' 'Red3' 'DeepPink3' 'DeepPink3' 'Magenta3'
    'Magenta3' 'Magenta2' 'DarkOrange3' 'IndianRed' 'HotPink3' 'HotPink2'
    'Orchid' 'MediumOrchid1' 'Orange3' 'LightSalmon3' 'LightPink3' 'Pink3'
    'Plum3' 'Violet' 'Gold3' 'LightGoldenrod3' 'Tan' 'MistyRose3' 'Thistle3'
    'Plum2' 'Yellow3' 'Khaki3' 'LightGoldenrod2' 'LightYellow3' 'Grey84'
    'LightSteelBlue1' 'Yellow2' 'DarkOliveGreen1' 'DarkOliveGreen1'
    'DarkSeaGreen1' 'Honeydew2' 'LightCyan1' 'Red1' 'DeepPink2' 'DeepPink1'
    'DeepPink1' 'Magenta2' 'Magenta1' 'OrangeRed1' 'IndianRed1' 'IndianRed1'
    'HotPink' 'HotPink' 'MediumOrchid1' 'DarkOrange' 'Salmon1' 'LightCoral'
    'PaleVioletRed1' 'Orchid2' 'Orchid1' 'Orange1' 'SandyBrown' 'LightSalmon1'
    'LightPink1' 'Pink1' 'Plum1' 'Gold1' 'LightGoldenrod2' 'LightGoldenrod2'
    'NavajoWhite1' 'MistyRose1' 'Thistle1' 'Yellow1' 'LightGoldenrod1' 'Khaki1'
    'Wheat1' 'Cornsilk1' 'Grey100' 'Grey3' 'Grey7' 'Grey11' 'Grey15' 'Grey19'
    'Grey23' 'Grey27' 'Grey30' 'Grey35' 'Grey39' 'Grey42' 'Grey46' 'Grey50'
    'Grey54' 'Grey58' 'Grey62' 'Grey66' 'Grey70' 'Grey74' 'Grey78' 'Grey82'
    'Grey85' 'Grey89' 'Grey93')

    # STYLE/COLOR CONSTANTS
    for level in FG BG ; do
        for style in '' DARK FAINT LIGHT ITALIC UNDERLINE BLINK SLOW_BLINK \
            RAPID_BLINK REVERSE CONCEAL CROSSED STRUCK ; do
            
            # Skip some combinations
            case $level:$style in
                BG:ITALIC) continue ;;
                BG:UNDERLINE) continue ;;
                BG:STRUCK) continue ;;
                BG:CROSSED) continue ;;
            esac

            for color in BLACK RED GREEN BROWN YELLOW BLUE PURPLE CYAN WHITE \
                GRAY ; do

                # Skip some combinations
                case $style:$color in
                    :GRAY) continue ;;
                    :*) ;;
                    DARK:BLACK|LIGHT:BLACK) continue ;;
                    DARK:WHITE|LIGHT:WHITE) continue ;;
                    DARK:BROWN|LIGHT:BROWN) continue ;;
                esac

                # Get style as integer
                S=0
                case $style:$color in
                    LIGHT:GRAY)           S=0 ;;
                    DARK:GRAY)            S=1 ;;
                    :BROWN)               S=0 ;;
                    :YELLOW)              S=1 ;;
                    LIGHT:*)              S=1 ;;
                    DARK:*)               S=0 ;;
                    FAINT:*)              S=2 ;;
                    ITALIC:*)             S=3 ;;
                    UNDERLINE:*)          S=4 ;;
                    BLINK:*|SLOW_BLINK:*) S=5 ;;
                    RAPID_BLINK:*)        S=6 ;;
                    REVERSE:*)            S=7 ;;
                    CONCEAL:*)            S=8 ;;
                    CROSSED:*|STRUCK:*)   S=9 ;;
                    *)                    S=0 ;;
                esac

                # Declare constant
                c="OP_$color" # Color code
                fgbg_code=3 # FG/BG code
                [[ $level == BG ]] && fgbg_code=4
                name="OP_${level}_$style${style:+_}$color"
                declare -gr "$name=\e[$S;$fgbg_code${!c}m"
            done
        done
    done
fi

## Convert a 256 colors palette color into an RGB color.
##
## Take the index of a color from the 256 colors palette, and return the
## corresponding HTML RGB color code.
## @arg clr256 The index of the color inside the 256 colors palette.
## @stdout The corresponding HTML RGB color, in the form "#aabbcc".
## @status 1 if the color index is out-of-range, 0 otherwise.
function op_256_to_html_rgb {
    
    local -ri clr256="$1"
    
    # Check index range
    [[ $clr256 -lt 0 || $clr256 -gt 255 ]] && \
        lg_error "Input color index must be between 0 and 255 included." && \
        return 1

    # Get RGB color
    local -r rgb="${OP_256TORGBHEX[$clr256]}"

    echo -n "#$rgb"
    return 0
}

# TODO ERROR Hue is from 0 to 360 (angle), Saturation from 0 to 100 (percentage)
# and Luminance from 0 to 100 (percentage).
## Convert an RGB color into an HSL color.
##
## @arg rgb The RGB color in the hexadecimal form "abcdef".
## @stdout The HSL color in as H S L (e.g.: "0 0 100").
## @status 1 if the input color is not in hexadecimal RGB format, 0 otherwise.
function op_rgb2hsl {

    # Read first argument (RGB)
    declare -a rgb
    readarray -t rgb <<<"$(op_hexrgb2decrgb "$1")"

    # Compute Luminace
    declare -ri max="$(mt_max "${rgb[0]}" "${rgb[1]}" "${rgb[2]}")"
    declare -ri min="$(mt_min "${rgb[0]}" "${rgb[1]}" "${rgb[2]}")"
    declare -ri luminance="$((100 * (min + max)/2 / 255))"
    
    # Compute saturation
    if [[ $max -eq $min ]] ; then
        declare -ri saturation=0
    elif [[ $((max+min)) -le 255 ]] ; then
        declare -ri saturation="$((100 * (max - min) / (max + min)))"
    else
        declare -ri saturation="$((100 * (max - min) / (2*255 - max - min)))"
    fi
    
    # Compute hue
    if [[ $max -eq $min ]] ; then
        declare -ri hue=0
    elif [[ ${rgb[0]} -eq $max ]] ; then
        declare -ri hue="$(((360 + (60 * (rgb[1] - rgb[2])) / (max - min)) \
            % 360))"
    elif [[ ${rgb[1]} -eq $max ]] ; then
        declare -ri hue="$(((480 + (60 * (rgb[2] - rgb[0])) / (max - min)) \
            % 360))"
    else
        declare -ri hue="$(((600 + (60 * (rgb[0] - rgb[1])) / (max - min)) \
            % 360))"
    fi

    # Print HSL
    echo -e "$hue\n$saturation\n$luminance"

    return 0
}

## Convert Hexadecimal RGB color into decimal format.
##
## @arg rgb The RGB color in the hexadecimal form "abcdef".
## @stdout The RGB color in decimal format (e.g.: "45 190 20").
## @status 1 if the input color is not in hexadecimal RGB format, 0 otherwise.
function op_hexrgb2decrgb {

    declare -r rgbhex="$1"

    # Get red, green and blue
    if [[ $rgbhex =~ ^([A-Fa-f0-9]{2})([A-Fa-f0-9]{2})([A-Fa-f0-9]{2})$ ]];then
        declare -r hex_red="${BASH_REMATCH[1]}"
        declare -r hex_green="${BASH_REMATCH[2]}"
        declare -r hex_blue="${BASH_REMATCH[3]}"
    else
        lg_error "Color input \"$rgbhex\" is not in hexadecimal RGB format."
        return 1
    fi

    # Convert to decimal
    declare -ri red="$(mt_hex2dec "$hex_red")"
    declare -ri green="$(mt_hex2dec "$hex_green")"
    declare -ri blue="$(mt_hex2dec "$hex_blue")"
    
    echo -e "$red\n$green\n$blue"
    return 0
}

## Print a text in color, on a 256 colors terminal.
##
## @arg fg Foreground color index.
## @arg bg Background color index.
## @arg *  The text to print.
## @status 0.
function op_printf256 {
    
    declare fg="$1"
    declare bg="$2"
    shift 2

    [[ -z $fg ]] || echo -n $'\e[38;5;'"$fg"'m'
    [[ -z $bg ]] || echo -n $'\e[48;5;'"$bg"'m'
    # shellcheck disable=SC2059
    printf "$@"
    [[ -z $fg && -x $bg ]] || echo -n $'\e[0m' # reset settings
    
    return 0
}

# ****************************************************************
# BASH >= 4.4 SECTION
# ****************************************************************
if vr_bash_ge 4.4 ; then

## Find closest 256 color in RGB space.
##
## Find the 256 colors palette's color closest to an RGB color.
## @arg rgb The RGB color in the hexadecimal form "abcdef".
## @arg stdout The index of 256 colors palette's color.
## @status 0.
function op_closest256 {

    # Read first argument (RGB)
    declare -a rgb
    readarray -t rgb <<<"$(op_hexrgb2decrgb "$1")"

    # Loop on all colors of the 256 colors palette
    declare -a x
    declare dist
    declare -i i
    declare -i best=0
    declare best_dist
    for i in $(seq 0 255) ; do

        # Get RGB representation for this color
        rgb_dec="${OP_256TORGBDEC[$i]}"
        readarray -t x <<<"$(tr " " "\n" <<<"$rgb_dec")"
        
        # Compute distance from two colors
        echo "SIZE=${#rgb[@]} rgb=${rgb[*]}">&2
        echo "SIZE=${#x[@]} x=${x[*]}">&2
        dist="$(mt_int_vect_dist_l1 rgb x)"
        
        # Is it better than the current one?
        if  [[ -z $best_dist || $dist -lt $best_dist ]] ; then
            best_dist="$dist"
            best="$i"
        fi
    done

    echo -n "$best"
    return 0
}

## Find closest 256 color in HSL space.
##
## Find the 256 colors palette's color closest to an RGB color.
## @arg rgb The RGB color in the hexadecimal form "abcdef".
## @arg stdout The index of 256 colors palette's color.
## @status 0.
function op_closest256_as_hsl {
    
    declare -r rgbhex="$1"

    # Get input color as HSL
    declare -a hsl
    readarray -t hsl <<<"$(op_rgb2hsl "$rgbhex")" || return 1

    # Loop on all colors of the 256 colors palette
    declare -a x
    declare dist
    declare -i i
    declare -i best=0
    declare best_dist
    for i in $(seq 0 255) ; do

        # Get HSL representation for this color
        readarray -t x <<<"$(op_rgb2hsl "${OP_256TORGBHEX[$i]}")"
        
        # Compute distance from two colors
        dist="$(mt_int_vect_dist_l1 hsl x)"
        
        # Is it better than the current one?
        if  [[ -z $best_dist || $dist -lt $best_dist ]] ; then
            best_dist="$dist"
            best="$i"
        fi
    done

    echo -n "$best"
    return 0
}

fi
true
