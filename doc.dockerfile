FROM ubuntu:22.04
# vi: ft=dockerfile

# Install dependencies
RUN apt-get update
RUN apt-get install -y make netsurf-gtk tidy

# Install Firefox
#ENV FF_VER 119.0b4
#ENV FF_DIR /dist
#RUN apt upgrade -y && apt install -y wget bzip2 libgtk-3-common libasound2 libdbus-glib-1-2
#RUN mkdir -p $FF_DIR && cd $FF_DIR && wget -O - https://ftp.mozilla.org/pub/firefox/releases/$FF_VER/linux-x86_64/en-US/firefox-$FF_VER.tar.bz2 | tar -xjf -
#ENV PATH $FF_DIR/firefox:$PATH

# Set local variables
ARG user=john
ARG group=$user
ARG home=/home/$user
ARG dir=$home/bash-lib

# Create user and group
RUN groupadd -g 1000 $group
RUN useradd -d $home -s /bin/bash -m $group -u 1000 -g 1000

# Copy files
COPY --chown=$user:$group Makefile index.md *.sh *.css gendoc $dir/

# Set user
USER $user
ENV HOME=$home

# Build HTML
WORKDIR $dir
RUN make doc

# Open main page in Firefox
CMD netsurf-gtk $HOME/bash-lib/public/index.html
