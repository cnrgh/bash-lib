# vi: ft=bash
# shellcheck disable=SC2034
# The name/prefix of this module is IN (INfo):
#  * `in_`  for the public functions.
#  * `IN_`  for the public global variables or constants.
#  * `_in_` for the private functions.
#  * `_IN_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_IN_SOURCED ]] || return 0
    _IN_SOURCED=1
fi

declare -r IN_BASHLIB_VER_MAJOR=0
declare -r IN_BASHLIB_VER_MINOR=5
declare -r IN_BASHLIB_VER_PATCH=0
declare -r IN_BASHLIB_VERSION="${IN_BASHLIB_VER_MAJOR}.${IN_BASHLIB_VER_MINOR}.${IN_BASHLIB_VER_PATCH}"

true
