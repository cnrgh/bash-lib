# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing Old Lib's array module"

source "$SCRIPT_DIR/../deprecate.sh"
dp_disable_depreaction_messages
source "$SCRIPT_DIR/../src/array.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/array.sh" || return 1
    
    return 0
}

function test_100_join_by {
    
    tt_expect_output_eq '1,2,3,4' array::join_by ',' 1 2 3 4 || return 1
    tt_expect_output_eq '1234' array::join_by '' 1 2 3 4 || return 2
    tt_expect_output_eq 't 1,t 2,t 3,t 4' array::join_by ',' \
        't 1' 't 2' 't 3' 't 4' || return 3
    tt_expect_output_eq '' array::join_by ',' || return 4
    tt_expect_output_eq '1' array::join_by ',' 1 || return 5
    tt_expect_output_eq '1_2' array::join_by '_' 1 2 || return 6
    tt_expect_output_eq '1, 2, 3' array::join_by ', ' 1 2 3 || return 7
    tt_expect_output_eq 'c1_c3_c2_c5_c78' array::join_by --before 'c' '_' \
        1 3 2 5 78 || return 8
    tt_expect_output_eq '1, 3, 2, 5, 78,' array::join_by --after ',' ' ' \
        1 3 2 5 78 || return 9
    tt_expect_output_eq '"1" "3" "2" "5" "78"' array::join_by --before '"' \
        --after '"' ' ' 1 3 2 5 78 || return 10
    tt_expect_output_eq "<1>,<2>,<3>,<4>" array::join_by --before "<" \
        --after ">" "," 1 2 3 4
    tt_expect_failure array::join_by --wrong_option || return 11
  
    return 0
}
