tt_context "Testing testthat library"

function echo_stdout {
    echo "$@"
}

function echo_stderr {
    echo "$@" >&2
}

function echo_stdout_and_stderr {
    # shellcheck disable=SC2086
    echo $1 "$2"
    # shellcheck disable=SC2086
    echo $3 "$4" >&2
}

function test_empty_output {

    local -i i=0

    for out in stdout stderr ; do
        tt_expect_success tt_expect_empty_$out echo_$out -n || return $((++i))
        tt_expect_failure tt_expect_empty_$out echo_$out ABC || return $((++i))
    done

    # Test deprecated function name
    tt_expect_success tt_expect_empty_output echo -n || return $((++i))

    return 0
}

function test_non_empty_output {

    local -i i=0

    for out in stdout stderr ; do
        tt_expect_success tt_expect_non_empty_$out echo_$out ABC \
            || return $((++i))
        tt_expect_failure tt_expect_non_empty_$out echo_$out -n \
            || return $((++i))
    done

    # Test deprecated function name
    tt_expect_success tt_expect_non_empty_output echo ABC || return $((++i))

    return 0
}

function test_output_eq {

    local -i i=0
    local -r cr="
"

    for out in stdout stderr ; do
        tt_expect_success tt_expect_${out}_eq "" echo_$out -n \
            || return $((++i))
        tt_expect_success tt_expect_${out}_eq "ABC" echo_$out -n ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_eq "1" echo_$out -n \
            || return $((++i))
        tt_expect_success tt_expect_${out}_eq 'A\nBC' echo_$out -n 'A\nBC' \
            || return $((++i))
        tt_expect_success tt_expect_${out}_eq "A${cr}BC" echo_$out -ne "A\nBC" \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_eq 'A\nBC' echo_$out -ne 'A\nBC' \
            || return $((++i))
        tt_expect_success tt_expect_${out}_eq "ABC" echo_$out ABC \
            || return $((++i))
        tt_expect_success tt_expect_${out}_eq "" echo_$out || return $((++i))
    done

    # Test deprecated function name
    tt_expect_success tt_expect_output_eq "" echo -n || return $((++i))

    return 0
}

function test_output_ne {

    local -i i=0
    local -r cr="
"

    for out in stdout stderr ; do
        tt_expect_failure tt_expect_${out}_ne "" echo_$out -n \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_ne "ABC" echo_$out -n ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_ne 'A\nBC' echo_$out -n 'A\nBC' \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_ne "A${cr}BC" echo_$out -ne "A\nBC" \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_ne "ABC" echo_$out ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_ne "" echo_$out || return $((++i))
        tt_expect_success tt_expect_${out}_ne "1" echo_$out -n \
            || return $((++i))
        tt_expect_success tt_expect_${out}_ne 'A\nBC' echo_$out -ne 'A\nBC' \
            || return $((++i))
    done

    # Test deprecated function name
    tt_expect_failure tt_expect_output_ne "" echo -n || return $((++i))

    return 0
}

function test_output_esc_eq {

    for out in stdout stderr ; do
        tt_expect_success tt_expect_${out}_esc_eq "" echo_$out -n \
            || return 1
        tt_expect_success tt_expect_${out}_esc_eq "ABC" echo_$out -n ABC \
            || return 2
        tt_expect_failure tt_expect_${out}_esc_eq "1" echo_$out -n \
            || return 3
        tt_expect_success tt_expect_${out}_esc_eq "A\tBC" echo_$out -ne "A\tBC" \
            || return 4
        tt_expect_success tt_expect_${out}_esc_eq "A\nBC" echo_$out -ne "A\nBC" \
            || return 5
        tt_expect_success tt_expect_${out}_esc_eq "ABC\n" echo_$out ABC \
            || return 6
        tt_expect_success tt_expect_${out}_esc_eq "\n" echo_$out \
            || return 7
    done

    # Test both stdout and stderr together
    out="stdout_and_stderr"
    tt_expect_success tt_expect_${out}_esc_eq "ab" echo_$out -n "a" -n "b" \
        || return 8
    tt_expect_success tt_expect_${out}_esc_eq "a\nb\n" echo_$out "" "a" "" "b" \
        || return 9
        
    # Test deprecated function name
    tt_expect_success tt_expect_output_esc_eq "" echo -n || return 10

    return 0
}

function test_output_esc_ne {

    local -i i=0

    for out in stdout stderr ; do
        tt_expect_failure tt_expect_${out}_esc_ne "" echo_$out -n \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_esc_ne "ABC" echo_$out -n ABC \
            || return $((++i))
        tt_expect_success tt_expect_${out}_esc_ne "1" echo_$out -n \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_esc_ne "A\nBC" echo_$out -ne "A\nBC"\
            || return $((++i))
        tt_expect_failure tt_expect_${out}_esc_ne "ABC\n" echo_$out ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_esc_ne "\n" echo_$out \
            || return $((++i))
    done

    # Test deprecated function name
    tt_expect_failure tt_expect_output_esc_ne "" echo -n || return $((++i))

    return 0
}

function test_output_nlines_eq {

    local -i i=0

    for out in stdout stderr ; do
        tt_expect_success tt_expect_${out}_nlines_eq 0 echo_$out -n \
            || return $((++i))
        tt_expect_success tt_expect_${out}_nlines_eq 1 echo_$out ABC \
            || return $((++i))
        tt_expect_success tt_expect_${out}_nlines_eq 2 echo_$out -e "A\nBC" \
            || return $((++i))
        tt_expect_success tt_expect_${out}_nlines_eq 3 echo_$out -e "A\nB\nC" \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_nlines_eq 2 echo_$out ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_nlines_eq 0 echo_$out ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_nlines_eq 1 echo_$out -n \
            || return $((++i))
    done

    # Test deprecated function name
    tt_expect_success tt_expect_output_nlines_eq 0 echo -n || return $((++i))

    return 0
}

function test_output_nlines_ge {

    local -i i=0

    for out in stdout stderr ; do
        tt_expect_success tt_expect_${out}_nlines_ge 0 echo_$out -n \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_nlines_ge 1 echo_$out -n \
            || return $((++i))
        tt_expect_success tt_expect_${out}_nlines_ge 1 echo_$out ABC \
            || return $((++i))
        tt_expect_success tt_expect_${out}_nlines_ge 0 echo_$out ABC \
            || return $((++i))
        tt_expect_success tt_expect_${out}_nlines_ge 0 echo_$out -e "A\nBC" \
            || return $((++i))
        tt_expect_success tt_expect_${out}_nlines_ge 1 echo_$out -e "A\nBC" \
            || return $((++i))
        tt_expect_success tt_expect_${out}_nlines_ge 2 echo_$out -e "A\nBC" \
            || return $((++i))
        tt_expect_success tt_expect_${out}_nlines_ge 3 echo_$out -e "A\nB\nC" \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_nlines_ge 2 echo_$out ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_nlines_ge 3 echo_$out ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_nlines_ge 3 echo_$out -e "A\nBC" \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_nlines_ge 4 echo_$out -e "A\nB\nC" \
            || return $((++i))
    done

    # Test deprecated function name
    tt_expect_success tt_expect_output_nlines_ge 0 echo -n || return $((++i))

    return 0
}

function test_output_re {

    local -i i=0

    for out in stdout stderr ; do
        tt_expect_success tt_expect_${out}_re ".*" echo_$out -n \
            || return $((++i))
        tt_expect_success tt_expect_${out}_re "A.C" echo_$out -n ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_re "." echo_$out -n \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_re "^[B-C]+" echo_$out -n ABC \
            || return $((++i))
        tt_expect_success tt_expect_${out}_re "[A-C]*" echo_$out -n ABC \
            || return $((++i))
    done

    # Test deprecated function name
    tt_expect_success tt_expect_output_re ".*" echo -n || return $((++i))

    return 0
}

function test_output_not_re {

    local -i i=0

    for out in stdout stderr ; do
        tt_expect_failure tt_expect_${out}_not_re ".*" echo_$out -n \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_not_re "A.C" echo_$out -n ABC \
            || return $((++i))
        tt_expect_success tt_expect_${out}_not_re "." echo_$out -n \
            || return $((++i))
        tt_expect_success tt_expect_${out}_not_re "^[B-C]+" echo_$out -n ABC \
            || return $((++i))
        tt_expect_failure tt_expect_${out}_not_re "[A-C]*" echo_$out -n ABC \
            || return $((++i))
    done

    # Test deprecated function name
    tt_expect_failure tt_expect_output_not_re ".*" echo -n || return $((++i))

    return 0
}
