# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing deprecate library"

_DP_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../deprecate.sh"

function test_010_sourcing {

    _DP_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../deprecate.sh" || return 1
    
    return 0
}
