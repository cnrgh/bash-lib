# vi: ft=bash
# shellcheck disable=SC1091
# shellcheck disable=SC2034

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing introspect library"

_IN_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../introspect.sh"
source "$SCRIPT_DIR/../ver.sh"

function test_010_sourcing {

    _IN_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../introspect.sh" || return 1

    return 0
}

function test_100_in_get_var_flags {

    local      str_var=
    local -i   int_var=0
    local -r   ro_var='foo'
    local -ri  ro_int_var=10
    local -ar  ro_arr=( 1 2 3 )
    local -air ro_int_arr=( 1 2 3 )
    local -A   dict=([a]=1 [b]=2 [c]=3)

    # Module introspect.sh does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0
    
    tt_expect_failure         in_get_var_flags || return 1
    tt_expect_output_eq ''    in_get_var_flags undeclared_var || return 2
    tt_expect_output_eq '-'   in_get_var_flags str_var || return 3
    tt_expect_output_eq 'i'   in_get_var_flags int_var || return 4
    tt_expect_output_eq 'r'   in_get_var_flags ro_var || return 5
    tt_expect_output_eq 'ir'  in_get_var_flags ro_int_var || return 6
    tt_expect_output_eq 'ar'  in_get_var_flags ro_arr || return 7
    tt_expect_output_eq 'air' in_get_var_flags ro_int_arr || return 8
    tt_expect_output_eq 'A'   in_get_var_flags dict || return 9
    tt_expect_output_eq 'f'   in_get_var_flags in_get_var_flags || return 10

    return 0
}

function test_110_in_is_declared {

    local var

    # Module introspect.sh does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    tt_expect_failure in_is_declared || return 1
    tt_expect_failure in_is_declared undeclared_var || return 2
    tt_expect_success in_is_declared var || return 3
    tt_expect_success in_is_declared in_is_declared || return 4

    return 0
}

function test_120_in_typeof {

    local      str_var=
    local -i   int_var=0
    local -r   ro_var='foo'
    local -ri  ro_int_var=10
    local -ar  ro_arr=( 1 2 3 )
    local -air ro_int_arr=( 1 2 3 )
    local -A   dict=([a]=1 [b]=2 [c]=3)
    if vr_bash_ge 4.3.0 ; then
        local -n   name_ref=str_var
    fi

    # Module introspect.sh does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    tt_expect_failure         in_typeof || return 1
    tt_expect_failure         in_typeof undeclared_var || return 2
    tt_expect_output_eq 'str' in_typeof str_var || return 3
    tt_expect_output_eq 'int' in_typeof int_var || return 4
    tt_expect_output_eq 'str' in_typeof ro_var || return 5
    tt_expect_output_eq 'int' in_typeof ro_int_var || return 6
    tt_expect_output_eq 'array' in_typeof ro_arr || return 7
    tt_expect_output_eq 'array' in_typeof ro_int_arr || return 8
    tt_expect_output_eq 'dict' in_typeof dict || return 9
    tt_expect_output_eq 'fct' in_typeof in_typeof || return 10
    tt_expect_output_eq 'nameref' in_typeof name_ref || return 11

    return 0
}

function test_130_in_is {

    local      str_var
    local -i   int_var=0
    local -r   ro_var='foo'
    local -ri  ro_int_var=10
    local -ar  ro_arr=( 1 2 3 )
    local -air ro_int_arr=( 1 2 3 )
    local -A   dict=([a]=1 [b]=2 [c]=3)
    if vr_bash_ge 4.3.0 ; then
        local -n   name_ref=str_var
    fi

    # Module introspect.sh does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    tt_expect_failure       in_is || return 1
    tt_expect_failure       in_is undeclared_var || return 2
    tt_expect_failure       in_is 'unknown_type' undeclared_var || return 3
    tt_expect_failure       in_is 'int' undeclared_var || return 4

    tt_expect_success in_is 'str' str_var || return 10
    tt_expect_success in_is 'int' int_var || return 11
    tt_expect_success in_is 'str' ro_var || return 12
    tt_expect_success in_is 'ro' ro_var || return 13
    tt_expect_success in_is 'int' ro_int_var || return 14
    tt_expect_success in_is 'ro' ro_int_var || return 15
    tt_expect_success in_is 'array' ro_arr || return 16
    tt_expect_success in_is 'ro' ro_arr || return 17
    tt_expect_success in_is 'str' ro_arr || return 18
    tt_expect_success in_is 'array' ro_int_arr || return 19
    tt_expect_success in_is 'int' ro_int_arr || return 20
    tt_expect_success in_is 'ro' ro_int_arr || return 21
    tt_expect_success in_is 'dict' dict || return 22
    tt_expect_success in_is 'str' dict || return 23
    tt_expect_success in_is 'nameref' name_ref || return 24
    tt_expect_success in_is 'fct' in_is || return 25
    
    tt_expect_failure in_is 'int' str_var || return 40

    return 0
}
