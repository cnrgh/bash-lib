# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing Old Lib's command module"

source "$SCRIPT_DIR/../src/command.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/command.sh" || return 1
    
    return 0
}

function test_050_die {
    
    tt_expect_failure command::die 'Error !' || return 1

    return 0
}

function test_100_run {

    tt_expect_success command::run ls || return 1
    tt_expect_failure command::run SomeUnknownCommand || return 2
    
    return 0
}

function test_200_exists {
    
    tt_expect_output_eq true command::exists ls || return 1
    tt_expect_output_eq false command::exists SomeUnknownCommand || return 2

    return 0
}
