# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing boolean library"

_BO_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../bool.sh"

declare -a _TRUE_VALUES=(true TRUE True trUE on ON yes YES 1 12 x)
declare -a _FALSE_VALUES=(false off FALSE False 0 "")

function test_010_sourcing {

    _BO_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../bool.sh" || return 1
    
    return 0
}

function test_100_constants {

    tt_expect_num_eq "$BO_TRUE" 1 || return 1
    tt_expect_num_eq "$BO_FALSE" 0 || return 2

    return 0
}

function test_200_bo_is_false {

    for v in "${_TRUE_VALUES[@]}" ; do
        tt_expect_failure bo_is_false "$v" || return 1
    done

    for v in "${_FALSE_VALUES[@]}" ; do
        tt_expect_success bo_is_false "$v" || return 2
    done
    
    return 0
}

function test_210_bo_is_true {

    for v in "${_TRUE_VALUES[@]}" ; do
        tt_expect_success bo_is_true "$v" || return 1
    done

    for v in "${_FALSE_VALUES[@]}" ; do
        tt_expect_failure bo_is_true "$v" || return 2
    done

    return 0
}

function test_220_bo_is_off {

    for v in "${_TRUE_VALUES[@]}" ; do
        tt_expect_failure bo_is_off "$v" || return 1
    done

    for v in "${_FALSE_VALUES[@]}" ; do
        tt_expect_success bo_is_off "$v" || return 2
    done
    
    return 0
}

function test_230_bo_is_on {

    for v in "${_TRUE_VALUES[@]}" ; do
        tt_expect_success bo_is_on "$v" || return 1
    done

    for v in "${_FALSE_VALUES[@]}" ; do
        tt_expect_failure bo_is_on "$v" || return 2
    done

    return 0
}

function test_300_bo_not {

    tt_expect_output_eq "no" bo_not "yes" || return 1
    tt_expect_output_eq "No" bo_not "Yes" || return 2
    tt_expect_output_eq "NO" bo_not "YES" || return 3
    tt_expect_output_eq "yes" bo_not "no" || return 4
    tt_expect_output_eq "Yes" bo_not "No" || return 5
    tt_expect_output_eq "YES" bo_not "NO" || return 6
    tt_expect_output_eq "false" bo_not "true" || return 7
    tt_expect_output_eq "False" bo_not "True" || return 8
    tt_expect_output_eq "FALSE" bo_not "TRUE" || return 9
    tt_expect_output_eq "true" bo_not "false" || return 10
    tt_expect_output_eq "True" bo_not "False" || return 11
    tt_expect_output_eq "TRUE" bo_not "FALSE" || return 12
    tt_expect_output_eq "on" bo_not "off" || return 13
    tt_expect_output_eq "On" bo_not "Off" || return 14
    tt_expect_output_eq "ON" bo_not "OFF" || return 15
    tt_expect_output_eq "off" bo_not "on" || return 16
    tt_expect_output_eq "Off" bo_not "On" || return 17
    tt_expect_output_eq "OFF" bo_not "ON" || return 18
    tt_expect_output_eq 1 bo_not 0 || return 19
    tt_expect_output_eq 0 bo_not 10 || return 20
    tt_expect_output_eq 0 bo_not -1 || return 21
    tt_expect_output_eq "true" bo_not "" || return 22
    
    return 0
}

function test_310_bo_and {

    tt_expect_output_eq "true" bo_and "true" "true" || return 1
    tt_expect_output_eq "true" bo_and "true" "on" || return 2
    tt_expect_output_eq "TRUE" bo_and "TRUE" "on" || return 3
    tt_expect_output_eq "on" bo_and "on" "true" || return 4
    tt_expect_output_eq "ON" bo_and "ON" "true" || return 5
    tt_expect_output_eq "false" bo_and "true" "false" || return 6
    tt_expect_output_eq "false" bo_and "true" "False" || return 7
    tt_expect_output_eq "false" bo_and "true" "FALSE" || return 8
    tt_expect_output_eq "false" bo_and "true" "off" || return 9
    tt_expect_output_eq "false" bo_and "true" "true" "false" || return 10

    return 0
}

function test_320_bo_or {

    tt_expect_output_eq "true" bo_or "true" "true" || return 1
    tt_expect_output_eq "true" bo_or "true" "on" || return 2
    tt_expect_output_eq "TRUE" bo_or "TRUE" "on" || return 3
    tt_expect_output_eq "on" bo_or "on" "true" || return 4
    tt_expect_output_eq "ON" bo_or "ON" "true" || return 5
    tt_expect_output_eq "true" bo_or "true" "false" || return 6
    tt_expect_output_eq "true" bo_or "true" "False" || return 7
    tt_expect_output_eq "true" bo_or "true" "FALSE" || return 8
    tt_expect_output_eq "true" bo_or "true" "off" || return 9
    tt_expect_output_eq "true" bo_or "true" "true" "false" || return 10
    tt_expect_output_eq "false" bo_or "false" "false" || return 11
    tt_expect_output_eq "false" bo_or "false" "false" "false" || return 12

    return 0
}

function test_330_bo_xor {

    tt_expect_output_eq "false" bo_xor "true"  "true"  || return 1
    tt_expect_output_eq "false" bo_xor "true"  "on"    || return 2
    tt_expect_output_eq "FALSE" bo_xor "TRUE"  "on"    || return 3
    tt_expect_output_eq "off"   bo_xor "on"    "true"  || return 4
    tt_expect_output_eq "OFF"   bo_xor "ON"    "true"  || return 5
    tt_expect_output_eq "true"  bo_xor "true"  "false" || return 6
    tt_expect_output_eq "true"  bo_xor "true"  "False" || return 7
    tt_expect_output_eq "true"  bo_xor "true"  "FALSE" || return 8
    tt_expect_output_eq "true"  bo_xor "true"  "off"   || return 9
    tt_expect_output_eq "false" bo_xor "false" "false" || return 10
    tt_expect_output_eq "true"  bo_xor "false" "true"  || return 11
    tt_expect_output_eq "True"  bo_xor "False" "true"  || return 12
    tt_expect_output_eq "TRUE"  bo_xor "FALSE" "true"  || return 13
    tt_expect_output_eq "on"    bo_xor "off"   "true"  || return 14

    return 0
}

function test_400_bo_all_true {

    tt_expect_success bo_all_true "${_TRUE_VALUES[0]}" || return 1
    tt_expect_success bo_all_true "${_TRUE_VALUES[@]}" || return 2

    tt_expect_failure bo_all_true || return 3
    tt_expect_failure bo_all_true "${_FALSE_VALUES[0]}" || return 4
    tt_expect_failure bo_all_true "${_TRUE_VALUES[@]}" "${_FALSE_VALUES[0]}" \
        || return 5
    tt_expect_failure bo_all_true "${_FALSE_VALUES[0]}" "${_TRUE_VALUES[@]}" \
        || return 6

    return 0
}

function test_410_bo_all_false {

    tt_expect_success bo_all_false "${_FALSE_VALUES[0]}" || return 1
    tt_expect_success bo_all_false "${_FALSE_VALUES[@]}" || return 2

    tt_expect_failure bo_all_false || return 3
    tt_expect_failure bo_all_false "${_TRUE_VALUES[0]}" || return 4
    tt_expect_failure bo_all_false "${_FALSE_VALUES[@]}" "${_TRUE_VALUES[0]}" \
        || return 5
    tt_expect_failure bo_all_false "${_TRUE_VALUES[0]}" "${_FALSE_VALUES[@]}" \
        || return 6

    return 0
}

function test_420_bo_any_false {

    tt_expect_success bo_any_false "${_FALSE_VALUES[0]}" || return 1
    tt_expect_success bo_any_false "${_FALSE_VALUES[@]}" || return 2
    tt_expect_success bo_any_false "${_FALSE_VALUES[@]}" "${_TRUE_VALUES[0]}" \
        || return 3
    tt_expect_success bo_any_false "${_TRUE_VALUES[0]}" "${_FALSE_VALUES[@]}" \
        || return 4
    tt_expect_success bo_any_false "${_TRUE_VALUES[@]}" "${_FALSE_VALUES[0]}" \
        || return 5
    tt_expect_success bo_any_false "${_FALSE_VALUES[0]}" "${_TRUE_VALUES[@]}" \
        || return 6

    tt_expect_failure bo_any_false || return 7
    tt_expect_failure bo_any_false "${_TRUE_VALUES[0]}" || return 8

    return 0
}

function test_430_bo_any_true {

    tt_expect_success bo_any_true "${_TRUE_VALUES[0]}" || return 1
    tt_expect_success bo_any_true "${_TRUE_VALUES[@]}" || return 2
    tt_expect_success bo_any_true "${_TRUE_VALUES[@]}" "${_FALSE_VALUES[0]}" \
        || return 3
    tt_expect_success bo_any_true "${_FALSE_VALUES[0]}" "${_TRUE_VALUES[@]}" \
        || return 4
    tt_expect_success bo_any_true "${_FALSE_VALUES[@]}" "${_TRUE_VALUES[0]}" \
        || return 5
    tt_expect_success bo_any_true "${_TRUE_VALUES[0]}" "${_FALSE_VALUES[@]}" \
        || return 6

    tt_expect_failure bo_any_true || return 7
    tt_expect_failure bo_any_true "${_FALSE_VALUES[0]}" || return 8

    return 0
}

function test_500_bool_conversion {

    tt_expect_output_eq "true" bo_as_lc_bool 1 || return 1
    tt_expect_output_eq "false" bo_as_lc_bool 0 || return 2
    tt_expect_output_eq "True" bo_as_ucfirst_bool 1 || return 3
    tt_expect_output_eq "False" bo_as_ucfirst_bool 0 || return 4
    tt_expect_output_eq "TRUE" bo_as_uc_bool 1 || return 5
    tt_expect_output_eq "FALSE" bo_as_uc_bool 0 || return 6

    tt_expect_output_eq "yes" bo_as_lc_yesno 1 || return 7
    tt_expect_output_eq "no" bo_as_lc_yesno 0 || return 8
    tt_expect_output_eq "Yes" bo_as_ucfirst_yesno 1 || return 9
    tt_expect_output_eq "No" bo_as_ucfirst_yesno 0 || return 10
    tt_expect_output_eq "YES" bo_as_uc_yesno 1 || return 11
    tt_expect_output_eq "NO" bo_as_uc_yesno 0 || return 12

    tt_expect_output_eq "on" bo_as_lc_onoff 1 || return 13
    tt_expect_output_eq "off" bo_as_lc_onoff 0 || return 14
    tt_expect_output_eq "On" bo_as_ucfirst_onoff 1 || return 15
    tt_expect_output_eq "Off" bo_as_ucfirst_onoff 0 || return 16
    tt_expect_output_eq "ON" bo_as_uc_onoff 1 || return 17
    tt_expect_output_eq "OFF" bo_as_uc_onoff 0 || return 18

    tt_expect_output_eq "1" bo_as_bit TRUE || return 19
    tt_expect_output_eq "0" bo_as_bit FALSE || return 20

    tt_expect_output_eq "1" bo_as_str TRUE || return 19
    tt_expect_output_eq ""  bo_as_str FALSE || return 20

    return 0
}
