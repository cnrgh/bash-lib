# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing string library"

_ST_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../str.sh"

function test_010_sourcing {

    _ST_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../str.sh" || return 1
    
    return 0
}

function test_100_join {

    tt_expect_str_eq "$(st_join)" "" || return 1
    tt_expect_str_eq "$(st_join ,)" "" || return 1
    tt_expect_str_eq "$(st_join "" a)" a || return 1
    tt_expect_str_eq "$(st_join , a)" a || return 1
    tt_expect_str_eq "$(st_join , a b)" a,b || return 1
    tt_expect_str_eq "$(st_join , a b c)" a,b,c || return 1
    tt_expect_str_eq "$(st_join , ab)" ab || return 1
    tt_expect_str_eq "$(st_join , ab c)" ab,c || return 1
    tt_expect_str_eq "$(st_join ', ' ab c)" "ab, c" || return 1
    tt_expect_str_eq "$(st_join ', ' ab c)" "ab, c" || return 1

    tt_expect_stdout_esc_eq "a\nb" st_join "\n" a b || return 1

    return 0
}

function test_110_split {

    tt_expect_stdout_eq "" st_split || return 1
    tt_expect_stdout_eq "a" st_split "" a || return 1
    tt_expect_stdout_eq "a" st_split "," a || return 1
    tt_expect_stdout_eq "a b" st_split "," a,b || return 1

    local -a arr
    st_split "," a,b arr
    tt_expect_success_last_cmd || return 1
    tt_expect_num_eq 2 ${#arr[@]} || return 1
    tt_expect_str_eq "a b" "${arr[*]}" || return 1

    return 0
}

function test_200_get_random {

    tt_expect_str_eq "$(st_get_random 0)" "" || return 1
    tt_expect_str_ne "$(st_get_random)" "" || return 2
    tt_expect_str_len "$(st_get_random)" 8 || return 3
    for i in 0 1 2 3 4 8 16 ; do
        tt_expect_str_len "$(st_get_random $i)" $i || return $((100 + i))
    done

    return 0
}

function test_300_st_search_substr {

    tt_expect_output_eq 0  st_search_substr || return 1
    tt_expect_output_eq -1 st_search_substr "a" || return 2
    tt_expect_output_eq 0  st_search_substr "a" "a" || return 3
    tt_expect_output_eq -1 st_search_substr "b" "a" || return 3
    tt_expect_output_eq 0  st_search_substr "" "a" || return 4
    tt_expect_output_eq 0  st_search_substr "a" "abc" || return 5
    tt_expect_output_eq 1  st_search_substr "a" "bac" || return 6
    tt_expect_output_eq 2  st_search_substr "a" "cba" || return 7
    tt_expect_output_eq -1 st_search_substr "d" "abc" || return 8
    tt_expect_output_eq 1  st_search_substr "bc" "abcd" || return 9

    return 0
}

function test_300_st_is_substr {

    tt_expect_failure st_is_substr "a" || return 1
    tt_expect_failure st_is_substr "b" "a" || return 2
    tt_expect_failure st_is_substr "d" "abc" || return 3
    
    tt_expect_success st_is_substr || return 10
    tt_expect_success st_is_substr "a" "a" || return 11
    tt_expect_success st_is_substr "" "a" || return 12
    tt_expect_success st_is_substr "a" "abc" || return 13
    tt_expect_success st_is_substr "a" "bac" || return 14
    tt_expect_success st_is_substr "a" "cba" || return 15
    tt_expect_success st_is_substr "bc" "abcd" || return 16

    return 0
}

function test_310_st_starts_with {

    tt_expect_success st_starts_with "" "" || return 1
    tt_expect_success st_starts_with "" "a" || return 2
    tt_expect_success st_starts_with "a" "a" || return 3
    tt_expect_failure st_starts_with "b" "a" || return 4
    tt_expect_failure st_starts_with "b" "ab" || return 5
    tt_expect_success st_starts_with "a" "ab" || return 6
    tt_expect_success st_starts_with "ab" "ab" || return 7

    return 0
}

function test_400_st_is_int {

    tt_expect_success st_is_int "1" || return 1
    tt_expect_success st_is_int "-1" || return 2
    tt_expect_success st_is_int "0" || return 3
    tt_expect_success st_is_int "10" || return 4
    tt_expect_success st_is_int "+1" || return 5
 
    tt_expect_failure st_is_int "" || return 100
    tt_expect_failure st_is_int ".0" || return 101
    tt_expect_failure st_is_int "1." || return 102
    tt_expect_failure st_is_int "0." || return 103
    tt_expect_failure st_is_int "1.0" || return 104

    return 0
}

function test_410_st_is_float {

    tt_expect_failure st_is_float "" || return 1
    tt_expect_failure st_is_float "1" || return 2
    tt_expect_failure st_is_float "-1" || return 3
    tt_expect_failure st_is_float "0" || return 4
    tt_expect_failure st_is_float "10" || return 5
    tt_expect_failure st_is_float "+1" || return 6
 
    tt_expect_success st_is_float ".0" || return 101
    tt_expect_success st_is_float "1." || return 102
    tt_expect_success st_is_float "0." || return 103
    tt_expect_success st_is_float "1.0" || return 104
    tt_expect_success st_is_float "-1.0" || return 105
    tt_expect_success st_is_float "+1.0" || return 105

    return 0
}

function test_500_st_rm_first {

    tt_expect_output_eq ''  st_rm_first || return 1
    tt_expect_output_eq 'a' st_rm_first '' 'a' || return 2
    tt_expect_output_eq ''  st_rm_first 'a' 'a' || return 3
    tt_expect_output_eq 'a' st_rm_first 'b' 'a' || return 4
    tt_expect_output_eq 'a' st_rm_first 'b' 'ab' || return 5
    tt_expect_output_eq 'a' st_rm_first 'b' 'ba' || return 6

    return 0
}

function test_500_st_rm_all {

    tt_expect_output_eq ''   st_rm_all || return 1
    tt_expect_output_eq 'a'  st_rm_all '' 'a' || return 2
    tt_expect_output_eq ''   st_rm_all 'a' 'a' || return 3
    tt_expect_output_eq 'a'  st_rm_all 'b' 'a' || return 4
    tt_expect_output_eq 'a'  st_rm_all 'b' 'ab' || return 5
    tt_expect_output_eq 'a'  st_rm_all 'b' 'ba' || return 6
    tt_expect_output_eq 'a'  st_rm_all 'b' 'bab' || return 7
    tt_expect_output_eq 'bb' st_rm_all 'a' 'bab' || return 8

    return 0
}

function test_600_rep {

    local -i i=0

    tt_expect_output_eq ''     st_rep 0 'a'  || return $((++i))
    tt_expect_output_eq 'a'    st_rep 1 'a'  || return $((++i))
    tt_expect_output_eq 'aa'   st_rep 2 'a'  || return $((++i))
    tt_expect_output_eq ''     st_rep 0 'ab' || return $((++i))
    tt_expect_output_eq 'ab'   st_rep 1 'ab' || return $((++i))
    tt_expect_output_eq 'abab' st_rep 2 'ab' || return $((++i))
    tt_expect_output_eq ''     st_rep 0 ' '  || return $((++i))
    tt_expect_output_eq ' '    st_rep 1 ' '  || return $((++i))
    tt_expect_output_eq '  '   st_rep 2 ' '  || return $((++i))
    tt_expect_output_eq ''     st_rep 0 '  ' || return $((++i))
    tt_expect_output_eq '  '   st_rep 1 '  ' || return $((++i))

    return 0
}
