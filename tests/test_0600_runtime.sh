# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing runtime library"

_RT_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../runtime.sh"
source "$SCRIPT_DIR/../ver.sh"

function test_010_sourcing {

    _RT_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../runtime.sh" || return 1
    
    return 0
}

function test_100_rt_get_trap_cmd {

    # Module does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    declare sig=
    declare trap_cmd=
    for sig in SIGUSR1 USR1 SIGTERM TERM SIGQUIT QUIT EXIT ; do
        echo "sig=$sig" >&2

        # No commands
        trap - "$sig" || return 1
        trap_cmd="$(rt_get_trap_cmd "$sig")" || return 1
        tt_expect_str_null "$trap_cmd" || return 1

        trap "true" $sig >&2
        trap -p "$sig" >&2
        # Test some commands
        for cmd in "a" "a b" 'a "b c"' "a 'b c'" ; do
            # shellcheck disable=SC2064
            trap "$cmd" "$sig" || return 1
            trap -p "$sig" >&2
            trap_cmd="$(rt_get_trap_cmd "$sig")" || return 1
            tt_expect_str_eq "$trap_cmd" "$cmd" || return 1
        done
        trap - "$sig" || return 1
    done

    return 0
}

function test_110_rt_append_trap_cmd {

    # Module does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    # No existing command
    for cmd in "a" "a b" 'a "b c"' "a 'b c'" ; do
        trap - SIGUSR1 || return 1
        rt_append_trap_cmd "$cmd" SIGUSR1 || return 2
        tt_expect_stdout_eq "$cmd" rt_get_trap_cmd SIGUSR1 || return 3
    done
    trap - SIGUSR1 || return 4

    # A trap command is already defined
    for cmd1 in "a" "a b" 'a "b c"' "a 'b c'" ; do
        for cmd2 in "a" "a b" 'a "b c"' "a 'b c'" ; do
            # shellcheck disable=SC2064
            trap "$cmd1" SIGUSR1 || return 11
            rt_append_trap_cmd "$cmd2" SIGUSR1 || return 12
            tt_expect_stdout_eq "$cmd1;$cmd2" rt_get_trap_cmd SIGUSR1 \
                || return 13
        done
        trap - SIGUSR1 || return 14
    done

    # With two signals
    for cmd in "a" "a b" 'a "b c"' "a 'b c'" ; do
        trap - SIGUSR1 SIGUSR2 || return 21
        rt_append_trap_cmd "$cmd" SIGUSR1 SIGUSR2 || return 22
        tt_expect_stdout_eq "$cmd" rt_get_trap_cmd SIGUSR1 || return 23
        tt_expect_stdout_eq "$cmd" rt_get_trap_cmd SIGUSR1 || return 24
    done
    trap - SIGUSR1 SIGUSR2 || return 25

    return 0
}

function test_120_rt_insert_trap_cmd {

    # Module does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    # No existing command
    for cmd in "a" "a b" 'a "b c"' "a 'b c'" ; do
        trap - SIGUSR1 || return 1
        rt_insert_trap_cmd "$cmd" SIGUSR1 || return 2
        tt_expect_stdout_eq "$cmd" rt_get_trap_cmd SIGUSR1 || return 3
    done
    trap - SIGUSR1 || return 4

    # A trap command is already defined
    for cmd1 in "a" "a b" 'a "b c"' "a 'b c'" ; do
        for cmd2 in "a" "a b" 'a "b c"' "a 'b c'" ; do
            # shellcheck disable=SC2064
            trap "$cmd1" SIGUSR1 || return 11
            rt_insert_trap_cmd "$cmd2" SIGUSR1 || return 12
            tt_expect_stdout_eq "$cmd2;$cmd1" rt_get_trap_cmd SIGUSR1 \
                || return 13
        done
        trap - SIGUSR1 || return 14
    done

    # With two signals
    for cmd in "a" "a b" 'a "b c"' "a 'b c'" ; do
        trap - SIGUSR1 SIGUSR2 || return 21
        rt_insert_trap_cmd "$cmd" SIGUSR1 SIGUSR2 || return 22
        tt_expect_stdout_eq "$cmd" rt_get_trap_cmd SIGUSR1 || return 23
        tt_expect_stdout_eq "$cmd" rt_get_trap_cmd SIGUSR1 || return 24
    done
    trap - SIGUSR1 SIGUSR2 || return 25

    return 0
}

function test_130_rt_safe_append_trap_cmd {

    # Module does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    # SIGINT
    declare -r prefix="trap - INT;set +ex;(($_RT_PRE_CMD_END))"
    declare -r suffix="(($_RT_POST_CMD_START));exit 130"
    trap - SIGINT || return 1
    rt_safe_append_trap_cmd "true" SIGINT || return 2
    tt_expect_stdout_eq "$prefix;true;$suffix" rt_get_trap_cmd SIGINT || return 3
    rt_safe_append_trap_cmd "((a+b))" SIGINT || return 4
    tt_expect_stdout_eq "$prefix;true;((a+b));$suffix" rt_get_trap_cmd SIGINT \
        || return 5
    trap - SIGINT || return 6

    return 0
}

function test_140_default_redirection_to_exit {

    # Module does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    declare -r sig_fct=true
    declare term_fct
    declare quit_fct
    declare int_fct
    declare prefix
    declare suffix

    for first_sig in "" SIGINT SIGTERM SIGQUIT ; do
        echo "FIRST_SIG=$first_sig" >&2

        # Reset signals
        trap - EXIT SIGINT SIGTERM SIGQUIT || return 1

        # Define one signal
        [[ -z $first_sig ]] || rt_safe_append_trap_cmd "$sig_fct" $first_sig \
            || return 2
        term_fct=
        quit_fct=
        int_fct=
        [[ $first_sig == SIGINT ]] && int_fct="$sig_fct"
        [[ $first_sig == SIGQUIT ]] && quit_fct="$sig_fct"
        [[ $first_sig == SIGTERM ]] && term_fct="$sig_fct"

        # Set EXIT signal, undefined signals should get a default definition
        rt_safe_append_trap_cmd "false" EXIT || return 3

        # Check EXIT
        prefix="rc=\$?;set +ex;(($_RT_PRE_CMD_END))"
        suffix="(($_RT_POST_CMD_START));_rt_exit_trap_done \$\$ \$rc"
        declare trap_cmd=
        trap_cmd="$(rt_get_trap_cmd EXIT)" || return 1
        tt_expect_str_eq "$trap_cmd" "$prefix;false;$suffix" || return 1

        # Check SIGTERM
        prefix="set +ex;(($_RT_PRE_CMD_END))"
        if [[ $first_sig == SIGTERM ]] ; then
            suffix="(($_RT_POST_CMD_START));kill -s SIGTERM \$\$"
        else
            suffix="(($_RT_POST_CMD_START));exit $RT_SC_SIGTERM"
        fi
        trap_cmd="$(rt_get_trap_cmd TERM)" || return 1
        tt_expect_str_eq "$trap_cmd" "$prefix;${term_fct:+$term_fct;}$suffix" \
            || return 1

        # Check SIGQUIT
        prefix="set +ex;(($_RT_PRE_CMD_END))"
        if [[ $first_sig == SIGQUIT ]] ; then
            suffix="(($_RT_POST_CMD_START));kill -s SIGQUIT \$\$"
        else
            suffix="(($_RT_POST_CMD_START));exit $RT_SC_SIGQUIT"
        fi
        trap_cmd="$(rt_get_trap_cmd QUIT)" || return 1
        tt_expect_str_eq "$trap_cmd" "$prefix;${quit_fct:+$quit_fct;}$suffix" \
            || return 1

        # Check SIGINT
        prefix="trap - INT;set +ex;(($_RT_PRE_CMD_END))"
        suffix="(($_RT_POST_CMD_START));exit $RT_SC_SIGINT"
        trap_cmd="$(rt_get_trap_cmd INT)" || return 1
        tt_expect_str_eq "$trap_cmd" "$prefix;${int_fct:+$int_fct;}$suffix" \
            || return 1
    done

    # Reset signals
    trap - SIGINT SIGTERM SIGQUIT EXIT || return 20

    return 0
}

