# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
OUTPUT_DIR="$SCRIPT_DIR/output"

tt_context "Testing Old Lib's output module"

source "$SCRIPT_DIR/../deprecate.sh"
dp_disable_depreaction_messages
source "$SCRIPT_DIR/../src/output.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/output.sh" || return 1

    return 0
}

function test_100_output_error {

    local -r wrk_dir="$OUTPUT_DIR/old_lib_output"
    mkdir -p "$wrk_dir"
    local -r err_file="$wrk_dir/output_error_stderr.txt"
    local -r out_file="$wrk_dir/output_error_stdout.txt"

    output::error 'test' >"$out_file" 2>"$err_file"
    tt_expect_success_last_cmd || return 1
    tt_expect_output_eq "" cat "$out_file" || return 2
    tt_expect_output_esc_eq "[[0;31mERROR[0m] test\n" cat "$err_file" \
        || return 3
  
    return 0
}

function test_200_output_warning {

    local -r wrk_dir="$OUTPUT_DIR/old_lib_output"
    mkdir -p "$wrk_dir"
    local -r err_file="$wrk_dir/output_warning_stderr.txt"
    local -r out_file="$wrk_dir/output_warning_stdout.txt"

    output::warning 'test' >"$out_file" 2>"$err_file"
    tt_expect_success_last_cmd || return 1
    tt_expect_output_eq "" cat "$out_file" || return 2
    tt_expect_output_eq '[[0;33mWARNING[0m] test' cat "$err_file" || \
        return 3
  
    return 0
}

function test_300_output_info {

    local -r wrk_dir="$OUTPUT_DIR/old_lib_output"
    mkdir -p "$wrk_dir"
    local -r err_file="$wrk_dir/output_info_stderr.txt"
    local -r out_file="$wrk_dir/output_info_stdout.txt"

    output::info 'test' >"$out_file" 2>"$err_file"
    tt_expect_success_last_cmd || return 1
    tt_expect_output_eq '' cat "$out_file" || \
        return 2
    tt_expect_output_eq "[[0;36mINFO[0m] test" cat "$err_file" || return 3

    return 0
}

function test_400_output_style {

    tt_expect_output_eq $'\e[0m'"ABC" echo -e "$(output::style reset)ABC" \
        || return 1
    tt_expect_output_eq $'\e[1m'"ABC"$'\e[0m' echo -e \
        "$(output::style bold)ABC$(output::style reset)" || return 1
    tt_expect_output_eq $'\e[3m'"ABC"$'\e[0m' echo -e \
        "$(output::style italic)ABC$(output::style reset)" || return 2
    tt_expect_output_eq $'\e[4m'"ABC"$'\e[0m' echo -e \
        "$(output::style underline)ABC$(output::style reset)" || return 3
    tt_expect_output_eq $'\e[9m'"ABC"$'\e[0m' echo -e \
        "$(output::style strikethrough)ABC$(output::style reset)" || return 4

    tt_expect_failure output::style unknown_style || return 10

    return 0
}

function test_410_output::style::bg {

    # WARNING Confusion betwee foreground and background. The implemented
    # function set the foreground color.

    tt_expect_failure output::style::bg yellow unknown_mode || return 1
    tt_expect_failure output::style::bg unknown_color normal || return 2
    tt_expect_failure output::style::bg unknown_color unknown_mode || return 3

    tt_expect_output_eq $'\e[0;33m'"ABC" echo -e \
        "$(output::style::bg yellow normal)ABC" || return 10
    tt_expect_output_eq $'\e[1;31m'"ABC" echo -e \
        "$(output::style::bg red bold)ABC" || return 11
    
    # XXX Italic, underline and strikethrough backgrounds make no sense !
    tt_expect_output_eq $'\e[3;34m'"ABC" echo -e \
        "$(output::style::bg blue italic)ABC" || return 12
    tt_expect_output_eq $'\e[4;35m'"ABC" echo -e \
        "$(output::style::bg purple underline)ABC" || return 12
    tt_expect_output_eq $'\e[9;36m'"ABC" echo -e \
        "$(output::style::bg cyan strikethrough)ABC" || return 12

    return 0
}

function test_410_output::style::fg {

    # WARNING Confusion betwee foreground and background. The implemented
    # function set the background color.

    tt_expect_failure output::style::fg yellow unknown_mode || return 1
    tt_expect_failure output::style::fg unknown_color normal || return 2
    tt_expect_failure output::style::fg unknown_color unknown_mode || return 3

    tt_expect_output_eq $'\e[0;43m'"ABC" echo -e \
        "$(output::style::fg yellow normal)ABC" || return 10
    tt_expect_output_eq $'\e[1;41m'"ABC" echo -e \
        "$(output::style::fg red bold)ABC" || return 11
    tt_expect_output_eq $'\e[3;44m'"ABC" echo -e \
        "$(output::style::fg blue italic)ABC" || return 12
    tt_expect_output_eq $'\e[4;45m'"ABC" echo -e \
        "$(output::style::fg purple underline)ABC" || return 12
    tt_expect_output_eq $'\e[9;46m'"ABC" echo -e \
        "$(output::style::fg cyan strikethrough)ABC" || return 12

    return 0
}
