# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
OUTPUT_DIR="$SCRIPT_DIR/output"

tt_context "Testing Old Lib's file module"

source "$SCRIPT_DIR/../src/file.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/file.sh" || return 1

    return 0
}

function test_100_exists {
    
    local -r wrk_dir="$OUTPUT_DIR/old_lib_file_exists"
    mkdir -p "$wrk_dir"
    local -r a_file="$wrk_dir/a_file"
    touch "$a_file"
    
    tt_expect_success file::exists "$a_file" || return 1
    tt_expect_failure file::exists "AFileThatDoesNotExist" || return 2
    
    return 0
}

function test_200_get_extension {
    
    tt_expect_output_eq 'fasta' file::get_extension 'test.fasta' || return 1
    tt_expect_output_eq '' file::get_extension 'test.' || return 2
    tt_expect_output_eq '' file::get_extension 'test' || return 3
    
    return 0
}

function test_210_has_extension {
    
    tt_expect_success file::has_extension 'fasta' 'test.fasta' || return 1
    tt_expect_failure file::has_extension 'txt' 'test.fasta' || return 2
    
    return 0
}

function test_220_remove_extension {
    
    tt_expect_output_eq 'test' file::remove_extension 'test.fasta' || return 1
    tt_expect_output_eq 'test' file::remove_extension 'test' || return 2
    
    return 0
}

function test_300_get_tmp {
    
    trap - EXIT SIGQUIT SIGTERM SIGINT
    tt_expect_success file::get_tmp || return 1
    
    return 0
}

function test_400_get_log {

    trap - EXIT SIGQUIT SIGTERM SIGINT
    tt_expect_success file::get_log 'a_file' || return 1

    return 0
}
