# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing Old Lib's dictionary module"

source "$SCRIPT_DIR/../src/dictionary.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/dictionary.sh" || return 1

    return 0
}

function test_100_has_key {
    
    local -Arx my_hash=( [a]=1 [b]=2 )
    tt_expect_failure dictionary::has_key '' my_hash || return 3
    tt_expect_success dictionary::has_key 'a' my_hash || return 1
    tt_expect_failure dictionary::has_key 'c' my_hash || return 2
    tt_expect_failure dictionary::has_key 'c' SomeUnexistentHash || return 4
    tt_expect_failure dictionary::has_key 'c' '' || return 5

    return 0
}

function test_200_get {
    
    local -Arx my_hash=( [a]=1 [b]=2 )

    tt_expect_output_eq 1 dictionary::get a my_hash || return 1
    tt_expect_output_eq 2 dictionary::get b my_hash || return 2
    tt_expect_failure dictionary::get c my_hash || return 3

    return 0
}

function test_300_put {

    local -Ax my_hash=( [a]=1 [b]=2 )

    dictionary::put c 3 my_hash
    tt_expect_success_last_cmd || return 1
    tt_expect_output_eq 3 dictionary::get c my_hash || return 2

    return 0
}
