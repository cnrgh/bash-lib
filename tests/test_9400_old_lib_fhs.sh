# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
OUTPUT_DIR="$SCRIPT_DIR/output"

tt_context "Testing Old Lib's fhs module"

source "$SCRIPT_DIR/../src/fhs.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/fhs.sh" || return 1

    return 0
}

function test_100_prefix {

    local -r wrk_dir="$OUTPUT_DIR/old_lib_fhs_prefix"

    tt_expect_output_eq '/usr' fhs::prefix || return 1

    local -r user_dir=$(realpath "${HOME}"'/.local')
    echo "user_dir=$user_dir" >&2
    echo "fhs::prefix --user=$(fhs::prefix --user)" >&2
    tt_expect_output_eq "$user_dir" fhs::prefix --user || return 2

    local -r local_dir=$(realpath "/usr/local")
    tt_expect_output_eq "$local_dir" fhs::prefix --local || return 3

    mkdir -p "$wrk_dir/usr/bin"
    local save_path="$PATH"
    PATH="${wrk_dir}/usr/bin:${PATH}"
    touch "${wrk_dir}/usr/bin/cnrgh_foo"
    chmod +x "${wrk_dir}/usr/bin/cnrgh_foo"
    local -r exe_dir=$(realpath "${wrk_dir}/usr")
    tt_expect_output_eq "$exe_dir" fhs::prefix --executable cnrgh_foo || \
        return 4
    PATH="${save_path}"

    return 0
}

function test_200_bindir {

    local -r wrk_dir="$OUTPUT_DIR/old_lib_fhs_bindir"

    tt_expect_output_eq '/usr/bin' fhs::bindir || return 1

    local -r user_dir=$(realpath "${HOME}"'/.local/bin')
    tt_expect_output_eq "$user_dir" fhs::bindir --user || return 2
    
    local -r local_dir=$(realpath '/usr/local/bin')
    tt_expect_output_eq "$local_dir" fhs::bindir --local || return 3

    mkdir -p "$wrk_dir/usr/bin"
    local save_path="$PATH"
    PATH="${wrk_dir}/usr/bin:${PATH}"
    touch "${wrk_dir}/usr/bin/cnrgh_foo"
    chmod +x "${wrk_dir}/usr/bin/cnrgh_foo"
    
    local -r prefix_dir=$(realpath "${wrk_dir}/usr/bin")
    tt_expect_output_eq "$prefix_dir" fhs::bindir --prefix "${wrk_dir}/usr" ||\
        return 4
    
    local -r exe_dir=$(realpath "${wrk_dir}/usr/bin")
    tt_expect_output_eq "$exe_dir" fhs::bindir --executable cnrgh_foo || \
        return 5

    PATH="${save_path}"

    return 0
}

function test_300_datadir {

    local -r wrk_dir="$OUTPUT_DIR/old_lib_fhs_datadir"

    tt_expect_output_eq '/usr/share' fhs::datadir || return 1

    local -r user_dir=$(realpath "${HOME}"'/.local/share')
    tt_expect_output_eq "$user_dir" fhs::datadir --user || return 2
    
    local -r local_dir=$(realpath '/usr/local/share')
    tt_expect_output_eq "$local_dir" fhs::datadir --local || return 3

    mkdir -p "$wrk_dir/usr/bin"
    local save_path="$PATH"
    PATH="${wrk_dir}/usr/bin:${PATH}"
    touch "${wrk_dir}/usr/bin/cnrgh_foo"
    chmod +x "${wrk_dir}/usr/bin/cnrgh_foo"
    
    declare prefix_dir
    touch "${wrk_dir}/usr/share" || return 10
    prefix_dir=$(realpath "${wrk_dir}/usr/share") || return 11
    tt_expect_output_eq "$prefix_dir" fhs::datadir --prefix "${wrk_dir}/usr" ||\
        return 4
    
    declare exe_dir
    exe_dir=$(realpath "${wrk_dir}/usr/share") || return 20
    tt_expect_output_eq "$exe_dir" fhs::datadir --executable cnrgh_foo || \
        return 21

    PATH="${save_path}"

    return 0
}

function test_300_confdir {

    # fhs::confdir does not work on Darwin
    [[ $(uname) == Darwin ]] && return 0

    local -r wrk_dir="$OUTPUT_DIR/old_lib_fhs_confdir"

    tt_expect_output_eq '/etc' fhs::confdir || return 1

    local -r user_dir=$(realpath "${HOME}"'/.config')
    tt_expect_output_eq "$user_dir" fhs::confdir --user || return 2
    
    local -r local_dir=$(realpath '/usr/local/etc')
    tt_expect_output_eq "$local_dir" fhs::confdir --local || return 3

    mkdir -p "$wrk_dir/usr/bin"
    local save_path="$PATH"
    PATH="${wrk_dir}/usr/bin:${PATH}"
    touch "${wrk_dir}/usr/bin/cnrgh_foo"
    chmod +x "${wrk_dir}/usr/bin/cnrgh_foo"
    
    local -r prefix_dir=$(realpath "${wrk_dir}/usr/etc")
    tt_expect_output_eq "$prefix_dir" fhs::confdir --prefix "${wrk_dir}/usr" ||\
        return 4
    
    local -r exe_dir=$(realpath "${wrk_dir}/usr/etc")
    tt_expect_output_eq "$exe_dir" fhs::confdir --executable cnrgh_foo || \
        return 5

    PATH="${save_path}"

    return 0
}
