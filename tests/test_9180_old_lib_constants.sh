# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing Old Lib's constants module"

source "$SCRIPT_DIR/../src/constants.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/constants.sh" || return 1

    return 0
}

function test_100_exit {

    tt_expect_num_eq "$EXIT_SUCCESS" 0
    tt_expect_num_eq "$EXIT_FAILURE" 1

    return 0
}

function test_200_tmp {

    # Tmp files and folders constants are deprecated
    tt_expect_str_null "$TMP_FILE_TEMPLATE"
    tt_expect_str_null "$TMP_WORKING_DIR"
    tt_expect_str_null "$TMP_LOG_DIR"

    return 0
}

function test_300_pid {

    tt_expect_str_not_null "$PID"

    return 0
}
