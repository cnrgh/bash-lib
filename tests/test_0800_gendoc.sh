# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
APP="$SCRIPT_DIR/../gendoc"
RES_DIR="$SCRIPT_DIR/res/gendoc"
OUTPUT_DIR="$SCRIPT_DIR/output/gendoc"
mkdir -p "$OUTPUT_DIR"

tt_context "Testing gendoc tool"

function test_010_help {

    local -i i=0

    tt_expect_success "$APP" -h || return $((++i))
    tt_expect_success "$APP" --help || return $((++i))

    return 0
}

function test_020_stdin {

    local -i i=0
    local -r res_dir="$RES_DIR/stdin"
    local -r out_dir="$OUTPUT_DIR/stdin"
    mkdir -p "$out_dir"

    # Loop on all test files
    for f in "$res_dir"/*.sh ; do
        
        # Get test name
        local n
        n="$(basename "$f")"
        n=${n%.sh}
        
        # Loop on output formats
        for fmt in tag html ; do
            "$APP" -c "" -f "$fmt" >"$out_dir/$n.$fmt" <"$f"
            tt_expect_success_last_cmd || return $((++i))
            if [[ $fmt == html ]] ; then
                tt_expect_success tidy -e "$out_dir/$n.html" \
                    || return $((++i))
            fi
            tt_expect_same_files "$res_dir/$n.$fmt" \
                "$out_dir/$n.$fmt" || return $((++i))
        done
    done

    return 0
}

function test_100_char_escape {

    local -i i=0
    
    "$APP" -c "" >"$OUTPUT_DIR/char_escape.html" <"$RES_DIR/char_escape.sh"
    tt_expect_success_last_cmd || return $((++i))
    tt_expect_success tidy -eq "$OUTPUT_DIR/char_escape.html" || return $((++i))
    tt_expect_same_files "$RES_DIR/char_escape.html" "$OUTPUT_DIR/char_escape.html" \
        || return $((++i))

    return 0
}
