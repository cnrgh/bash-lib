# vi: ft=bash
# shellcheck disable=SC1091
# shellcheck disable=SC2034

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing Old Lib's type module"

source "$SCRIPT_DIR/../deprecate.sh"
dp_disable_depreaction_messages
source "$SCRIPT_DIR/../src/type.sh"
source "$SCRIPT_DIR/../ver.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/type.sh" || return 1

    return 0
}

function test_100_type_declaration {

    local str_var
    local -r   expected_read_only='r'
    local -air read_only_array_integer=( 1 2 3 )

    # Module introspect.sh does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0
    
    tt_expect_output_eq '-' type::declaration str_var || return 1
    tt_expect_output_eq '' type::declaration undeclared_var || return 2
    tt_expect_output_eq 'r' type::declaration expected_read_only || return 3
    tt_expect_output_eq 'air' type::declaration read_only_array_integer || \
        return 4

    return 0
}

function test_200_type_typeof {

    # Module introspect.sh does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0
    
    local -ir i=0
    tt_expect_output_eq 'integer' type::typeof i || return 1

    local -r  s=''
    tt_expect_output_eq 'string' type::typeof s || return 2

    local -ar a=()
    tt_expect_output_eq 'array' type::typeof a || return 3

    local -Ar h=()
    tt_expect_output_eq 'hash' type::typeof h || return 4

    return 0  
}

function test_300_type_is {

    # Module introspect.sh does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    local -r s='hello'
    tt_expect_success type::is 'string' s || return 1

    local -i i=1
    tt_expect_success type::is 'integer' i || return 2

    return 0  
}
