# vi: ft=bash
# shellcheck disable=SC1091
# shellcheck disable=SC2034

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing maths library"

_MT_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../maths.sh"

function test_010_sourcing {

    _MT_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../maths.sh" || return 1

    return 0
}

function test_100_mt_div {

    tt_expect_failure mt_div || return 1
    tt_expect_failure mt_div 10 || return 2
    tt_expect_failure mt_div "" 10 || return 3
    tt_expect_failure mt_div 1 0 || return 4

    tt_expect_output_eq '.5' mt_div 1 2 1 || return 5

    return 0
}

function test_110_mt_lt {
    tt_expect_success mt_lt 1 2 || return 1
    tt_expect_failure mt_lt 2 1 || return 1
    tt_expect_failure mt_lt 2 2 || return 1
    return 0
}

function test_200_mt_percent {

    tt_expect_failure mt_percent || return 1
    tt_expect_failure mt_percent 10 || return 2
    tt_expect_failure mt_percent "" 10 || return 3
    tt_expect_failure mt_percent 1 0 || return 4

    tt_expect_output_eq '50' mt_percent 1 2 0 || return 5
    tt_expect_output_eq '100' mt_percent 1 1 0 || return 6
    tt_expect_output_eq '33.33' mt_percent 1 3 2 || return 7

    return 0
}

function test_300_scale_int {

    local -i i=0

    tt_expect_stdout_eq 0 mt_scale_int 0 0 1 0 1  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 1 0 1 0 1  || return $((++i))

    tt_expect_stdout_eq 0 mt_scale_int 0 0 3 0 1  || return $((++i))
    tt_expect_stdout_eq 0 mt_scale_int 1 0 3 0 1  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 2 0 3 0 1  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 3 0 3 0 1  || return $((++i))

    tt_expect_stdout_eq 1 mt_scale_int 0 0 3 1 2  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 1 0 3 1 2  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 2 0 3 1 2  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 3 0 3 1 2  || return $((++i))

    tt_expect_stdout_eq 1 mt_scale_int 1 1 4 1 2  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 2 1 4 1 2  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 3 1 4 1 2  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 4 1 4 1 2  || return $((++i))

    tt_expect_stdout_eq 1 mt_scale_int 0 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 1 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 2 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 3 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 4 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 5 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 6 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 7 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 8 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 9 0 10 1 2  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 10 0 10 1 2  || return $((++i))

    tt_expect_stdout_eq 1 mt_scale_int 1 1 4 1 8  || return $((++i))
    tt_expect_stdout_eq 3 mt_scale_int 2 1 4 1 8  || return $((++i))
    tt_expect_stdout_eq 5 mt_scale_int 3 1 4 1 8  || return $((++i))
    tt_expect_stdout_eq 7 mt_scale_int 4 1 4 1 8  || return $((++i))

    tt_expect_stdout_eq 0 mt_scale_int 1 1 4 0 4  || return $((++i))
    tt_expect_stdout_eq 1 mt_scale_int 2 1 4 0 4  || return $((++i))
    tt_expect_stdout_eq 2 mt_scale_int 3 1 4 0 4  || return $((++i))
    tt_expect_stdout_eq 3 mt_scale_int 4 1 4 0 4  || return $((++i))

    return 0
}

function test_320_rand {

    local -i i=0

    local min=1
    local max=6
    for ((j=0 ; j<10 ; ++j)) ; do
        local n
        n=$(mt_rand "$min" "$max")
        tt_expect_success_last_cmd || return $((++i))
        tt_expect_num_ge "$n" "$min" || return $((++i))
        tt_expect_num_le "$n" "$max" || return $((++i))
    done

    return 0
}

function test_400_max {
    tt_expect_stdout_eq "" mt_max || return 1
    tt_expect_stdout_eq 1 mt_max 1 || return 1
    tt_expect_stdout_eq 1 mt_max 0 1 || return 1
    tt_expect_stdout_eq 0 mt_max 0 -1 || return 1
    tt_expect_stdout_eq -1 mt_max -2 -1 || return 1
    return 0
}

function test_401_min {
    tt_expect_stdout_eq "" mt_min || return 1
    tt_expect_stdout_eq 1 mt_min 1 || return 1
    tt_expect_stdout_eq 0 mt_min 0 1 || return 1
    tt_expect_stdout_eq -1 mt_min 0 -1 || return 1
    tt_expect_stdout_eq -2 mt_min -2 -1 || return 1
    return 0
}

function test_500_mt_hex2dec {
    tt_expect_failure mt_hex2dec || return 1
    tt_expect_failure mt_hex2dec g || return 1
    tt_expect_failure mt_hex2dec z || return 1
    tt_expect_stdout_eq 0 mt_hex2dec 00 || return 1
    tt_expect_stdout_eq 1 mt_hex2dec 1 || return 1
    tt_expect_stdout_eq 1 mt_hex2dec 01 || return 1
    tt_expect_stdout_eq 10 mt_hex2dec a || return 1
    tt_expect_stdout_eq 11 mt_hex2dec b || return 1
    tt_expect_stdout_eq 12 mt_hex2dec c || return 1
    tt_expect_stdout_eq 13 mt_hex2dec d || return 1
    tt_expect_stdout_eq 14 mt_hex2dec e || return 1
    tt_expect_stdout_eq 15 mt_hex2dec f || return 1
    tt_expect_stdout_eq 16 mt_hex2dec 10 || return 1
    tt_expect_stdout_eq 32 mt_hex2dec 20 || return 1
    tt_expect_stdout_eq 255 mt_hex2dec ff || return 1
    tt_expect_stdout_eq 65536 mt_hex2dec 10000 || return 1
    return 0
}

function test_600_mt_int_vect_dist_l1 {
    vr_bash_ge 4.4 || return 0
    declare -a v0=(0)
    declare -a v1=(1)
    declare -a v2=(2)
    declare -a v0_0=(0 0)
    declare -a v1_2=(1 2)
    declare -a v2_2=(2 2)
    tt_expect_failure mt_int_vect_dist_l1 v1 v1_2 || return 1
    tt_expect_stdout_eq 0 mt_int_vect_dist_l1 v1_2 v1_2 || return 1
    tt_expect_stdout_eq 1 mt_int_vect_dist_l1 v0 v1 || return 1
    tt_expect_stdout_eq 2 mt_int_vect_dist_l1 v0 v2 || return 1
    tt_expect_stdout_eq 4 mt_int_vect_dist_l1 v0_0 v2_2 || return 1
    return 0
}
