# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
RES_DIR="$SCRIPT_DIR/res/yaml"

tt_context "Testing YAML library"

_YM_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../yaml.sh"

function test_010_sourcing {

    _YM_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../yaml.sh" || return 1
    
    return 0
}

function test_100_path_exists {

    local -i i=0

    tt_expect_success ym_path_exists "$RES_DIR/struct.yml" "" \
        || return $((++i))
    tt_expect_success ym_path_exists "$RES_DIR/struct.yml" "A" \
        || return $((++i))
    tt_expect_success ym_path_exists "$RES_DIR/struct.yml" "B" \
        || return $((++i))
    tt_expect_success ym_path_exists "$RES_DIR/struct.yml" "A.a" \
        || return $((++i))
    tt_expect_success ym_path_exists "$RES_DIR/struct.yml" "A.b" \
        || return $((++i))
    tt_expect_failure ym_path_exists "$RES_DIR/struct.yml" "Z" \
        || return $((++i))
    tt_expect_failure ym_path_exists "$RES_DIR/struct.yml" "A.c" \
        || return $((++i))
    
    return 0
}

function test_110_get_type {

    local -i i=0

    tt_expect_stdout_eq "struct" ym_get_type "$RES_DIR/struct.yml" \
        || return $((++i))
    tt_expect_stdout_eq "struct" ym_get_type "$RES_DIR/struct.yml" A \
        || return $((++i))
    tt_expect_stdout_eq "NoneType" ym_get_type "$RES_DIR/struct.yml" B \
        || return $((++i))

    return 0
}

function test_115_is_empty {

    local -i i=0

    tt_expect_failure ym_is_empty "$RES_DIR/struct.yml" "A" \
        || return $((++i))
    tt_expect_success ym_is_empty "$RES_DIR/struct.yml" "B" \
        || return $((++i))
    tt_expect_failure ym_is_empty "$RES_DIR/struct.yml" "Z" \
        || return $((++i))

    return 0
}

function test_120_is_struct {

    local -i i=0

    tt_expect_success ym_is_struct "$RES_DIR/struct.yml" \
        || return $((++i))
    tt_expect_success ym_is_struct "$RES_DIR/struct.yml" "A" \
        || return $((++i))

    return 0
}

function test_130_is_sequence {

    local -i i=0

    tt_expect_failure ym_is_sequence "$RES_DIR/struct.yml" \
        || return $((++i))

    return 0
}

function test_200_get_keys {

    local -i i=0

    tt_expect_stdout_eq "A B C" ym_get_keys "$RES_DIR/struct.yml" \
        || return $((++i))
    tt_expect_stdout_eq "a b" ym_get_keys "$RES_DIR/struct.yml" A \
        || return $((++i))

    return 0
}

function test_210_has_key {

    local -i i=0

    tt_expect_success ym_has_key "$RES_DIR/struct.yml" "" A || return $((++i))
    tt_expect_success ym_has_key "$RES_DIR/struct.yml" "" B || return $((++i))
    tt_expect_success ym_has_key "$RES_DIR/struct.yml" "" C || return $((++i))
    tt_expect_success ym_has_key "$RES_DIR/struct.yml" A a || return $((++i))
    tt_expect_success ym_has_key "$RES_DIR/struct.yml" A b || return $((++i))

    tt_expect_failure ym_has_key "$RES_DIR/struct.yml" "" D || return $((++i))
    tt_expect_failure ym_has_key "$RES_DIR/struct.yml" A c || return $((++i))
    tt_expect_failure ym_has_key "$RES_DIR/struct.yml" B a || return $((++i))
    tt_expect_failure ym_has_key "$RES_DIR/struct.yml" C a || return $((++i))

    tt_expect_success ym_has_key "$RES_DIR/struct.yml" A || return $((++i))
    tt_expect_success ym_has_key "$RES_DIR/struct.yml" B || return $((++i))
    tt_expect_success ym_has_key "$RES_DIR/struct.yml" C || return $((++i))
    tt_expect_success ym_has_key "$RES_DIR/struct.yml" A.a || return $((++i))
    tt_expect_success ym_has_key "$RES_DIR/struct.yml" A.b || return $((++i))

    tt_expect_failure ym_has_key "$RES_DIR/struct.yml" D || return $((++i))
    tt_expect_failure ym_has_key "$RES_DIR/struct.yml" A.c || return $((++i))
    tt_expect_failure ym_has_key "$RES_DIR/struct.yml" B.a || return $((++i))
    tt_expect_failure ym_has_key "$RES_DIR/struct.yml" C.a || return $((++i))

    return 0
}

function test_300_get_value {

    local -i i=0

    tt_expect_stdout_eq "xxx" ym_get_value "$RES_DIR/struct.yml" A.a \
        || return $((++i))
    tt_expect_stdout_eq "" ym_get_value "$RES_DIR/struct.yml" B \
        || return $((++i))

    return 0
}

function test_310_get_array_values {

    local -i i=0

    tt_expect_stdout_esc_eq "I\nII\nIII\n" \
        ym_get_array_values "$RES_DIR/struct.yml" C || return $((++i))
    tt_expect_stdout_eq "" ym_get_array_values "$RES_DIR/struct.yml" B \
        || return $((++i))
    tt_expect_failure ym_get_array_values "$RES_DIR/struct.yml" A.a \
        || return $((++i))

    return 0
}

function test_310_get_struct_values {

    local -i i=0

    tt_expect_failure ym_get_struct_values "$RES_DIR/struct.yml" C \
        || return $((++i))
    tt_expect_stdout_eq "" ym_get_struct_values "$RES_DIR/struct.yml" B \
        || return $((++i))
    tt_expect_stdout_esc_eq "xxx\nyyy\n" \
        ym_get_struct_values "$RES_DIR/struct.yml" A || return $((++i))

    return 0
}

function test_400_get_size {

    local -i i=0

    tt_expect_stdout_eq "3" ym_get_size "$RES_DIR/struct.yml" \
        || return $((++i))
    tt_expect_stdout_eq "2" ym_get_size "$RES_DIR/struct.yml" A \
        || return $((++i))
    tt_expect_stdout_eq "0" ym_get_size "$RES_DIR/struct.yml" B \
        || return $((++i))
    tt_expect_stdout_eq "3" ym_get_size "$RES_DIR/struct.yml" C \
        || return $((++i))

    return 0
}
