# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing date/time library"

_DT_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../date.sh"

function test_010_sourcing {

    _DT_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../date.sh" || return 1

    return 0
}

function test_100_dt_sec2time {

    tt_expect_failure dt_sec2time || return 1
    tt_expect_output_eq "00:00:00" dt_sec2time 0 || return 2
    tt_expect_output_eq "00:00:01" dt_sec2time 1 || return 3
    tt_expect_output_eq "00:00:10" dt_sec2time 10 || return 4
    tt_expect_output_eq "00:00:59" dt_sec2time 59 || return 5
    tt_expect_output_eq "00:01:00" dt_sec2time 60 || return 6
    tt_expect_output_eq "00:01:01" dt_sec2time 61 || return 7
    tt_expect_output_eq "00:02:01" dt_sec2time 121 || return 8
    tt_expect_output_eq "00:59:00" dt_sec2time $((3600-60)) || return 9
    tt_expect_output_eq "00:59:59" dt_sec2time $((3600-1)) || return 10
    tt_expect_output_eq "01:00:00" dt_sec2time 3600 || return 11
    tt_expect_output_eq "23:59:59" dt_sec2time $((24*3600-1)) || return 12
    tt_expect_output_eq "24:00:00" dt_sec2time $((24*3600)) || return 13
    tt_expect_output_eq "24:00:01" dt_sec2time $((24*3600+1)) || return 14
    tt_expect_output_eq "99:59:59" dt_sec2time $((100*3600-1)) || return 15
    tt_expect_output_eq "100:00:00" dt_sec2time $((100*3600)) || return 16

    return 0
}
