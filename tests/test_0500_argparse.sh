# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing argparse library"

_AP_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../argparse.sh"

function test_010_sourcing {

    _AP_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../argparse.sh" || return 1
    
    return 0
}

function test_opt {

    ap_reset_args || return 1
    tt_expect_failure ap_read_args "-a" 2>/dev/null || return 1
    tt_expect_failure ap_read_args "-g" 2>/dev/null || return 1
    tt_expect_failure ap_read_args "-h" 2>/dev/null || return 1

    ap_add_debug_opt || return 1
    tt_expect_failure ap_read_args "-a" 2>/dev/null || return 1
    tt_expect_failure ap_read_args "-h" 2>/dev/null || return 1

    ap_add_help_opt || return 1

    ap_read_args "" || return 1
    tt_expect_num_eq  "$_LG_DEBUG_LEVEL" 0 || return 1

    ap_read_args "-g" || return 1
    tt_expect_num_eq  "$_LG_DEBUG_LEVEL" 1 || return 1

    _LG_DEBUG_LEVEL=0
    ap_read_args "--debug" || return 1
    tt_expect_num_eq  "$_LG_DEBUG_LEVEL" 1 || return 1

    _LG_DEBUG_LEVEL=0
    ap_read_args "-gg" || return 1
    tt_expect_num_eq  "$_LG_DEBUG_LEVEL" 2 || return 1

    _LG_DEBUG_LEVEL=0
    ap_read_args "-ggg" || return 1
    tt_expect_num_eq  "$_LG_DEBUG_LEVEL" 3 || return 1

    ap_add_help_opt || return 1
    tt_expect_success read_args "-h" || return 1

    return 0
}

function test_opt_enum {

    ap_reset_args || return 1
    ap_add_opt_enum "myopt" ENUM_VAL "val1,val2" "My desc." || return 2
    read_args || return 3
    tt_expect_str_eq "$ENUM_VAL" "val1" || return 4
    read_args --myopt val1 || return 3
    tt_expect_str_eq "$ENUM_VAL" "val1" || return 5
    read_args --myopt val2 || return 6
    tt_expect_str_eq "$ENUM_VAL" "val2" || return 7
    tt_expect_failure read_args --myopt wrong_value || return 8
    tt_expect_failure read_args --myopt "" || return 9

    return 0
}

function test_pos {

    ap_reset_args || return 1
    ap_add_pos_one SERVER "A server address." || return 1
    ap_read_args "my.addr" || return 1
    tt_expect_str_eq "$SERVER" my.addr || return 1

    ap_reset_args || return 1
    ap_add_pos_n FILES 2 "Two valid files." || return 1
    ap_read_args "my.file1" "my.file2" || return 1
    tt_expect_num_eq ${#FILES[@]} 2 || return 1
    tt_expect_str_eq "${FILES[0]}" "my.file1" || return 1
    tt_expect_str_eq "${FILES[1]}" "my.file2" || return 1

    unset FILES
    ap_reset_args || return 1
    ap_add_pos_max FILES "A list of files." || return 1
    ap_read_args f1 f2 f3 f4 || return 1
    tt_expect_num_eq ${#FILES[@]} 4 || return 1

    return 0
}

function test_bflag {

    ap_reset_args || return 1
    ap_add_opt_bflag 'b' 'BVAR' || return 2
    read_args || return 3
    tt_expect_failure "$BVAR" || return 4
    read_args '-b' || return 5
    tt_expect_success "$BVAR" || return 6

    return 0
}

function test_ap_add_opt_sflag_arr {

    # Define one flag
    ap_reset_args || return 1
    ap_add_opt_sflag_arr "a" ARRAY "val.a" "Desc." || return 2
    read_args || return 3
    tt_expect_num_eq 0 ${#ARRAY[@]} || return 4
    read_args "-a" || return 5
    tt_expect_num_eq 1 ${#ARRAY[@]} || return 6
    tt_expect_str_eq "val.a" "${ARRAY[0]}" || return 7

    # Define two flags 
    ap_add_opt_sflag_arr "b" ARRAY "val.b" "Desc." || return 10
    read_args || return 11
    tt_expect_num_eq 0 ${#ARRAY[@]} || return 12
    read_args "-a" || return 13
    tt_expect_num_eq 1 ${#ARRAY[@]} || return 14
    tt_expect_str_eq "val.a" "${ARRAY[0]}" || return 15
    read_args "-ab" || return 16
    tt_expect_num_eq 2 ${#ARRAY[@]} || return 17
    tt_expect_str_eq "val.a" "${ARRAY[0]}" || return 18
    tt_expect_str_eq "val.b" "${ARRAY[1]}" || return 19

    return 0
}

function test_ap_add_opt_shortcut_flag {

    ap_reset_args || return 1
    ap_add_opt_flag a AFLAG "My desc" || return 2
    ap_add_opt_flag b BFLAG "My desc" || return 3
    ap_add_opt_shortcut_flag y "-ab" || return 4
    ap_add_opt_shortcut_flag z "-a -b" || return 5
    read_args -y || return 10
    tt_expect_success bo_is_true "$AFLAG" || return 11
    tt_expect_success bo_is_true "$BFLAG" || return 12
    unset AFLAG BFLAG
    read_args -z || return 20
    tt_expect_success bo_is_true "$AFLAG" || return 21
    tt_expect_success bo_is_true "$BFLAG" || return 22
    
    return 0
}
