# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
OUTPUT_DIR="$SCRIPT_DIR/output"

tt_context "Testing Old Lib's expect module"

source "$SCRIPT_DIR/../src/expect.sh"
source "$SCRIPT_DIR/../ver.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/expect.sh" || return 1

    return 0
}

function test_100_success {

    # Module does not work properly with bash < 4.4
    vr_bash_ge 4.4.0 || return 0

    # expect::success does not work on Darwin
    [[ $(uname) == Darwin ]] && return 0
    
    tt_expect_success expect::success ls || return 1
    tt_expect_failure expect::success ls -option_do_not_exists || return 2
    tt_expect_failure expect::success NonExistentCommand || return 3
    tt_expect_output_eq "" expect::success echo ok || return 4

    return 0
}

function test_110_failure {

    # expect::failure does not work on Darwin
    [[ $(uname) == Darwin ]] && return 0

    tt_expect_failure expect::failure ls || return 1
    tt_expect_success expect::failure ls -option_do_not_exists || return 2
    tt_expect_success expect::failure NonExistentCommand || return 3

    return 0
}

function test_200_file_exists {
    
    local wrk_dir="$OUTPUT_DIR/old_lib_file_exists"
    mkdir -p "$wrk_dir"
    local a_file="$wrk_dir/a_file"
    touch "$a_file"
    
    tt_expect_success expect::file_exists "$a_file" || return 1
    
    return 0
}

function test_210_link_exists {
    
    local wrk_dir="$OUTPUT_DIR/old_lib_link_exists"
    mkdir -p "$wrk_dir"
    local a_file="$wrk_dir/a_file"
    touch "$a_file"
    local a_link="$wrk_dir/a_link"
    ln -s "$a_file" "$a_link"
    
    tt_expect_success expect::link_exists "$a_link" || return 1
    
    return 0
}

function test_220_directory_exists {
    
    local wrk_dir="$OUTPUT_DIR/old_lib_directory_exists"
    mkdir -p "$wrk_dir"
    
    tt_expect_success expect::directory_exists "$wrk_dir" || return 1
    
    return 0
}

function test_300_string_is_empty {

    tt_expect_success expect::string_is_empty '' || return 1
    tt_expect_failure expect::string_is_empty 'foo' || return 2

    return 0
}

function test_310_string_is_not_empty {

    tt_expect_success expect::string_is_not_empty 'foo' || return 1
    tt_expect_failure expect::string_is_not_empty '' || return 2

    return 0
}

function test_320_string_is_equal_to {

    tt_expect_success expect::string_is_equal_to '' '' || return 1
    tt_expect_success expect::string_is_equal_to 'a' 'a' || return 2
    tt_expect_failure expect::string_is_equal_to '' 'a' || return 3
    tt_expect_failure expect::string_is_equal_to 'b' 'a' || return 4
    tt_expect_failure expect::string_is_equal_to 'b' '' || return 5

    return 0
}

function test_320_string_is_not_equal_to {

    tt_expect_failure expect::string_is_not_equal_to '' '' || return 1
    tt_expect_failure expect::string_is_not_equal_to 'a' 'a' || return 2
    tt_expect_success expect::string_is_not_equal_to '' 'a' || return 3
    tt_expect_success expect::string_is_not_equal_to 'b' 'a' || return 4
    tt_expect_success expect::string_is_not_equal_to 'b' '' || return 5

    return 0
}

function test_400_arithmetic_is_true {

    tt_expect_success expect::arithmetic_is_true  0 '=='  0 || return 1
    tt_expect_success expect::arithmetic_is_true 10 '!='  0 || return 2
    tt_expect_success expect::arithmetic_is_true  0 '>='  0 || return 3
    tt_expect_success expect::arithmetic_is_true  0 '<='  0 || return 4
    tt_expect_success expect::arithmetic_is_true 10  '>'  0 || return 5
    tt_expect_success expect::arithmetic_is_true  0  '<' 10 || return 6

    tt_expect_failure expect::arithmetic_is_true  0 '!='  0 || return 7
    tt_expect_failure expect::arithmetic_is_true 10 '=='  0 || return 8
    tt_expect_failure expect::arithmetic_is_true  0 '>'  0 || return 9
    tt_expect_failure expect::arithmetic_is_true  0 '<'  0 || return 10
    tt_expect_failure expect::arithmetic_is_true 10  '<'  0 || return 11
    tt_expect_failure expect::arithmetic_is_true  0  '>' 10 || return 12

    return 0
}
