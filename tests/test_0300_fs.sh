# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
WORK_DIR="$SCRIPT_DIR/wrk"

tt_context "Testing file system library"

_FS_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../fs.sh"

function test_010_sourcing {

    _FS_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../fs.sh" || return 1
    
    return 0
}

function test_100_get_size {

    local -r wrk_dir="$WORK_DIR/get_size"
    local -r file="$wrk_dir/aaa.txt"

    tt_expect_success mkdir -p "$wrk_dir" || return 1
    tt_expect_success rm -f "$file" || return 1
    tt_expect_failure fs_get_file_size "$file" || return 1
    tt_expect_success touch "$file" || return 1

    # Loop on different sizes in bytes
    for ((i = 0 ; i <= 10 ; ++i)) ; do

        # Test
        tt_expect_output_eq "$i" fs_get_file_size "$file" || return 1

        # Add one character to the file
        echo -n 'X' >>"$file"
    done

    return 0
}

function test_150_absolute {

    # Test non existing paths
    tt_expect_stdout_eq "/" fs_absolute "/" || return 1
    tt_expect_stdout_eq "/a" fs_absolute "/a" || return 1
    tt_expect_stdout_eq "/a" fs_absolute "/a/." || return 1
    tt_expect_stdout_eq "/" fs_absolute "/a/.." || return 1
    tt_expect_stdout_eq "/abc" fs_absolute "/abc" || return 1
    tt_expect_stdout_eq "/a/b" fs_absolute "/a/b" || return 1
    tt_expect_stdout_eq "/" fs_absolute "" "/" || return 1
    tt_expect_stdout_eq "/a" fs_absolute "a" "/" || return 1
    tt_expect_stdout_eq "/a" fs_absolute "a/." "/" || return 1
    tt_expect_stdout_eq "/" fs_absolute "a/.." "/" || return 1

    # Test failure
    tt_expect_failure fs_absolute ".." "/" || return 1
    tt_expect_failure fs_absolute "a/../.." "/" || return 1

    # Test existing path
    local wrk_dir="$WORK_DIR/absolute"
    tt_expect_success rm -rf "$wrk_dir" || return 30
    tt_expect_success mkdir -p "$wrk_dir" || return 31
    wrk_dir="$(realpath "$wrk_dir")"
    declare -r file="$wrk_dir/abc"
    touch "$file" || return 32
    tt_expect_stdout_eq "$file" fs_absolute "abc" "$wrk_dir" || return 33

    # Test link to file on same disk
    declare -r link="$wrk_dir/def"
    ln -s "$file" "$link" || return 34
    tt_expect_stdout_eq "$link" fs_absolute "$link" || return 35

    return 0
}

function test_200_safe_rm {

    # Create working directory
    local wrk_dir="$WORK_DIR/safe_rm"
    tt_expect_success rm -rf "$wrk_dir" || return 1
    tt_expect_success mkdir -p "$wrk_dir" || return 1

    # Path does not exist
    declare file="$wrk_dir/foo"
    declare -i n=0
    tt_expect_failure fs_safe_rm_push "$file" || return 1
    n="$(fs_safe_rm_count)" || return 1
    tt_expect_num_eq 0 "$n" || return 1

    # Create file
    touch "$file" || return 1
    fs_safe_rm_push "$file" || return 1
    n="$(fs_safe_rm_count)" || return 1
    tt_expect_num_eq 1 "$n" || return 1
    fs_safe_rm_pop || return 1
    n="$(fs_safe_rm_count)" || return 1
    tt_expect_num_eq 0 "$n" || return 1
    tt_expect_file "$file" || return 1

    # Put back file to remove and remove it while popping it
    fs_safe_rm_push "$file" || return 1
    n="$(fs_safe_rm_count)" || return 1
    tt_expect_num_eq 1 "$n" || return 1
    fs_safe_rm_pop_del || return 1
    n="$(fs_safe_rm_count)" || return 1
    tt_expect_num_eq 0 "$n" || return 1
    tt_expect_no_path "$file" || return 1

    # Check file is removed by EXIT interruption
    trap - EXIT SIGINT SIGQUIT SIGTERM
    touch "$file" || return 1
    (fs_safe_rm_push "$file") || return 1
    n="$(fs_safe_rm_count)" || return 1
    tt_expect_num_eq 0 "$n" || return 1
    tt_expect_no_path "$file" || return 1

    return 0
}

function test_250_acquire_tmp_file {

    local -r wrk_dir="$WORK_DIR/acquire_tmp_file"
    tt_expect_success rm -rf "$wrk_dir" || return 1
    tt_expect_success mkdir -p "$wrk_dir" || return 1

    # Test if a file is created
    declare tmp_file=
    fs_tmp_acquire_file "foo" "$wrk_dir" || return 1
    tmp_file="$(fs_tmp_last_path)" || return 1
    tt_expect_n_files_in_folder 1 "$wrk_dir" || return 1
    tt_expect_file "$tmp_file" || return 1
    fs_tmp_release_last || return 1
    tt_expect_n_files_in_folder 0 "$wrk_dir" || return 1

    # Run in a subshell in order to trigger EXIT signal
    trap - EXIT SIGINT SIGQUIT SIGTERM
    (fs_tmp_acquire_file "foo" "$wrk_dir")
    tt_expect_n_files_in_folder 0 "$wrk_dir" || return 1

    return 0
}

function test_250_acquire_tmp_dir {

    local -r wrk_dir="$WORK_DIR/acquire_tmp_dir"
    tt_expect_success rm -rf "$wrk_dir" || return 1
    tt_expect_success mkdir -p "$wrk_dir" || return 1

    # Test if a folder is created
    declare tmp_dir=
    fs_tmp_acquire_dir "foo" "$wrk_dir" || return 1
    tmp_dir="$(fs_tmp_last_path)" || return 1
    tt_expect_n_paths_in_folder 1 "$wrk_dir" || return 1
    tt_expect_n_folders_in_folder 1 "$wrk_dir" || return 1
    tt_expect_folder "$tmp_dir" || return 1
    fs_tmp_release_last || return 1
    tt_expect_n_paths_in_folder 0 "$wrk_dir" || return 1

    # Run in a subshell in order to trigger EXIT signal
    trap - EXIT SIGINT SIGQUIT SIGTERM
    (fs_tmp_acquire_dir "foo" "$wrk_dir")
    tt_expect_n_paths_in_folder 0 "$wrk_dir" || return 1

    return 0
}
