# vi: ft=bash
# shellcheck disable=SC1091

TEST_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing output library"

_OP_SOURCED= # Force re-sourcing
source "$TEST_DIR/../output.sh"

function test_010_sourcing {

    _OP_SOURCED= # Force re-sourcing
    tt_expect_success source "$TEST_DIR/../output.sh" || return 1

    return 0
}

function test_100_op_256_to_html_rgb {

    tt_expect_stdout_eq "#eeeeee" op_256_to_html_rgb 255 || return 1

    return 0
}

function test_110_op_rgb2hsl {

    tt_expect_stdout_esc_eq "0\n0\n0\n" op_rgb2hsl "000000" || return 1
    tt_expect_stdout_esc_eq "0\n0\n100\n" op_rgb2hsl "ffffff" || return 1
    tt_expect_stdout_esc_eq "0\n100\n50\n" op_rgb2hsl "ff0000" || return 1
    tt_expect_stdout_esc_eq "120\n100\n50\n" op_rgb2hsl "00ff00" || return 1
    tt_expect_stdout_esc_eq "240\n100\n50\n" op_rgb2hsl "0000ff" || return 1
    tt_expect_stdout_esc_eq "180\n100\n50\n" op_rgb2hsl "00ffff" || return 1
    tt_expect_stdout_esc_eq "300\n100\n50\n" op_rgb2hsl "ff00ff" || return 1
    tt_expect_stdout_esc_eq "267\n38\n55\n" op_rgb2hsl "8a63b9" || return 1

    return 0
}

function test_200_op_closest256 {
    vr_bash_ge 4.4 || return 0
    tt_expect_stdout_eq 0 op_closest256 "000000" || return 1
    tt_expect_stdout_eq 0 op_closest256 "000001" || return 1
    tt_expect_stdout_eq 1 op_closest256 "7f0000" || return 1
#    tt_expect_stdout_eq 0 op_closest256 "000100" || return 1
#    tt_expect_stdout_eq 0 op_closest256 "010000" || return 1
#    tt_expect_stdout_eq 9 op_closest256 "ff0000" || return 1
#    tt_expect_stdout_eq 10 op_closest256 "00ff00" || return 1
#    tt_expect_stdout_eq 25 op_closest256 "005faf" || return 1
#    tt_expect_stdout_eq 255 op_closest256 "eeeeee" || return 1
    return 0
}

function test_200_op_closest256_as_hsl {
    vr_bash_ge 4.4 || return 0
    tt_expect_stdout_eq 0 op_closest256_as_hsl "000000" || return 1
    tt_expect_stdout_eq 17 op_closest256_as_hsl "000001" || return 1
#    tt_expect_stdout_eq 22 op_closest256_as_hsl "000100" || return 1
#    tt_expect_stdout_eq 52 op_closest256_as_hsl "010000" || return 1
#    tt_expect_stdout_eq 9 op_closest256_as_hsl "ff0000" || return 1
#    tt_expect_stdout_eq 10 op_closest256_as_hsl "00ff00" || return 1
#    tt_expect_stdout_eq 25 op_closest256_as_hsl "005faf" || return 1
#    tt_expect_stdout_eq 255 op_closest256_as_hsl "eeeeee" || return 1
    return 0
}
