# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing Old Lib's version module"

source "$SCRIPT_DIR/../deprecate.sh"
dp_disable_depreaction_messages
source "$SCRIPT_DIR/../src/version.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/version.sh" || return 1

    return 0
}

function test_100_version_equals {

    tt_expect_success version::equals '1' '1' || return 1
    tt_expect_success version::equals '1.10.0' '1.10.0' || return 1
    tt_expect_success version::equals '0.0.90' '0.0.90' || return 2
    tt_expect_success version::equals '0.90' '0.90' || return 3
    tt_expect_failure version::equals '0.90' '0.91' || return 4
    tt_expect_failure version::equals '0.90' '0.9' || return 5
    tt_expect_failure version::equals '0.0.90' '0.0.91' || return 7
    tt_expect_failure version::equals '0.0.90' '0.0.09' || return 8
    tt_expect_failure version::equals '0.1.90' '0.0.90' || return 9

    return 0
}

function test_200_version_is {

    tt_expect_success version::is '1.0.0' '==' '1.0.0' || return 1
    tt_expect_success version::is '1.0.0' '!=' '0.0.0' || return 2
    tt_expect_success version::is '1.0.0' '>=' '0.9.0' || return 3
    tt_expect_success version::is '1.0.0' '>='  '1.0.0' || return 4
    tt_expect_success version::is '1.0.0' '<='  '1.0.0' || return 5
    tt_expect_success version::is '1.0.0' '<=' '20.0.0' || return 6
    tt_expect_success version::is '1.0.0' '<'  '20.0.0' || return 7
    tt_expect_success version::is '1.0.0' '>'  '0.9.9' || return 8
    tt_expect_failure version::is '1.0.0' '==' '1.0.1' || return 9
    tt_expect_failure version::is '1.0.0' '!=' '1.0.0' || return 10
    tt_expect_failure version::is '1.0.0' '<=' '0.9.9' || return 11
    tt_expect_failure version::is '1.0.0' '<'  '0.9.9' || return 12
    tt_expect_failure version::is '1.0.0' '<'  '1.0.0' || return 13
    tt_expect_failure version::is '1.0.0' '>'  '1.0.0' || return 14
    tt_expect_failure version::is '1.0.0' '>'  '1.0.1' || return 15
    tt_expect_failure version::is '1.0.0' '>='  '1.0.1' || return 16

    return 0
}
