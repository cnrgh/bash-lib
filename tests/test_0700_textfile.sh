# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing text file library"

_TF_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../textfile.sh"

function test_010_sourcing {

    _TF_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../textfile.sh" || return 1
    
    return 0
}
