# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing Old Lib's string module"

source "$SCRIPT_DIR/../src/string.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/string.sh" || return 1

    return 0
}

function test_100_string_contains {

    tt_expect_success string::contains 'fasta' 'is a fasta file' || return 1

    return 0
}

function test_110_string_enables_with {

    tt_expect_success string::enables_with "" "" || return 1
    tt_expect_success string::enables_with "" "a" || return 2
    tt_expect_success string::enables_with "a" "a" || return 3
    tt_expect_failure string::enables_with "b" "a" || return 4

    return 0
}

function test_200_string_remove {

    tt_expect_output_eq '-ok' string::remove 'ok' 'ok-ok' || return 1

    return 0
}

function test_210_string_remove_all {

    tt_expect_success string::remove_all 'ok' 'ok-ok' || return 1
    tt_expect_output_eq '-' string::remove_all 'ok' 'ok-ok' || return 1

    return 0
}

function test_300_string_to_array {

    tt_expect_output_eq '1 2 3 4' string::to_array '1 2 3 4' || return 1
    tt_expect_output_eq "1 2 3 4" string::to_array '1,2,3,4' ',' || return 2

    return 0
}

function test_400_string_is_int {

    tt_expect_success string::is_int "1" || return 1
    tt_expect_success string::is_int "-1" || return 2
    tt_expect_success string::is_int "0" || return 3
    tt_expect_success string::is_int "10" || return 4
 
    tt_expect_failure string::is_int "" || return 100
    tt_expect_failure string::is_int ".0" || return 101
    tt_expect_failure string::is_int "1." || return 102
    tt_expect_failure string::is_int "0." || return 103
    tt_expect_failure string::is_int "1.0" || return 104

    return 0
}

function test_410_string_is_float {

    tt_expect_failure string::is_float "" || return 1
    tt_expect_failure string::is_float "1" || return 2
    tt_expect_failure string::is_float "-1" || return 3
    tt_expect_failure string::is_float "0" || return 4
    tt_expect_failure string::is_float "10" || return 5
    tt_expect_failure string::is_float "+1" || return 6
 
    tt_expect_success string::is_float ".0" || return 101
    tt_expect_success string::is_float "1." || return 102
    tt_expect_success string::is_float "0." || return 103
    tt_expect_success string::is_float "1.0" || return 104
    tt_expect_success string::is_float "-1.0" || return 105

    return 0
}
