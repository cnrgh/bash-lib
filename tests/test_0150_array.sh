# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing array library"

_AR_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../array.sh"

function test_010_sourcing {

    _AR_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../array.sh" || return 1

    return 0
}

function test_100_ar_index {

    tt_expect_output_eq -1 ar_index || return 1
    tt_expect_output_eq -1 ar_index a || return 2
    tt_expect_output_eq 0 ar_index a a || return 3
    tt_expect_output_eq -1 ar_index a b || return 4
    tt_expect_output_eq 0 ar_index a a a a || return 5
    tt_expect_output_eq 0 ar_index a a b b || return 6
    tt_expect_output_eq 1 ar_index a b a b || return 7
    tt_expect_output_eq 2 ar_index a b b a || return 8

    return 0
}

function test_100_ar_contains {

    tt_expect_failure ar_contains || return 1
    tt_expect_failure ar_contains a || return 2
    tt_expect_success ar_contains a a || return 3
    tt_expect_failure ar_contains a b || return 4
    tt_expect_success ar_contains a a a a || return 5
    tt_expect_success ar_contains a a b b || return 6
    tt_expect_success ar_contains a b a b || return 7
    tt_expect_success ar_contains a b b a || return 8

    return 0
}

function test_110_ar_is_empty {

    tt_expect_success ar_is_empty || return 1
    tt_expect_failure ar_is_empty "" "" || return 2
    tt_expect_failure ar_is_empty "" || return 3
    tt_expect_failure ar_is_empty "a" "" || return 4
    tt_expect_failure ar_is_empty "a" "b" || return 5

    return 0
}

function test_200_ar_move_first {

    tt_expect_output_eq "" ar_move_first || return 1
    tt_expect_output_eq "" ar_move_first "a" || return 2
    tt_expect_output_eq "a" ar_move_first "a" "a" || return 3
    tt_expect_output_eq "b" ar_move_first "a" "b" || return 4
    tt_expect_output_eq "a b" ar_move_first "a" "a" "b" || return 5
    tt_expect_output_eq "a b" ar_move_first "a" "b" "a" || return 6

    return 0
}

function test_210_ar_remove {

    tt_expect_output_eq "" ar_remove || return 1
    tt_expect_output_eq "" ar_remove "a" || return 2
    tt_expect_output_eq "a" ar_remove "" "a" || return 3
    tt_expect_output_eq "" ar_remove "a" "" || return 4
    tt_expect_output_eq "" ar_remove "a" "a" || return 5
    tt_expect_output_eq "b" ar_remove "a" "b" || return 6
    tt_expect_output_eq "b" ar_remove "a" "a" "b" || return 7
    tt_expect_output_eq "b" ar_remove "a" "b" "a" || return 8
    tt_expect_output_eq "a c" ar_remove "b" "a" "b" "c" || return 9

    return 0
}

function test_300_ar_sort {

    tt_expect_output_eq "" ar_sort || return 1
    tt_expect_output_eq "a" ar_sort a || return 1
    tt_expect_output_eq "a b" ar_sort a b || return 1
    tt_expect_output_eq "a b" ar_sort b a || return 1

    return 0
}
