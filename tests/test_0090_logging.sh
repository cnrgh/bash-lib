# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing logging library"

_LG_SOURCED= # Force re-sourcing
LG_ERROR_IS_FATAL=false
source "$SCRIPT_DIR/../logging.sh"
lg_disable_color
RES_DIR="$SCRIPT_DIR/res/logging"

function test_010_sourcing {

    _LG_SOURCED= # Force re-sourcing
    # shellcheck disable=SC2034
    LG_ERROR_IS_FATAL=false
    tt_expect_success source "$SCRIPT_DIR/../logging.sh" || return 1

    return 0
}

function test_100_lg_fatal {

    tt_expect_failure lg_fatal "Fatal error" || return 1
    tt_expect_status 1 lg_fatal "Fatal error" || return 2
    tt_expect_status 1 lg_fatal 1 "Fatal error" || return 3
    tt_expect_status 2 lg_fatal 2 "Fatal error" || return 4
    tt_expect_status 255 lg_fatal 255 "Fatal error" || return 5

    return 0
}

function test_110_lg_error {
    
    tt_expect_success lg_error "Error" || return 1
    tt_expect_stderr_esc_eq "[ERROR] Error\n" lg_error "Error" || return 2

    lg_set_level 0
    tt_expect_stderr_esc_eq "" lg_error "Error" || return 3
    lg_set_level "$LG_LEVEL_FATAL"
    tt_expect_stderr_esc_eq "" lg_error "Error" || return 4
    lg_set_level "$LG_LEVEL_ERROR"
    tt_expect_stderr_esc_eq "[ERROR] Error\n" lg_error "Error" || return 5

    return 0
}

function test_120_lg_warning {

    tt_expect_success lg_warning "Warning" || return 1
    tt_expect_stderr_esc_eq "[WARNING] Warning\n" lg_warning "Warning" \
        || return 2

    lg_set_level 0
    tt_expect_stderr_esc_eq "" lg_warning "Warning" || return 3
    lg_set_level "$LG_LEVEL_WARNING"
    tt_expect_stderr_esc_eq "[WARNING] Warning\n" lg_warning "Warning" \
        || return 4

    return 0
}

function test_130_lg_info {

    tt_expect_success lg_info "Info" || return 1
    tt_expect_stderr_esc_eq "[INFO] Info\n" lg_info "Info" \
        || return 2

    lg_set_level 0
    tt_expect_stderr_esc_eq "" lg_info "Info" || return 3
    lg_set_level "$LG_LEVEL_INFO"
    tt_expect_stderr_esc_eq "[INFO] Info\n" lg_info "Info" \
        || return 4

    return 0
}

function test_140_lg_debug {

    lg_set_level "$LG_LEVEL_DEBUG"
    lg_set_debug_level 1
    tt_expect_success lg_debug 1 "Debug" || return 2
    tt_expect_stderr_esc_eq "[DEBUG] Debug\n" lg_debug 1 "Debug" \
        || return 3

    tt_expect_success lg_debug 2 "Debug" || return 10
    tt_expect_stderr_esc_eq "" lg_debug 2 "Debug" || return 11
    lg_set_debug_level 2
    tt_expect_success lg_debug 2 "Debug" || return 13
    tt_expect_stderr_esc_eq "[TRACE] Debug\n" lg_debug 2 "Debug" \
        || return 14

    lg_set_level 0
    tt_expect_stderr_esc_eq "" lg_debug 2 "Debug" || return 20
    lg_set_level "$LG_LEVEL_DEBUG"
    tt_expect_stderr_esc_eq "[DEBUG] Debug\n" lg_debug 1 "Debug" \
        || return 21

    return 0
}

function test_150_lg_trace {

    tt_expect_success lg_trace "Trace" || return 1
    tt_expect_stderr_esc_eq "" lg_trace "Trace" || return 2
    lg_set_level 0
    tt_expect_stderr_esc_eq "" lg_trace "Trace" || return 3
    lg_set_level "$LG_LEVEL_TRACE"
    tt_expect_stderr_esc_eq "[TRACE] Trace\n" lg_trace "Trace" \
        || return 4

    return 0
}

function test_200_lg_info_from_file {

    tt_expect_stderr_eq "[INFO] ABC" lg_info_from_file "$RES_DIR/one_line.txt" \
        || return 1
    tt_expect_stderr_eq "[INFO]  ABC" lg_info_from_file \
        "$RES_DIR/one_line_with_space.txt" || return 2
    tt_expect_stderr_esc_eq "[INFO] \tABC\n" lg_info_from_file \
        "$RES_DIR/one_line_with_tab.txt" || return 3
    tt_expect_stderr_esc_eq "[INFO] ABC\n[INFO] DEF\n" lg_info_from_file \
        "$RES_DIR/two_lines.txt" || return 4

    return 0
}

function test_300_timestamps {

    lg_enable_epoch_timestamp
    tt_expect_stderr_re "\[[0-9]+\ INFO] Hello" lg_info "Hello" || return 1
    lg_enable_i18n_timestamp
    tt_expect_stderr_re \
        "[[0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]{2}:[0-9]{2}:[0-9]{2} INFO] Hello" \
        lg_info "Hello" || return 2
    lg_disable_timestamp

    return 0
}
