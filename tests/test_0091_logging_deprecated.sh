# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing logging library in old style"

_LG_SOURCED= # Force re-sourcing
LG_ERROR_IS_FATAL=true
source "$SCRIPT_DIR/../logging.sh" 2>/dev/null

function test_010_sourcing {

    _LG_SOURCED= # Force re-sourcing
    # shellcheck disable=SC2034
    LG_ERROR_IS_FATAL=true
    tt_expect_success source "$SCRIPT_DIR/../logging.sh" || return 1
    
    return 0
}

function test_100_lg_error {

    tt_expect_failure lg_error "Error" || return 1

    return 0
}

function test_110_lg_fatal {

    tt_expect_failure lg_fatal "Fatal error" || return 1

    return 0
}
