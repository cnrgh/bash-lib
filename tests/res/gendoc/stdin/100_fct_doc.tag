@fct_start@
## my_fct (function) [#](#my_fct)
@short_desc_start@
My short desc.
@short_desc_end@
### Usage
@fct_usage_start@
@fct_usage_name_start@
my_fct
@fct_usage_name_end@
@fct_usage_end@
Takes no arguments.
@fct_inouts_start@
@fct_inout_default_start@
@fct_arg_name_start@
stdin
@fct_arg_name_end@
@fct_arg_type_start@

@fct_arg_type_end@
@fct_arg_values_start@

@fct_arg_values_end@
@fct_arg_desc_start@
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inout_default_start@
@fct_arg_name_start@
stdout
@fct_arg_name_end@
@fct_arg_type_start@

@fct_arg_type_end@
@fct_arg_values_start@

@fct_arg_values_end@
@fct_arg_desc_start@
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inout_default_start@
@fct_arg_name_start@
stderr
@fct_arg_name_end@
@fct_arg_type_start@

@fct_arg_type_end@
@fct_arg_values_start@

@fct_arg_values_end@
@fct_arg_desc_start@
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inout_default_start@
@fct_arg_name_start@
status
@fct_arg_name_end@
@fct_arg_type_start@

@fct_arg_type_end@
@fct_arg_values_start@

@fct_arg_values_end@
@fct_arg_desc_start@
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inout_default_start@
@fct_arg_name_start@
exit
@fct_arg_name_end@
@fct_arg_type_start@

@fct_arg_type_end@
@fct_arg_values_start@

@fct_arg_values_end@
@fct_arg_desc_start@
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inouts_end@
### Description
@fct_long_desc_start@
My
long
description
@fct_long_desc_end@
@fct_end@
