@page_title_start@
My title
@page_title_end@
# My title
@short_desc_start@
My short description
@short_desc_end@
@long_desc_start@
My long
description
* My first point
* My second point
@long_desc_end@
@fct_start@
## my_fct (function) [#](#my_fct)
@short_desc_start@
My short desc.
@short_desc_end@
### Usage
@fct_usage_start@
@fct_usage_name_start@
my_fct
@fct_usage_name_end@
@fct_usage_end@
Takes no arguments.
@fct_inouts_start@
@fct_inout_default_start@
@fct_arg_name_start@
stdin
@fct_arg_name_end@
@fct_arg_type_start@
string
@fct_arg_type_end@
@fct_arg_values_start@

@fct_arg_values_end@
@fct_arg_desc_start@
No declared processing of stdin stream.
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inout_default_start@
@fct_arg_name_start@
stdout
@fct_arg_name_end@
@fct_arg_type_start@
string
@fct_arg_type_end@
@fct_arg_values_start@

@fct_arg_values_end@
@fct_arg_desc_start@
No declared output on stdout stream.
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inout_default_start@
@fct_arg_name_start@
stderr
@fct_arg_name_end@
@fct_arg_type_start@
string
@fct_arg_type_end@
@fct_arg_values_start@

@fct_arg_values_end@
@fct_arg_desc_start@
No explicit declaration of output on stderr stream. However debug, warning or error messages may occur.
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inout_default_start@
@fct_arg_name_start@
status
@fct_arg_name_end@
@fct_arg_type_start@
integer
@fct_arg_type_end@
@fct_arg_values_start@
0-255
@fct_arg_values_end@
@fct_arg_desc_start@
0 if successful, non-zero otherwise.
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inout_default_start@
@fct_arg_name_start@
exit
@fct_arg_name_end@
@fct_arg_type_start@
integer
@fct_arg_type_end@
@fct_arg_values_start@
0-255
@fct_arg_values_end@
@fct_arg_desc_start@
No explicit declaration of no-exit cases.
@fct_arg_desc_end@
@fct_inout_default_end@
@fct_inouts_end@
### Description
@fct_long_desc_start@
My
long
description
@fct_long_desc_end@
@fct_end@
