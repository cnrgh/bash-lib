# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing version library"

_VR_SOURCED= # Force re-sourcing
source "$SCRIPT_DIR/../ver.sh"

function test_010_sourcing {

    _VR_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../ver.sh" || return 1

    return 0
}

function test_100_vr_get_major {

    tt_expect_output_eq "0" vr_get_major "" || return 1
    tt_expect_output_eq "2" vr_get_major "2" || return 2
    tt_expect_output_eq "10" vr_get_major "10" || return 3
    tt_expect_output_eq "10" vr_get_major "10.0" || return 4
    tt_expect_output_eq "10" vr_get_major "10.1" || return 5
    tt_expect_output_eq "2" vr_get_major "2.1" || return 6
    tt_expect_output_eq "2" vr_get_major "2.1.0" || return 7

    return 0
}

function test_110_vr_get_minor {

    tt_expect_output_eq "0" vr_get_minor "" || return 1
    tt_expect_output_eq "0" vr_get_minor "2" || return 2
    tt_expect_output_eq "1" vr_get_minor "2.1" || return 3
    tt_expect_output_eq "10" vr_get_minor "2.10" || return 4
    tt_expect_output_eq "1" vr_get_minor "2.1.0" || return 5
    tt_expect_output_eq "10" vr_get_minor "2.10.0" || return 6
    tt_expect_output_eq "9" vr_get_minor "0.9" || return 7
    tt_expect_output_eq "90" vr_get_minor "0.90" || return 8

    return 0
}

function test_150_vr_cmp {

    tt_expect_output_eq "0" vr_cmp "" "" || return 1
    tt_expect_output_eq "0" vr_cmp "1" "1" || return 2
    tt_expect_output_eq "0" vr_cmp "1.0" "1" || return 3
    tt_expect_output_eq "0" vr_cmp "1.0" "1.0" || return 4
    tt_expect_output_eq "0" vr_cmp "1.0" "1.0.0" || return 5
    tt_expect_output_eq "0" vr_cmp "1.0.0" "1.0.0" || return 6
    tt_expect_output_eq "0" vr_cmp '1.10.0' '1.10.0' || return 7
    tt_expect_output_eq "0" vr_cmp '0.0.90' '0.0.90' || return 8
    tt_expect_output_eq "0" vr_cmp '0.90' '0.90' || return 9
    
    tt_expect_output_eq "+1" vr_cmp '0.90' '0.9' || return 10
    tt_expect_output_eq "+1" vr_cmp '0.0.90' '0.0.09' || return 11
    tt_expect_output_eq "-1" vr_cmp '0.90' '0.91' || return 12
    tt_expect_output_eq "-1" vr_cmp '0.0.90' '0.0.91' || return 13

    return 0
}
