# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
OUTPUT_DIR="$SCRIPT_DIR/output"

tt_context "Testing Old Lib's handler module"

source "$SCRIPT_DIR/../src/handler.sh"

function test_010_sourcing {

    tt_expect_success source "$SCRIPT_DIR/../src/handler.sh" || return 1

    return 0
}

function test_100_error_handler {

    tt_expect_failure eval \
        "handler::error::enable;a_command_that_does_not_exist" || return 1
    # TODO IMPORTANT Old lib legacy test imported unchanged. However it does not
    # test the behaviour of the handler.
    tt_expect_success handler::error::disable || return 2

    return 0
}

function test_200_report_error {

    local -r wrk_dir="$OUTPUT_DIR/old_lib_report_error"
    mkdir -p "$wrk_dir"
    local -r error_file="$wrk_dir/report_error.txt"
    
    (handler::report::error foo.sh 12 1 2>"$error_file")
    tt_expect_failure_last_cmd || return 1
    local -r msg='[[0;31mERROR[0m] Error on [0;36m[4mfoo.sh[0m at line [0;36m12[0m with the exit code [0;36m1[0m'
    tt_expect_output_eq "$msg" cat "$error_file" || return 2

    return 0
}
