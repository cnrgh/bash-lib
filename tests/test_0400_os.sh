# vi: ft=bash
# shellcheck disable=SC1091

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

tt_context "Testing operating system library"

_OS_SOURCED= # Force re-sourcing
source "$TEST_DIR/../os.sh"

function test_010_sourcing {

    _OS_SOURCED= # Force re-sourcing
    tt_expect_success source "$SCRIPT_DIR/../os.sh" || return 1
    
    return 0
}

function test_100_os_command_exists {
    
    tt_expect_success os_command_exists false || return 1
    tt_expect_success os_cmd_exists false || return 2
    tt_expect_failure os_command_exists MyUnknownCommand || return 3
    
    return 0
}

function test_115_os_commands_exist {
    
    tt_expect_success os_commands_exist false true || return 1
    tt_expect_failure os_commands_exist MyUnknownCommand || return 2
    tt_expect_failure os_commands_exist MyUnknownCommand false || return 3
    tt_expect_failure os_commands_exist MyUnknownCommand false true || return 4
    tt_expect_failure os_commands_exist false MyUnknownCommand true || return 5
    tt_expect_failure os_commands_exist false true MyUnknownCommand || return 6
    
    return 0
}

function test_120_os_check_commands {
    
    tt_expect_success os_check_commands false true || return 1
    tt_expect_failure os_check_commands MyUnknownCommand || return 2
    tt_expect_failure os_check_commands MyUnknownCommand false || return 3
    tt_expect_failure os_check_commands MyUnknownCommand false true || return 4
    tt_expect_failure os_check_commands false MyUnknownCommand true || return 5
    tt_expect_failure os_check_commands false true MyUnknownCommand || return 6

    return 0
}

function test_200_os_exec {

    tt_expect_success os_exec true || return 1
    tt_expect_failure os_exec false || return 2

    return 0
}

function gives_success_after_n_times {
    ((--_NB_TIMES))
    return "$_NB_TIMES"
}

function test_200_os_exec_until_success {

    tt_expect_failure os_exec_until_success 0 || return 1
    tt_expect_failure os_exec_until_success 0 0 || return 2
    tt_expect_failure os_exec_until_success 0 1000 || return 3
    tt_expect_failure os_exec_until_success 0 0 true || return 4
    tt_expect_success os_exec_until_success 1 0 true || return 5
    declare -g _NB_TIMES=1
    tt_expect_success os_exec_until_success 1 0 gives_success_after_n_times \
        || return 6
    _NB_TIMES=2
    tt_expect_success os_exec_until_success 2 0 gives_success_after_n_times \
        || return 7
    _NB_TIMES=2
    tt_expect_failure os_exec_until_success 1 0 gives_success_after_n_times \
        || return 8

    return 0
}
