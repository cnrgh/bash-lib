# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is YM (YaMl):
#  * `ym_`  for the public functions.
#  * `YM_`  for the public global variables or constants.
#  * `_ym_` for the private functions.
#  * `_YM_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_YM_SOURCED ]] || return 0
    _YM_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/bool.sh"
source "$(dirname "${BASH_SOURCE[0]}")/fs.sh"
source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"
source "$(dirname "${BASH_SOURCE[0]}")/os.sh"
source "$(dirname "${BASH_SOURCE[0]}")/array.sh"

set -o pipefail
_YM_SHYAML=

function _ym_chk_shyaml {

    bo_is_false _YM_SHYAML && os_check_commands shyaml && _YM_SHYAML=1
    
    return 0
}

function ym_path_exists {

    local -r file="$1"
    local -r path="$2"
    local -i status=0
    fs_check_file "$file"
    _ym_chk_shyaml

    if [[ -z $path ]] ; then
        shyaml get-type <"$file" >/dev/null 2>&1
        status=$?
    else
        shyaml get-type "$path" <"$file" >/dev/null 2>&1
        status=$?
    fi
    
    return $status
}

function ym_get_type {

    local -r file="$1"
    local -r path="$2"
    fs_check_file "$file"
    _ym_chk_shyaml

    # Get type
    local _type=
    local -i status=0
    if [[ -z $path ]] ; then
        _type=$(shyaml get-type <"$file" 2>/dev/null)
        status=$?
    else
        _type=$(shyaml get-type "$path" <"$file" 2>/dev/null)
        status=$?
    fi
    [[ $status -eq 0 ]] || lg_fatal "Unable to find path \"$path\" inside"\
        "YAML file \"$file\"."

    echo "$_type"
    return 0
}

function ym_is_empty {

    local -r file="$1"
    local -r path="$2"

    local _type=
    _type=$(ym_get_type "$file" "$path")
    
    [[ $_type == NoneType ]]
}

function ym_is_sequence {

    local -r file="$1"
    local -r path="$2"

    local _type=
    _type=$(ym_get_type "$file" "$path")
    
    [[ $_type == sequence ]]
}

function ym_is_struct {

    local -r file="$1"
    local -r path="$2"

    local _type=
    _type=$(ym_get_type "$file" "$path")
    
    [[ $_type == struct ]]
}

function ym_get_keys {

    local -r file="$1"
    local -r path="$2"
    local -r sep="${3:- }"
    local -a keys=
    
    # Check type
    ym_is_struct "$file" "$path" || lg_fatal "\"$path\" is not a struct in" \
        "YAML file \"$file\"."
    
    # Get keys
    if [[ -z $path ]] ; then
        readarray -t keys <<<"$(shyaml keys <"$file")"
    else
        readarray -t keys <<<"$(shyaml keys "$path" <"$file")"
    fi
    
    st_join "$sep" "${keys[@]}"
}

function ym_has_key {

    local -r file="$1"
    local path="$2"
    local key="$3"

    # No separate key value
    if [[ -z $key ]] ; then
        if [[ $path =~ \. ]] ; then
            key=${path##*.}
            path=${path%.*}
        else
            key="$path"
            path=
        fi
    fi

    # Get keys
    local -a keys
    readarray -t keys <<<"$(ym_get_keys "$file" "$path" "\n")"

    ar_contains "$key" "${keys[@]}"
}

function ym_get_value {

    local -r file="$1"
    local -r path="$2"

    ym_is_empty "$file" "$path" && return 0
        
    shyaml get-value "$path" <"$file"
}

function ym_get_array_values {

    local -r file="$1"
    local -r path="$2"

    ym_is_empty "$file" "$path" && return 0
        
    shyaml get-values "$path" <"$file"
}

function ym_get_struct_values {

    local -r file="$1"
    local -r path="$2"

    ym_is_empty "$file" "$path" && return 0
        
    shyaml values "$path" <"$file"
}

function ym_get_size {

    local -r file="$1"
    local -r path="$2"
    local -i size=0
    local -i status=0
    
    if ! ym_is_empty "$file" "$path" ; then
        if [[ -z $path ]] ; then
            size="$(shyaml get-length <"$file")"
            status=$?
        else
            size="$(shyaml get-length "$path" <"$file")"
            status=$?
        fi
    fi
    
    echo "$size"
    return $status
}

true
