# vi: ft=bash
# The name/prefix of this module is ST (STring):
#  * `st_`  for the public functions.
#  * `ST_`  for the public global variables or constants.
#  * `_st_` for the private functions.
#  * `_ST_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_ST_SOURCED ]] || return 0
    _ST_SOURCED=1
fi

function st_search_substr {

    local -r needle="$1"
    local -r haystack="$2"

    # Remove everything up to substring
    # shellcheck disable=SC2295
    local -r rest=${haystack#*$needle}

    # Compute index, using lengths of strings
    local -i index=$(( ${#haystack} - ${#rest} - ${#needle} ))

    # If index is < 0 (substring not found), set it to -1
    [[ $index -lt 0 ]] && index=-1

    # Output index
    echo $index

    return 0
}

function st_is_substr {

    local -ri index=$(st_search_substr "$1" "$2")

    test "$index" -ge 0
}

function st_starts_with {

    local -r sub="$1"
    local -r s="$2"

    test "${s:0:${#sub}}" == "$sub"
}

function st_is_int {

    local -r s="$1"

    [[ $s =~  ^[+-]?[0-9]+$ ]]
}

function st_is_float {

    local -r s="$1"

    [[ $s =~ ^[+-]?([0-9]+\.[0-9]+|\.[0-9]+|[0-9]+\.)$ ]]
}

function st_rm_first {

    local -r sub="$1"
    local -r s="$2"
    
    echo "${s/$sub/}"

    return 0
}

function st_rm_all {

    local -r sub="$1"
    local -r s="$2"
    
    echo "${s//$sub/}"

    return 0
}


function st_join {
    # Join multiple strings together using an arbitrary separator.
    # arg1: The separator.
    # Following arguments: strings to join.

    local -r sep="$1"
    shift

    local i=0
    for s in "$@" ; do
        [[ $i -gt 0 ]] && echo -ne "$sep"
        echo -n "$s"
        ((++i))
    done

    return 0
}

function st_get_random {

    local len="$1"
    local chrs="$2"

    # Check
    [[ $len =~ [0-9]* ]] || lg_fatal "First argument must be an integer" \
        "or empty."

    # Set default values
    [[ -z $len ]] && len=8
    [[ -z $chrs ]] && chrs="A-Za-z0-9"

    # Print random string
    # Must set locale to C on macos otherwise we get an error from tr:
    #    tr: Illegal byte sequence
    local -r lc_all="$LC_ALL"
    local -r lc_ctype="$LC_CTYPE"
    LC_ALL=C
    LC_CTYPE=C
    tr -dc "$chrs" </dev/urandom | head -c "$len"
    LC_ALL="$lc_all"
    LC_CTYPE="$lc_ctype"

    return 0
}

## Split a string into an array.
##
## Split a string along a separator, and fill an array with the values obtained.
## @arg sep     The separator string.
## @arg s       The string to split.
## @arg arr_var The name of the array variable to fill.
## @stdout If no array variable is provided, output the values separated by
## spaces.
## @status 0.
function st_split {

    local -r sep="$1"
    local -r s="$2"
    local -r arr_var="$3"

    # Change Input Field Separator    
    local -r old_ifs="$IFS"
    IFS="$sep"

    if [[ -z $arr_var ]] ; then
        local -a arr=()
        read -ra arr <<<"$s"
    else
        read -ra "${arr_var?}" <<<"$s"
    fi
    
    # Revert Input Field Separator    
    IFS="$old_ifs"

    # Print result
    [[ -z $arr_var ]] && echo "${arr[@]}"

    return 0
}

function st_rep {
    
    local -r n="$1"
    local -r s="$2"

    local t
    t="$(printf "%${n}s")" || lg_fatal "Failure with printf."
    echo "${t// /$s}"

    return 0
}

true
