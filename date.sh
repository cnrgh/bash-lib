# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is DT (DateTime):
#  * `dt_`  for the public functions.
#  * `DT_`  for the public global variables or constants.
#  * `_dt_` for the private functions.
#  * `_DT_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_DT_SOURCED ]] || return 0
    _DT_SOURCED=1
fi

function dt_sec2time {

    local -r s="$1"
    [[ -z $s ]] && lg_fatal "Function dt_sec2time() needs an argument."

    printf '%02d:%02d:%02d' $((s / 3600)) $((s % 3600 / 60)) $((s % 60))

    return 0
}
