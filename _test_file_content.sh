# vi: ft=bash
## @title File content assertions
##
## Assertions testing the content of text files.

## Tests if a file is empty.
##
## Checks that a file exists and that its size is zero.
##
## @arg file path A path to a valid regular file.
## @arg msg str A message to print in case of failure.
## @stderr If the test fails a message is printed.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_empty_file {

    local file="$1"
    local msg="$2"

    if [[ ! -f $file || -s $file ]] ; then
        rt_print_call_stack >&2
        echo "\"$file\" does not exist, is not a file or is not empty. $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a file is not empty.
##
## Checks that a file exists and that its size is greater than zero.
##
## @arg file path A path to a valid regular file.
## @arg msg str A message to print in case of failure.
## @stderr If the test fails a message is printed.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_non_empty_file {

    local file="$1"
    local msg="$2"

    if [[ ! -f $file || ! -s $file ]] ; then
        rt_print_call_stack >&2
        echo "\"$file\" does not exist, is not a file or is empty. $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if two files have identical content.
##
## Checks that both file exists and that their content are identical using
## `diff` command.
##
## @arg file1 path A path to a valid regular file.
## @arg file2 path A path to another valid regular file.
## @stderr If the test fails a message is printed.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_same_files {

    local file1="$1"
    local file2="$2"

    tt_expect_file "$file1" || return 2
    tt_expect_file "$file2" || return 3

    if ! diff -q "$file1" "$file2" >/dev/null ; then
        rt_print_call_stack >&2
        echo "Files \"$file1\" and \"$file2\" differ." >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if two files have the same number of lines.
##
## Checks that both file exists and that they have the number of lines.
##
## @arg file1 path A path to a valid regular file.
## @arg file2 path A path to another valid regular file.
## @stderr If the test fails a message is printed.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_same_number_of_rows {

    local file1=$1
    local file2=$2

    if [[ $(tf_get_nb_rows "$file1") -ne $(tf_get_nb_rows "$file2") ]] ; then
        rt_print_call_stack >&2
        echo "\"$file1\" and \"$file2\" do not have the same number of rows." >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a file does not contain twice the same line.
##
## Checks that a file exists and that it does not contain twice the same line.
##
## @arg file path A path to a valid regular file.
## @stderr If the test fails a message is printed.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_no_duplicated_row {

    local file=$1

    nrows=$(wc -l "$file") || lg_fatal "Failed counting lines of \"$file\"."
    n_uniq_rows=$(sort -u "$file" | wc -l) || \
        lg_fatal "Failed counting unique lines of \"$file\"."
    [[ $nrows -eq $n_uniq_rows ]] || return 1

    echo -n .
    return 0
}

## Tests if a file contains a certain number of lines.
##
## Checks that a file exists and that it contains `n` lines exactly.
##
## @arg file path A path to a valid regular file.
## @arg n int The expected number of lines.
## @arg msg str A message to print in case of failure.
## @stderr If the test fails a message is printed.
## @status Returns 0 if the test succeeds, 1 otherwise.
function tt_expect_file_has_n_lines {

    local file="$1"
    local n="$2"
    local msg="$3"

    if [[ -f $file ]] ; then
        local m
        m="$(wc -l <"$file")" || lg_fatal "Failed running wc command."
        if [[ $n -ne $m ]] ; then
            rt_print_call_stack >&2
            echo "\"$file\" has $m lines while expecting $n. $msg" >&2
            return 1
        fi
    else
        rt_print_call_stack >&2
        echo "Cannot count lines of \"$file\", it does not exist or is not" \
            "a file. $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}
