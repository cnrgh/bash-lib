# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is MT (maths):
#  * `mt_`  for the public functions.
#  * `MT_`  for the public global variables or constants.
#  * `_mt_` for the private functions.
#  * `_MT_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_MT_SOURCED ]] || return 0
    _MT_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/str.sh"
source "$(dirname "${BASH_SOURCE[0]}")/ver.sh"

function mt_div {

    local -r dividend="$1"
    local -r divisor="$2"
    local -r scale="$3"

    [[ -n $dividend ]] || lg_fatal "No dividend."
    [[ -n $divisor ]] || lg_fatal "No divisor."

    local -a cmds=()
    [[ -z $scale ]] || cmds+=("scale=$scale")
    cmds+=("$dividend / $divisor")
    local -r cmd=$(st_join ' ; ' "${cmds[@]}")
    lg_debug 1 "bc cmd=$cmd"
    declare -r res=$(bc -l <<<"$cmd" 2>/dev/null)
    [[ -n $res ]] || lg_fatal "Failure while running bc command \"$cmd\"."
    echo -n "$res"

    return 0
}

function mt_percent {

    declare -r dividend="$1"
    declare -r divisor="$2"
    declare -r scale="$3"

    mt_div "(100 * $dividend)" "$divisor" "$scale" || \
        lg_fatal "Failure in mt_div."

    return 0
}

function mt_scale_int {

    local -ri n="$1"
    local -ri src_min="$2" # Lower bound of the source range
    local -ri src_max="$3" # Upper bound of the source range
    local -ri dst_min="$4" # Lower bound of the destination range
    local -ri dst_max="$5" # Upper bound of the destination range

    echo $(( (n - src_min) * (dst_max - dst_min + 1) / (src_max - src_min + 1) \
        + dst_min ))

    return 0
}

function mt_rand {
    
    local -ri min="$1"
    local -ri max="$2"

    mt_scale_int "$SRANDOM" "0" "$((2**32-1))" "$min" "$max"

    return 0
}

## Find the maximum in an integer array
##
## @arg * Integers.
## @stdout The found maximum.
## @status 0.
function mt_max {
    local max="$1"
    local -i i
    for i in "$@" ; do
        [[ "$i" -gt "$max" ]] && max="$i"
    done
    echo -n "$max"
    return 0
}

## Find the minimum in an integer array
##
## @arg * Integers.
## @stdout The found minimum.
## @status 0.
function mt_min {
    local min="$1"
    local -i i
    for i in "$@" ; do
        [[ "$i" -lt "$min" ]] && min="$i"
    done
    echo -n "$min"
    return 0
}

## Convert an hexadecimal number (string) into an integer
##
## @arg hex The hexadecimal number.
## @stdout An integer.
## @status 1 if the input number is not in hexadecimal format, 0 otherwise.
function mt_hex2dec {
    local -r hex="$1"
    [[ ! $hex =~ ^[A-Fa-f0-9]+$ ]] && \
        lg_error "Input \"$1\" is not an hexadecimal number." && return 1
    echo -n "$((16#$hex))"
    return 0
}

## Compare two numbers.
##
## Compare two numbers (integers or floats).
## @arg a The first number.
## @arg b The second number.
## @status 0 if a < b, 1 otherwise.
function mt_lt {
    
    local -r a="$1"
    local -r b="$2"
    
    # Compare
    local -i cmp
    cmp="$(bc <<<"$a < $b")"

    [[ $cmp == 1 ]]
}

# ****************************************************************
# BASH >= 4.4 SECTION
# ****************************************************************
if vr_bash_ge 4.4 ; then

## Compute the L1 distance between two integer vectors.
##
## The two vectors must have the same length.
## They must be provided as comma separated list of values (e.g.:
## "1,2,3").
## @arg u The name of the first array.
## @arg v The name of the second array.
## @stdout The L1 norm of the difference of the two vectors.
## @status 1 if the two vector have different sizes, 0 otherwise.
function mt_int_vect_dist_l1 {

    declare -n u="$1"
    declare -n v="$2"

    [[ ${#u[@]} -ne ${#v[@]} ]] && \
        lg_error "The two vectors must have the same size." && return 1

    # Compute distance
    declare -i dist=0
    for i in $(seq 0 $((${#u[@]}-1))) ; do
        (( dist += (v[i] > u[i] ? v[i]-u[i] : u[i]-v[i]) ))
    done

    echo -n "$dist"
    return 0
}

fi
true
