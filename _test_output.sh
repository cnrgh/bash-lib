# vi: ft=bash
## @title Output assertions
##
## Assertions testing the output (stdout or stderr) of a command.

## Tests if a command outputs nothing on stderr.
##
## Runs the passed command with its arguments and checks stderr.
## If stderr contains nothing returns 0, otherwise returns 1.
##
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command writes something
## on stderr.
## @status Returns 0 if the command writes nothing on stderr and 1 otherwise.
function tt_expect_empty_stderr {
    _tt_expect_output_op 2 'eq' '' "$@"
}

## Tests if a command outputs nothing on stdout.
##
## Runs the passed command with its arguments and checks stdout.
## If stdout contains nothing returns 0, otherwise returns 1.
##
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command writes something
## on stdout.
## @status Returns 0 if the command writes nothing on stdout and 1 otherwise.
function tt_expect_empty_stdout {
    _tt_expect_output_op 1 'eq' '' "$@"
}

## Tests if a command outputs something on stderr.
##
## Runs the passed command with its arguments and checks stderr.
## If stderr contains at least one character returns 0, otherwise returns 1.
##
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command writes nothing 
## on stderr.
## @status Returns 0 if the command writes something on stderr and 1 otherwise.
function tt_expect_non_empty_stderr {
    _tt_expect_output_op 2 'ne' '' "$@"
}

## Tests if a command outputs something on stdout.
##
## Runs the passed command with its arguments and checks stdout.
## If stdout contains at least one character returns 0, otherwise returns 1.
##
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command writes nothing 
## on stdout.
## @status Returns 0 if the command writes something on stdout and 1 otherwise.
function tt_expect_non_empty_stdout {
    _tt_expect_output_op 1 'ne' '' "$@"
}

function _tt_expect_output_op {

    declare -ir fd="$1"
    declare -r op="$2"
    declare -r expected_output="$3"
    declare -r hex_expected_output=$(echo "$expected_output" | \
        hexdump -e '/1 "%02x"')
    shift 3
    declare cmd="$*"
    declare tmpfile
    fs_tmp_acquire_file "$PROGNAME.stdout" || \
        lg_fatal "Failed creating temporary file."
    tmpfile="$(fs_tmp_last_path)"

    # Run command
    declare -i status=
    case $fd in
        1) ( "$@" >"$tmpfile" ) ; status=$? ;;
        2) ( "$@" 2>"$tmpfile" ) ; status=$? ;;
        *) echo -e "Unknown file descriptor \"$fd\"." >&2 ; return 10 ;;
    esac

    declare output
    output=$(cat "$tmpfile") || \
        lg_fatal "Impossible to access file \"$tmpfile\"."
    declare -r hex_output=$(echo "$output" | hexdump -e '/1 "%02x"')
    fs_tmp_release_last

    if [[ $status -ne 0 ]] ; then
        rt_print_call_stack >&2
        echo "Command \"$cmd\" failed with status $status." >&2
        return 1
    elif [[ $op == eq && "$expected_output" != "$output" ]] ; then
        rt_print_call_stack >&2
        echo "Output of \"$cmd\" is wrong. Expected \"$expected_output\""\
            "($hex_expected_output)." \
            "Got \"$output\" ($hex_output)." >&2
        return 2
    elif [[ $op == ne && "$expected_output" == "$output" ]] ; then
        rt_print_call_stack >&2
        echo "Output of \"$cmd\" is wrong." \
            "Expected something different from \"$expected_output\"" \
            "($hex_expected_output)." >&2
        return 3
    elif [[ $op == re ]] && \
        ! grep -E "$expected_output" >/dev/null <<<"$output" ; then
        rt_print_call_stack >&2
        echo "Output of \"$cmd\" is wrong. Expected \"$expected_output\""\
            "($hex_expected_output)." \
            "Got \"$output\" ($hex_output)." >&2
        return 4
    elif [[ $op == notre ]] && \
        grep -E "$expected_output" >/dev/null <<<"$output" ; then
        rt_print_call_stack >&2
        echo "Output of \"$cmd\" is wrong. \"$expected_output\""\
            "($hex_expected_output) was NOT expected." \
            "Got \"$output\" ($hex_output)." >&2
        return 5
    fi

    echo -n .
    return 0
}

## Tests if the stdout output of a command equals a value.
##
## Runs a command, gets its stdout output and compares it with a reference
## value.
## If there is equality, returns 0, otherwise returns 1.
##
## @arg value str The expected value.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command does not return
## the expected value.
## @status Returns 0 if the command does return the expected value and 1
## otherwise.
function tt_expect_stdout_eq {
    _tt_expect_output_op 1 'eq' "$@"
}

## Tests if the stdout output of a command is different from a value.
##
## Runs a command, gets its stdout output and compares it with a reference
## value.
## If the values are different, returns 0, otherwise returns 1.
##
## @arg value str The expected value.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command does return
## the expected value.
## @status Returns 0 if the command does not return the expected value and 1
## otherwise.
function tt_expect_stdout_ne {
    _tt_expect_output_op 1 'ne' "$@"
}

## Tests if stdout output of a command matches a regular expression.
##
## Runs a command, gets its stdout output and tries to match it with
## a regex pattern.
## If the pattern matches, returns 0, otherwise returns 1.
##
## @arg regex str The regular expression to match.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the output does not match
## the regular expression.
## @status Returns 0 if the output matches the regular expression and 1
## otherwise.
function tt_expect_stdout_re {
    _tt_expect_output_op 1 're' "$@"
}

## Tests if stdout output of a command does not match a regular expression.
##
## Runs a command, gets its stdout output and tries to match it with
## a regex pattern.
## If the pattern does not match, returns 0, otherwise returns 1.
##
## @arg regex str The regular expression to match.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the output matches
## the regular expression.
## @status Returns 0 if the output does not match the regular expression and 1
## otherwise.
function tt_expect_stdout_not_re {
    _tt_expect_output_op 1 'notre' "$@"
}

## Tests if the stderr output of a command equals a value.
##
## Runs a command, gets its stderr output and compares it with a reference
## value.
## If there is equality, returns 0, otherwise returns 1.
##
## @arg value str The expected value.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command does not return
## the expected value.
## @status Returns 0 if the command does return the expected value and 1
## otherwise.
function tt_expect_stderr_eq {
    _tt_expect_output_op 2 'eq' "$@"
}

## Tests if the stderr output of a command is different from a value.
##
## Runs a command, gets its stderr output and compares it with a reference
## value.
## If the values are different, returns 0, otherwise returns 1.
##
## @arg value str The expected value.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command does return
## the expected value.
## @status Returns 0 if the command does not return the expected value and 1
## otherwise.
function tt_expect_stderr_ne {
    _tt_expect_output_op 2 'ne' "$@"
}

## Tests if stderr output of a command matches a regular expression.
##
## Runs a command, gets its stderr output and tries to match it with
## a regex pattern.
## If the pattern matches, returns 0, otherwise returns 1.
##
## @arg regex str The regular expression to match.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the output does not match
## the regular expression.
## @status Returns 0 if the output matches the regular expression and 1
## otherwise.
function tt_expect_stderr_re {
    _tt_expect_output_op 2 're' "$@"
}

## Tests if stderr output of a command does not match a regular expression.
##
## Runs a command, gets its stderr output and tries to match it with
## a regex pattern.
## If the pattern does not match, returns 0, otherwise returns 1.
##
## @arg regex str The regular expression to match.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the output matches
## the regular expression.
## @status Returns 0 if the output does not match the regular expression and 1
## otherwise.
function tt_expect_stderr_not_re {
    _tt_expect_output_op 2 'notre' "$@"
}

function _tt_expect_output_esc_op {

    declare -r fd="$1"
    declare -r op="$2"
    declare -r expected_output="$3"
    shift 3
    declare cmd="$*"
    declare tmpfile
    declare tmpfile2
    fs_tmp_acquire_file "$PROGNAME.stderr.1" || \
        lg_fatal "Failed creating temporary file."
    tmpfile="$(fs_tmp_last_path)"
    fs_tmp_acquire_file "$PROGNAME.stderr.2" || \
        lg_fatal "Failed creating temporary file."
    tmpfile2="$(fs_tmp_last_path)"

    # Run command
    declare -i status=
    case $fd in
        1) ( "$@" >"$tmpfile" ) ; status=$? ;;
        2) ( "$@" 2>"$tmpfile" ) ; status=$? ;;
        both) ( "$@" >"$tmpfile" 2>&1 ) ; status=$? ;;
        *) echo -e "Unknown file descriptor \"$fd\"." >&2 ; return 10 ;;
    esac

    echo -ne "$expected_output" >"$tmpfile2"

    # Check command status
    if [[ $status -ne 0 ]] ; then
        rt_print_call_stack >&2
        echo "Command \"$cmd\" failed with status $status." >&2
        fs_tmp_release_last 2
        return 1
    fi
    
    # Test
    if [[ $op == eq ]] && ! diff -q "$tmpfile" "$tmpfile2" ; then
        rt_print_call_stack >&2
        echo "Output of \"$cmd\" is wrong." >&2
        echo -e "Expected \"$expected_output\"." >&2
        hexdump -C "$tmpfile2" >&2
        echo -n "Got \"" >&2
        cat "$tmpfile" >&2
        echo "\"." >&2
        hexdump -C "$tmpfile" >&2
        fs_tmp_release_last 2
        return 2
    elif [[ $op == ne ]] && diff -q "$tmpfile" "$tmpfile2" ; then
        rt_print_call_stack >&2
        echo -n "Output of \"$cmd\" is wrong." \
            "Expected something different from \"$expected_output\"." >&2
        fs_tmp_release_last 2
        return 3
    fi

    fs_tmp_release_last 2
    echo -n .
    return 0
}

## Tests if the stdout output of a command equals a value that uses escape
## characters.
##
## Runs a command, gets both its stdout and stderr outputs and compares it with
#a reference # value in which escape characters are accepted.
## If there is equality, returns 0, otherwise returns 1.
##
## @arg value str The expected value.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command does not return
## the expected value.
## @status Returns 0 if the command returns the expected value and 1
## otherwise.
function tt_expect_stdout_and_stderr_esc_eq {
    _tt_expect_output_esc_op both 'eq' "$@"
}

## Tests if the stdout output of a command equals a value that uses escape
## characters.
##
## Runs a command, gets its stdout output and compares it with a reference
## value in which escape characters are accepted.
## If there is equality, returns 0, otherwise returns 1.
##
## @arg value str The expected value.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command does not return
## the expected value.
## @status Returns 0 if the command returns the expected value and 1
## otherwise.
function tt_expect_stdout_esc_eq {
    _tt_expect_output_esc_op 1 'eq' "$@"
}

## Tests if the stdout output of a command is different from a value that uses
## escape characters.
##
## Runs a command, gets its stdout output and compares it with a reference
## value in which escape characters are accepted.
## If the values are different, returns 0, otherwise returns 1.
##
## @arg value str The expected value.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command returns
## the expected value.
## @status Returns 0 if the command does not return the expected value and 1
## otherwise.
function tt_expect_stdout_esc_ne {
    _tt_expect_output_esc_op 1 'ne' "$@"
}

## Tests if the stderr output of a command equals a value that uses escape
## characters.
##
## Runs a command, gets its stderr output and compares it with a reference
## value in which escape characters are accepted.
## If there is equality, returns 0, otherwise returns 1.
##
## @arg value str The expected value.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command does not return
## the expected value.
## @status Returns 0 if the command returns the expected value and 1
## otherwise.
function tt_expect_stderr_esc_eq {
    _tt_expect_output_esc_op 2 'eq' "$@"
}

## Tests if the stderr output of a command is different from a value that uses
## escape characters.
##
## Runs a command, gets its stderr output and compares it with a reference
## value in which escape characters are accepted.
## If the values are different, returns 0, otherwise returns 1.
##
## @arg value str The expected value.
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command returns
## the expected value.
## @status Returns 0 if the command does not return the expected value and 1
## otherwise.
function tt_expect_stderr_esc_ne {
    _tt_expect_output_esc_op 2 'ne' "$@"
}

## Tests if a command outputs an exact number of lines on stdout.
##
## Counts number of lines on stdout and compares it with the submitted number.
## If numbers are equal returns 0, otherwise returns 1.
##
## @arg n int The expected number of lines on stdout.
## @arg cmd str ... The command and its arguments.
## @status Returns 0 if exactly n lines were output on stdout, otherwise
## returns 1.
function tt_expect_stdout_nlines_eq {
    _tt_expect_output_nlines_op 1 eq "$@"
}

## Tests if a command outputs an exact number of lines on stderr.
##
## Counts number of lines on stderr and compares it with the submitted number.
## If numbers are equal returns 0, otherwise returns 1.
##
## @arg n int The expected number of lines on stderr.
## @arg cmd str ... The command and its arguments.
## @status Returns 0 if exactly n lines were output on stderr, otherwise
## returns 1.
function tt_expect_stderr_nlines_eq {
    _tt_expect_output_nlines_op 2 eq "$@"
}

## Tests if a command does not output an exact number of lines on stdout.
##
## Counts number of lines on stdout and compares it with the submitted number.
## If numbers are different returns 0, otherwise returns 1.
##
## @arg n int The number of lines we do not want to count on stdout.
## @arg cmd str ... The command and its arguments.
## @status Returns 1 if exactly n lines were output on stdout, otherwise
## returns 0.
function tt_expect_stdout_nlines_ne {
    _tt_expect_output_nlines_op 1 ne "$@"
}

## Tests if a command does not output an exact number of lines on stderr.
##
## Counts number of lines on stderr and compares it with the submitted number.
## If numbers are different returns 0, otherwise returns 1.
##
## @arg n int The number of lines we do not want to count on stderr.
## @arg cmd str ... The command and its arguments.
## @status Returns 1 if exactly n lines were output on stderr, otherwise
## returns 0.
function tt_expect_stderr_nlines_ne {
    _tt_expect_output_nlines_op 2 ne "$@"
}

## Tests if a command outputs more than a number of lines on stdout.
##
## Counts number of lines on stdout and compares it with the submitted number.
## If number of lines is greater or equal to the required number returns 0,
## otherwise returns 1.
##
## @arg n int The expected minmum number of lines on stdout.
## @arg cmd str ... The command and its arguments.
## @status Returns 0 if less than n lines were output on stdout, otherwise
## returns 1.
function tt_expect_stdout_nlines_ge {
    _tt_expect_output_nlines_op 1 ge "$@"
}

## Tests if a command outputs more than a number of lines on stderr.
##
## Counts number of lines on stderr and compares it with the submitted number.
## If number of lines is greater or equal to the required number returns 0,
## otherwise returns 1.
##
## @arg n int The expected minmum number of lines on stderr.
## @arg cmd str ... The command and its arguments.
## @status Returns 1 if less than n lines were output on stderr, otherwise
## returns 0.
function tt_expect_stderr_nlines_ge {
    _tt_expect_output_nlines_op 2 ge "$@"
}

## Tests if a command outputs more than a number of lines on stdout.
##
## Counts number of lines on stdout and compares it with the submitted number.
## If number of lines is greater to the required number returns 0,
## otherwise returns 1.
##
## @arg n int The expected minimum number of lines on stdout.
## @arg cmd str ... The command and its arguments.
## @status Returns 0 if more than n lines were output on stdout, otherwise
## returns 1.
function tt_expect_stdout_nlines_gt {
    _tt_expect_output_nlines_op 1 gt "$@"
}

## Tests if a command outputs more than a number of lines on stderr.
##
## Counts number of lines on stderr and compares it with the submitted number.
## If number of lines is greater to the required number returns 0,
## otherwise returns 1.
##
## @arg n int The expected minimum number of lines on stderr.
## @arg cmd str ... The command and its arguments.
## @status Returns 0 if more than n lines were output on stderr, otherwise
## returns 1.
function tt_expect_stderr_nlines_gt {
    _tt_expect_output_nlines_op 2 gt "$@"
}

## Tests if a command outputs less than a number of lines on stdout.
##
## Counts number of lines on stdout and compares it with the submitted number.
## If number of lines is lesser or equal to the required number returns 0,
## otherwise returns 1.
##
## @arg n int The expected maximum number of lines on stdout.
## @arg cmd str ... The command and its arguments.
## @status Returns 1 if more than n lines were output on stdout, otherwise
## returns 0.
function tt_expect_stdout_nlines_le {
    _tt_expect_output_nlines_op 1 le "$@"
}

## Tests if a command outputs less than a number of lines on stderr.
##
## Counts number of lines on stderr and compares it with the submitted number.
## If number of lines is lesser or equal to the required number returns 0,
## otherwise returns 1.
##
## @arg n int The expected maximum number of lines on stderr.
## @arg cmd str ... The command and its arguments.
## @status Returns 1 if more than n lines were output on stderr, otherwise
## returns 0.
function tt_expect_stderr_nlines_le {
    _tt_expect_output_nlines_op 2 le "$@"
}

## Tests if a command outputs less than a number of lines on stdout.
##
## Counts number of lines on stdout and compares it with the submitted number.
## If number of lines is lesser to the required number returns 0,
## otherwise returns 1.
##
## @arg n int The expected maximum number of lines on stdout.
## @arg cmd str ... The command and its arguments.
## @status Returns 0 if less than n lines were output on stdout, otherwise
## returns 1.
function tt_expect_stdout_nlines_lt {
    _tt_expect_output_nlines_op 1 lt "$@"
}

## Tests if a command outputs less than a number of lines on stderr.
##
## Counts number of lines on stderr and compares it with the submitted number.
## If number of lines is lesser to the required number returns 0,
## otherwise returns 1.
##
## @arg n int The expected maximum number of lines on stderr.
## @arg cmd str ... The command and its arguments.
## @status Returns 0 if less than n lines were output on stderr, otherwise
## returns 1.
function tt_expect_stderr_nlines_lt {
    _tt_expect_output_nlines_op 2 lt "$@"
}

# DEPRECATED FUNCTIONS

## Tests if a command has an empty stdout.
##
## Calls `tt_expect_empty_stdout`
##
## @deprecated
function tt_expect_empty_output {
    dp_deprecated tt_expect_empty_stdout
    tt_expect_empty_stdout "$@"
}

## Tests if a command has a non empty stdout.
##
## Calls `tt_expect_non_empty_stdout`
##
## @deprecated
function tt_expect_non_empty_output {
    dp_deprecated tt_expect_non_empty_stdout
    tt_expect_non_empty_stdout "$@"
}

## Tests the output of a command.
##
## Calls `tt_expect_stdout_eq`
##
## @deprecated
function tt_expect_output_eq {
    dp_deprecated tt_expect_stdout_eq
    tt_expect_stdout_eq "$@"
}

## Tests the output of a command.
##
## Calls `tt_expect_stdout_ne`
##
## @deprecated
function tt_expect_output_ne {
    dp_deprecated tt_expect_stdout_ne
    tt_expect_stdout_ne "$@"
}

## Tests the output of a command.
##
## Calls `tt_expect_stdout_esc_eq`
##
## @deprecated
function tt_expect_output_esc_eq {
    dp_deprecated tt_expect_stdout_esc_eq
    tt_expect_stdout_esc_eq "$@"
}

## Tests the output of a command.
##
## Calls `tt_expect_stdout_esc_ne`
##
## @deprecated
function tt_expect_output_esc_ne {
    dp_deprecated tt_expect_stdout_esc_ne
    tt_expect_stdout_esc_ne "$@"
}

## Tests the number of lines output by a command.
##
## Calls `tt_expect_stdout_nlines_eq`
##
## @deprecated
function tt_expect_output_nlines_eq {
    dp_deprecated tt_expect_stdout_nlines_eq
    tt_expect_stdout_nlines_eq "$@"
}

## Tests the number of lines output by a command.
##
## Calls `tt_expect_stdout_nlines_ge`
##
## @deprecated
function tt_expect_output_nlines_ge {
    dp_deprecated tt_expect_stdout_nlines_ge
    tt_expect_stdout_nlines_ge "$@"
}

## Tests the output of a command.
##
## Calls `tt_expect_stdout_re`
##
## @deprecated
function tt_expect_output_re {
    dp_deprecated tt_expect_stdout_re
    tt_expect_stdout_re "$@"
}

## Tests the output of a command.
##
## Calls `tt_expect_stdout_not_re`
##
## @deprecated
function tt_expect_output_not_re {
    dp_deprecated tt_expect_stdout_not_re
    tt_expect_stdout_not_re "$@"
}
