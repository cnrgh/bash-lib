# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is IN (INtrospection):
#  * `in_`  for the public functions.
#  * `IN_`  for the public global variables or constants.
#  * `_in_` for the private functions.
#  * `_IN_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_IN_SOURCED ]] || return 0
    _IN_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"
source "$(dirname "${BASH_SOURCE[0]}")/ver.sh"
vr_bash_ge 4.4.0 || lg_warning "Module introspect.sh does not work properly" \
    "for versions of bash lower than 4.4.0."

function in_get_var_flags {
    # Returns the flags used when the variable was declared.
    # Returns '-' if the declared variable has not attributes.
    # Returns an empty string if the variable was not declared.

    local -r var="$1"
    [[ -n $var ]] || \
        lg_fatal "No variable name provided to ${FUNCNAME[1]} function."
    
    # Try to find as a variable
    local flags
    flags="$(declare -p "$var" 2>/dev/null)"
    local status=$?

    # Try to find as a function
    if [[ $status -ne 0 ]] && vr_bash_ge 4.3.0 ; then
        flags="$(declare -pF "$var" 2>/dev/null)"
    fi

    # Trim to get flags
    flags=${flags#* -}
    flags=${flags%% *}

    echo "$flags"

    return 0
}

function in_is_declared {

    local -r var="$1"
    [[ -n $var ]] || \
        lg_fatal "No variable name provided to ${FUNCNAME[1]} function."
    local ret=0

    # Test if variable or function has been declared
    if ! declare -p "$var" >/dev/null 2>&1 ; then
        if vr_bash_lt 4.3.0 ; then
            ret=1
        else
            declare -pF "$var" >/dev/null 2>&1
            ret=$?
        fi
    fi
        
    return $ret
}

function in_typeof {

    local -r var="$1"
    local -r flags=$(in_get_var_flags "$var")

    # Undeclared variable
    [[ -n $flags ]] || lg_fatal "Variable $var is not declared."

    case "$flags" in
        *a*) echo 'array' ;;
        *A*) echo 'dict' ;;
        *i*) echo 'int' ;;
        *f*) echo 'fct' ;  vr_bash_lt 4.3.0 && lg_warning "-F option is not" \
            "working in declare statement in bash 4.2 or lower." ;;
        *n*) echo 'nameref' ; vr_bash_lt 4.3.0 && lg_warning "nameref type is" \
            "not handled by declare statement in bash 4.2 or lower." ;;
        *)   echo 'str' ;;
    esac 

    return 0
}

function in_is {

    local -r type="$1"
    local -r var="$2"
    local -r flags=$(in_get_var_flags "$var")

    # Undeclared variable
    [[ -n $flags ]] || lg_fatal "Variable $var is not declared."

    # Define flags to look at
    local on=
    local off=
    case $type in
        str|string)  off='i'        ;;
        int|integer)         on='i' ;;
        ro)                  on='r' ;;
        array)               on='a' ;;
        dict|hash)           on='A' ;;
        nameref)             on='n' ;;
        fct)                 on='f' ;;
        *) lg_fatal "Unknown type $type."
    esac

    echo "on=$on off=$off flags=$flags">&2
    # Test on flags
    local ret=1
    if [[ -z $on ]] ; then
        ret=0 # string requires no flag
    else
        local IFS=
        for flag in $on ; do
            [[ $flags == *$flag* ]] && ret=0 && break
        done
    fi

    # Test off flags
    if [[ ret -eq 0 && -n $off ]] ; then
        local IFS=
        for flag in $off ; do
            [[ $flags == *$flag* ]] && ret=2 && break
        done
    fi

    return $ret
}

true
