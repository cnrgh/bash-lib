# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is OS (Operating System):
#  * `os_`  for the public functions.
#  * `OS_`  for the public global variables or constants.
#  * `_os_` for the private functions.
#  * `_OS_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_OS_SOURCED ]] || return 0
    _OS_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"

OS_DRYRUN=
OS_LOCK_FILE="$HOME/.$(basename "$0").lock"
_OS_PRINT_COMMANDS=
_OS_NAME=
_OS_WARN_FOR_MISSING_CMD=
_OS_PLATFORM=

function os_warn_about_missing_commands {
    _OS_WARN_FOR_MISSING_CMD=1
    return 0
}

function os_enable_cmd_printing {
    _OS_PRINT_COMMANDS=1
    return 0
}

function os_get_platform {
    [[ -n $_OS_PLATFORM ]] || _OS_PLATFORM=$(uname) \
        || lg_fatal "Command \"uname\" failed when getting platform name."
    echo "$_OS_PLATFORM"
    return 0
}

function os_is_linux {
    [[ $(os_get_platform) == Linux ]]
}

function os_is_macos {
    # shellcheck disable=SC2317
    [[ $(os_get_platform) == Darwin ]]
}

function os_exec {
    # Runs a command

    local status=0

    if [[ -z $OS_DRYRUN ]] ; then
        if command -v "$1" >/dev/null 2>&1 ; then
            [[ -z $_OS_PRINT_COMMANDS ]] || lg_info "Running: $*"
            lg_debug 1 "os_exec: $(printf "\"%s\" " "$@")"
            "$@"
            status=$?
        else
            lg_fatal "Command \"$1\" is not available."
        fi
    else
        lg_info "Would run: $*"
    fi

    return $status
}

function os_exec_until_success {
    # Runs a command n times up to success

    local -ir n_tries="$1"
    local -i wait_time="$2" # In milliseconds
    shift 2
    local -i try_index=0
    local status=1

    [[ $n_tries -ge 0 ]] || lg_fatal "The number of tries must be >= 0."

    while [[ $status -ne 0 && $try_index -lt $n_tries ]] ; do
        
        # Sleeps
        if [[ $try_index -gt 0 ]] ; then
            lg_warning "Command !$*! failed, trying again in $wait_time ms." \
                "Try number $((try_index+1))."
            # TODO IMPORTANT Find a command that waits in milliseconds.
            sleep $((wait_time/1000))
        fi
        
        # Runs
        os_exec "$@"
        status=$?
        
        # Decreases left number of tries
        ((++try_index))
    done

    return $status
}

function os_eval {
    # Use `eval` to run a command

    local status=0

    if [[ -z $OS_DRYRUN ]] ; then
        if command -v "$1" >/dev/null 2>&1 ; then
            lg_debug 1 "os_eval: $*"
            eval "$*"
            status=$?
        else
            lg_fatal "Command \"$1\" is not available."
        fi
    else
        echo "$*"
    fi

    return $status
}

function _os_get_os_name {

    [[ -z $_OS_NAME ]] && _OS_NAME=$(uname)
    
    echo -n "$_OS_NAME"

    return 0
}

function os_is_macos {
    
    [[ $(_os_get_os_name) == Darwin ]] && return 0
    
    return 1
}

function os_try_exec {
    if command -v "$1" >/dev/null 2>&1 ; then
        os_exec "$@"
    else
        lg_warning "Command \"$1\" not found."
        return 1
    fi
}

function os_exec_lock {
    # Execute a command only if lock file is available
    local -a cmd_line=("$@")

    PID=$$
    lg_debug 1 "PID=$PID"

    # Open lock file with file descriptor 9
    exec 9<>"$OS_LOCK_FILE"

    # Try to lock file descriptor 9
    if flock -n 9 ; then

        # Write PID of this script into lock file
        echo $PID >&9

        # Execute command
        lg_debug 1 "os_exec_lock: ${cmd_line[*]}"
        "${cmd_line[@]}"

        # Remove lock file
        [[ -f $OS_LOCK_FILE ]] && rm "$OS_LOCK_FILE"
    else
        running_pid=$(cat "$OS_LOCK_FILE")
        lg_fatal "Cannot run \"${cmd_line[*]}\", lock file $OS_LOCK_FILE"\
            "is already taken by process $running_pid."
    fi

    return 0
}

function os_force_remove_lock {

    local kill_cmd="$1"

    if [[ -f $OS_LOCK_FILE ]] ; then
        lg_debug 1 "Removing lock file ${OS_LOCK_FILE}."
        local running_pid
        running_pid=$(cat "$OS_LOCK_FILE") || \
            lg_fatal "Cannot read file \"$OS_LOCK_FILE\"."
        if [[ -n $running_pid ]] ; then

            # Kill running process
            if [[ -n $kill_cmd ]] ; then
                lg_debug 1 "Calling \"$kill_cmd\" to kill processes."
                "$kill_cmd"
            else
                pkill -P "$running_pid"
            fi

            [[ -f $OS_LOCK_FILE ]] && rm "$OS_LOCK_FILE"
        fi
    fi

    return 0
}

function os_command_exists {
    local -r cmd="$1"
    command -v "$cmd" >/dev/null 2>&1
    local -i status=$?
    [[ -n $_OS_WARN_FOR_MISSING_CMD && $status -gt 0 ]] && \
        lg_warning "Command \"$cmd\" does not exist."
    return $status
}

function os_cmd_exists {
    os_command_exists "$@"
}

function os_commands_exist {

    # Check that all submitted commands are available.
    for cmd in "$@" ; do
        lg_debug 1 "Checking existence of command \"$cmd\"."
        os_command_exists "$cmd" || return 1
    done

    return 0
}

function os_check_commands {

    # Check that all submitted commands are available.
    for cmd in "$@" ; do
        lg_debug 1 "Checking command \"$cmd\"."
        command -v "$cmd" >/dev/null 2>&1 || \
            lg_fatal "Cannot find command \"$cmd\"."
    done

    return 0
}

function os_get_first_cmd {
    # Select the first available command in a list

    local status=1

    for cmd in "$@" ; do
        if command -v "$cmd" >/dev/null 2>&1 ; then
            echo "$cmd"
            status=0
            break
        fi
    done

    return $status
}

function os_check_one_command {

    os_get_first_cmd "$@" >/dev/null || \
        lg_fatal "No command available among: $*."

    return 0
}

true
