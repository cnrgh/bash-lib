# CNRGH Bash module to handle shell command

# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_COMMAND} ]] || ! ${CNRGH_LIB_COMMAND}; then
declare -r CNRGH_LIB_COMMAND=true
# shellcheck source=src/output.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/output.sh'
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'

#######################################
# command::die
# Print to stderr a message and exit with code 1
#
# Globals:
#   None
# Arguments:
#   1/ message to print to standard error output before to exit
# Returns:
#   Exit 1
# Examples:
#   command::die "I want to exit now"
#######################################
command::die(){
  output::error "$*"
  exit 1
}


#######################################
# command::run
# Call a command with error handling
# Each command is stored into ${TMP_HISTORY_LOG} before to be launched in order to keep an history.
# Globals:
#   None
# Arguments:
#   1/ An array representing the  command to launch
# Returns:
#   Exit 1 if command fail otherwise return 0
# Examples:
#  command::run ls -l
#######################################
command::run(){
  local -i status_code="${EXIT_FAILURE}"
  if (( $# >= 1 )); then
    local -ra cmd=( "$@" )
    echo "${cmd[*]}" >>  "${TMP_HISTORY_LOG}"
    command "${cmd[@]}" || command::die "Command failed with error code $?\n\t└──> $*";
    status_code="${EXIT_SUCCESS}"
  else
    output::error "${FUNCNAME[0]} expect at least 1 parameters!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <a command> [parameters…]"
  fi
  return "${status_code}"
}


#######################################
# command::exists
# Check the existence of a command using the $PATH, true if the comannd exists otherwise false.
# Globals:
#   None
# Arguments:
#   1/ the command name
# Returns:
#   Exit always 0 to not raise an exception with the use of handler::error
# Examples:
#  is_present=$(command::exists ls)
#######################################
command::exists(){
  local -ri status_code=0
  local -r cmd_name="$1"
  local is_present=false
  if (( $# == 1 )); then
    echo "command::exists ${cmd_name}" >>  "${TMP_HISTORY_LOG}"
    command -v "${cmd_name}" &>/dev/null  && is_present=true || is_present=false
  else
    output::error "${FUNCNAME[0]} expect 1 parameters!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <a command>"
  fi
  echo "${is_present}"
  return "${status_code}"
}


fi
