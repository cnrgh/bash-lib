# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_DICTIONARY} ]] || ! ${CNRGH_LIB_DICTIONARY}; then
declare -r CNRGH_LIB_DICTIONARY=true
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'
# shellcheck source=src/version.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/version.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../logging.sh'

#######################################
# dictionary::has_key
# Inform of the given key exists into the dictionary
# Globals:
#   None
# Arguments:
#   1/ key
#   2/ the dictionary name
# Returns:
#   EXIT_SUCCESS when the key is present otherwise EXIT_FAILURE
# Examples:
#   if dictionary::has_key my_key my_hash; then
#     do_something
#   fi
#######################################
dictionary::has_key(){
  local -i status_code="${EXIT_FAILURE}"
  if (( $# == 2 )); then
    local -r key="${1}"
    if [[ -z "${key}" ]]; then
      lg_error_no_fail "${FUNCNAME[0]} expect a non empty key"
    elif [[ -z "${2}" ]]; then
      lg_error_no_fail "${FUNCNAME[0]} expect a non empty dictionary name"
    else
      if version::is "${BASH_VERSION}" '>=' '4.3'  ; then
        local -n dict="${2}"
        # with bash version < 4.3 shell check warn here
        # shellcheck disable=SC1009,SC1019,SC1020,SC1072,SC1073
        if [[ -v dict["${key}"] ]]; then
          status_code="${EXIT_SUCCESS}"
        fi
      else
        local -r dict="${2}"
        local expression="[[ -n \${${dict}[\${key}] + 1} ]] && echo true || echo false" 
        local is_present
        is_present=$(eval "$expression")
        readonly is_present
        if ${is_present}; then
          status_code="${EXIT_SUCCESS}"
        fi
      fi
    fi
  else
    lg_error_no_fail "${FUNCNAME[0]} expect at least two parameters!"
    lg_error_no_fail "\t└──> parameters used: $*"
    lg_error_no_fail "\t└──> Usage: ${FUNCNAME[0]} <a key> <a dict name>"
  fi
  return ${status_code}
}


#######################################
# dictionary::get
# Get value to the corresponding key into a given dictionary
# Globals:
#   None
# Arguments:
#   1/ key
#   2/ the dictionary name
# Returns:
#   EXIT_SUCCESS when the key is present otherwise EXIT_FAILURE
# Examples:
#   my_value=$(dictionary::get my_key my_hash) 
#######################################
dictionary::get(){
  local -i status_code="${EXIT_FAILURE}"
  if (( $# == 2 )); then
    local -r key="${1}"
    #shellcheck disable=SC2178
    local    result=''
    if [[ -z "${key}" ]]; then
      lg_error_no_fail "${FUNCNAME[0]} expect a non empty key"
    elif [[ -z "${2}" ]]; then
      lg_error_no_fail "${FUNCNAME[0]} expect a non empty dictionary name"
    else
      if version::is "${BASH_VERSION}" '>=' '4.3'  ; then
        local -n dict="${2}"
        if [[ -v dict["${key}"] ]]; then
          status_code="${EXIT_SUCCESS}"
          #shellcheck disable=SC2178
          result="${dict["${key}"]}"
        fi
      else
        local -r dict="${2}"
        local test_key_expression="[[ -n \${${dict}[\${key}] + 1} ]] && echo true || echo false" 
        local get_value_expression="echo \"\${${dict}[\${key}]}\"" 
        local is_present
        is_present=$(eval "${test_key_expression}")
        readonly is_present
        if ${is_present}; then
          status_code="${EXIT_SUCCESS}"
          #shellcheck disable=SC2178
          result="$(eval "${get_value_expression}")"
        fi
      fi
    fi
  else
    lg_error_no_fail "${FUNCNAME[0]} expect at least two parameters!"
    lg_error_no_fail "\t└──> parameters used: $*"
    lg_error_no_fail "\t└──> Usage: ${FUNCNAME[0]} <a key> <a dict name>"
  fi
  #shellcheck disable=SC2178,SC2128
  echo "${result}"
  return ${status_code}
}


#######################################
# dictionary::put
# Put a value to the corresponding key into a given dictionary
# Globals:
#   None
# Arguments:
#   1/ key
#   2/ value 
#   3/ the dictionary name
# Returns:
#   EXIT_SUCCESS when the key is present otherwise EXIT_FAILURE
# Examples:
#  declare -A my_dict=([a]=1 [b]=2)
#  dictionary::put c 3 my_dict
#######################################
dictionary::put(){
  local -i status_code="${EXIT_FAILURE}"
  if (( $# == 3 )); then
    local -r key="${1}"
    local -r value="${2}"
    if [[ -z "${key}" ]]; then
      lg_error_no_fail "${FUNCNAME[0]} expect a non empty key"
    elif [[ -z "${3}" ]]; then
      lg_error_no_fail "${FUNCNAME[0]} expect a non empty dictionary name"
    else
      if version::is "${BASH_VERSION}" '>=' '4.3'  ; then
        local -n dict="${3}"
        status_code="${EXIT_SUCCESS}"
        dict["${key}"]="${value}"
      else
        #shellcheck disable=SC2178
        local -r dict="${3}"
        #shellcheck disable=SC2178,SC2128
        local put_value_expression="${dict}[\"\${key}\"]=\"\${value}\"" 
        status_code="${EXIT_SUCCESS}"
        eval "${put_value_expression}"
      fi
    fi
  else
    lg_error_no_fail "${FUNCNAME[0]} expect at least three parameters!"
    lg_error_no_fail "\t└──> parameters used: $*"
    lg_error_no_fail "\t└──> Usage: ${FUNCNAME[0]} <a key> <a value> <a dict name>"
  fi
  return ${status_code}
}
fi
