# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_FILE} ]] || ! ${CNRGH_LIB_FILE}; then

declare -r CNRGH_LIB_FILE=true
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../fs.sh'

#######################################
# file::exists
# Check if the file exists
#
# Globals:
#   None
# Arguments:
#   1/ file path
# Returns:
#   EXIT_FAILURE if file do not exists
# Examples:
#   if file::exists 'my_file.fasta'; then
#     do_something
#   fi
#######################################
file::exists(){
  local -r file_path="$1"
  local -i status_code=${EXIT_FAILURE}
  if [[ -n "${file_path}" && -f "${file_path}"  ]]; then
    status_code=${EXIT_SUCCESS}
  fi
  return "${status_code}"
}

#######################################
# file::get_extension
# Guess the file extension from a file name
#
# Globals:
#   None
# Arguments:
#   1/ file name
# Returns:
#   EXIT_SUCCESS if value passing from echo is successful otherwise a value greater than 0
# Examples:
#   ext=$(file::get_extension 'my_file.fasta')
#######################################
file::get_extension(){
  local -r file_name="$1"
  local ext="${file_name##*.}"
  [[ $ext == $file_name ]] && ext=
  echo "$ext"
  return "$?"
}


#######################################
# file::has_extension
# Check if the extension is owned by the file
#
# Globals:
#   None
# Arguments:
#   1/ extension to verify is presence
#   2/ file name
# Returns:
#   EXIT_FAILURE if command fail otherwise return EXIT_SUCCESS
# Examples:
#   if file::has_extension '.fasta' 'my_file.fasta'; then
#     do_something
#   fi
#######################################
file::has_extension(){
  local -r expected_extension="$1"
  local -r file_name="$2"
  local    extension=''
  local -i status_code=${EXIT_FAILURE}
  extension=$(file::get_extension "$file_name")
  if (( $? == EXIT_SUCCESS )); then
    if [[ -n "${expected_extension}" && "${extension}" == "${expected_extension}" ]]; then
      status_code=${EXIT_SUCCESS}
    fi
  fi
  return "${status_code}"
}


#######################################
# file::remove_extension
# Remove the extension if is part of provided file name otherwise do nothing
#
# Globals:
#   None
# Arguments:
#   1/ file name
# Returns:
#   EXIT_SUCCESS if value passing from echo is successful otherwise a value greater than 0
#   
# Examples:
#   basename=$(file::remove_extension 'my_file.fasta')
#######################################
file::remove_extension(){
  local -r file_name="$1"
  echo "${file_name%*.*}"
  return "$?"
}


#######################################
# file::get_tmp
# Provides the absolute path of a temporary file
#
# Globals:
#   None
# Arguments:
#   1/ file name (default: a random temporary file into "${TMP_WORKING_DIR}")
# Returns:
#   EXIT_SUCCESS:
#     - if provided file name is not empty.
#     - if a directory do not exists inplace of the temporary file.
#     - if a symbolic link do not exists inplace of the temporary file.
#   EXIT_FAILURE in any others cases.
# Examples:
#  tmp_file1="$(file::get_tmp)"
#  tmp_file2="$(file::get_tmp foo.txt)"
#######################################
file::get_tmp(){
    fs_tmp_acquire_file "bash_lib_old_src_get_tmp"
    echo "$(fs_tmp_last_path)"
    return 0
#  local -r file_name="${1:-"$(basename "$(mktemp -u -p "${TMP_WORKING_DIR}")")"}"
#  local -i status_code="${EXIT_FAILURE}" 
#  local -r file_path="${TMP_WORKING_DIR}/${file_name}"
#  if [[ -n "${file_name}" && ! -d "${file_path}" && ! -L "${file_path}" ]]; then
#    status_code="${EXIT_SUCCESS}"
#  fi
#  echo "${file_path}"
#  return "${status_code}"
}


#######################################
# file::get_log
# Provides the absolute file path for a given log file
#
# Globals:
#   None
# Arguments:
#   1/ file name
# Returns:
#   EXIT_SUCCESS:
#     - if provided file name is not empty.
#     - if a directory do not exists inplace of the temporary file.
#     - if a symbolic link do not exists inplace of the temporary file.
#   EXIT_FAILURE in any others cases.
# Examples:
#  log_file="$(file::get_log cmd.out)"
#######################################
file::get_log(){
    fs_tmp_acquire_file "bash_lib_old_src_get_log"
    fs_tmp_acquire_file
    echo "$(fs_tmp_last_path)"
    return 0
#  local -r file_name="$1"
#  local -i status_code="${EXIT_FAILURE}" 
#  local -r file_path="${TMP_LOG_DIR}/${file_name}"
#  if [[ -n "${file_name}" && ! -d "${file_path}" && ! -L "${file_path}" ]]; then
#    status_code="${EXIT_SUCCESS}"
#  fi
#  echo "${file_path}"
#  return "${status_code}"
}

fi
