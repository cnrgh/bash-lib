# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_STRING} ]] || ! ${CNRGH_LIB_STRING}; then

declare -r CNRGH_LIB_STRING=true
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../str.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../deprecate.sh'
dp_deprecation_warning "src/string.sh is deprecated," \
    "please use str.sh instead."

#######################################
# string::contains
# Inform if a sub-string is part of another
#
# Globals:
#   None
# Arguments:
#   1/ the needle, the sub-string
#   2/ the haystack, string to look into
# Returns:
#   EXIT_SUCCESS when the needle is present otherwise EXIT_FAILURE
# Examples:
#  if string::contains hello 'hello world'; then
#    do_something
# fi
#######################################
string::contains(){
    dp_deprecated "st_is_substr" "str.sh"
    st_is_substr "$1" "$2"
}


#######################################
# string::enables_with
# Inform if a sub-string is part of another
#
# Globals:
#   None
# Arguments:
#   1/ needle, the sub-string
#   2/ haystack, the string to look into
# Returns:
#   EXIT_SUCCESS when the enabling enables with the given needle otherwise EXIT_FAILURE
# Examples:
#  if string::enables_with hello 'hello world'; then
#    do_something
# fi
#######################################
string::enables_with(){
    dp_deprecated "st_starts_with" "str.sh"
    st_starts_with "$1" "$2"
}


#######################################
# string::is_int
# Inform if a string is an integer
#
# Globals:
#   None
# Arguments:
#   1/ the string to look into
# Returns:
#   EXIT_SUCCESS when the string is an integer otherwise EXIT_FAILURE
# Examples:
#  if string::is_int '1'; then
#    do_something
# fi
#######################################
string::is_int(){
    dp_deprecated "st_is_int" "str.sh"
    st_is_int "$1"
}


#######################################
# string::is_float
# Inform if a string is a float
#
# Globals:
#   None
# Arguments:
#   1/ the string to look into
# Returns:
#   EXIT_SUCCESS when the string is a float otherwise EXIT_FAILURE
# Examples:
#  if string::is_float '1'; then
#    do_something
# fi
#######################################
string::is_float(){
    dp_deprecated "st_is_float" "str.sh"
    st_is_float "$1"
}


#######################################
# string::remove
# Remove a sub-string
#
# Globals:
#   None
# Arguments:
#   1/ needle, the sub-string
#   2/ haystack, the string
# Returns:
#   EXIT_SUCCESS if value passing from echo is successful otherwise a value greater than 0
# Examples:
#  str=$(string::remove 'world' 'hello world')
#  str now is equal to 'hello '
#######################################
string::remove(){
    dp_deprecated "st_rm_first" "str.sh"
    st_rm_first "$1" "$2"
}


#######################################
# string::remove_all
#
# Globals:
#   None
# Arguments:
#   1/ needle, the sub-string
#   2/ haystack, the string
# Returns:
#   EXIT_SUCCESS if value passing from echo is successful otherwise a value greater than 0
# Examples:
#  str=$(string::remove_all 'o' 'hello world')
#  str now is equal to 'hell wrld'
#######################################
string::remove_all(){
    dp_deprecated "st_rm_all" "str.sh"
    st_rm_all "$1" "$2"
}


#######################################
# string::to_array
# Split a string to array following a given delimiter
#
# Globals:
#   None
# Arguments:
#   1/ delimiter (default: IFS)
#   2/ the string to look into
# Returns:
#   EXIT_SUCCESS if delimiter is found and the split is succesful otherwise a value greater than 0
# Examples:
#  my_array1=( $(string::to_array '1 0 2') )
#  my_array2=( $(string::to_array 'foo,bar,baz' ',') )
# Notes:
# with bash 4.4 we could to use nameref in order to provides a variable name corresponding to a declared array
#   declare -a my_array=()
#   if string::to_array my_array 'foo,bar,baz' ','; then
#     do_something
#   fi
#######################################
string::to_array(){
    dp_deprecate "st_split" "str.sh"
    st_split "$2" "$1"
}

fi
