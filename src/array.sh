# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_ARRAY} ]] || ! ${CNRGH_LIB_ARRAY}; then

declare -r CNRGH_LIB_ARRAY=true
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'
# shellcheck source=src/debug.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/debug.sh'
#the module array.sh groups functions related to array manipulation
source "$(dirname "${BASH_SOURCE[0]}")"'/../str.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../deprecate.sh'
dp_deprecation_warning "src/array.sh is deprecated," \
    "please use array.sh and str.sh instead."

###########################
#array::join_by
#Join all elements from an array into a string, using a given separator.
#
#Globals:
#  None
#Arguments:
#  1/ the separator, a character that will join the array elements together.
#  2/ the array whose elements need to be joined.
#
#Parameters:
#  -a|--after <char> add the specified char after each element contained in the given array
#  -b|--before <char> add the specfied char before each element contained in the given array
#
#Returns:
#  EXIT_FAILURE if the array or the separator are empty, otherwise EXIT_SUCCESS
#
#Examples:
# array::join_by "," 1 2 3 4
# joined_str=1,2,3,4
#
#array::join_by --before "<" --after ">" "," 1 2 3 4
# joined_str=<1>,<2>,<3>,<4>
#
#array::join_by '' 1 2 3 4
# joined_str=1234
#
#array::join_by --before 'c' '_' 1 2 3 4
# joined_str=c1_c2_c3_c4
#
###########################
array::join_by(){
#  local is_parsing
#  
#  local -i char_to_remove=0
#  local -i len=0
#  
#  local joined_str=""
#  local value
#  local -i status_code=${EXIT_FAILURE}
  
  dp_deprecated "st_join" "bash-lib"

  # Before argument
  if [[ $1 =~ ^-b|--before$ ]] ; then
    local -r before_value="$2"
    shift 2
  fi

  # After argument
  if [[ $1 =~ ^-a|--after$ ]] ; then
    local -r after_value="$2"
    shift 2
  fi
  
  # Failure if wrong argument
  if [[ $1 =~ ^-.*$ ]] ; then
      lg_fatal "Wrong argument $1 for array::join_by function."
  fi

  # Get separator
  local -r sep="$1"
  shift
  
  # Join
  local -a array=("$@")
  [[ -z $before_value ]] || array=("${array[@]/#/$before_value}")
  [[ -z $after_value ]] || array=("${array[@]/%/$after_value}")
  st_join "$sep" "${array[@]}"

  return 0
}
fi
