# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_DEBUG} ]] || ! ${CNRGH_LIB_DEBUG}; then
declare -r CNRGH_LIB_DEBUG=true
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'
# shellcheck source=src/string.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/string.sh'

declare -x is_debug_asked=false
declare -x is_debug_enabled=false
declare -x is_cnrgh_bash_lib_debug_asked=false

#######################################
# debug::read_env_var
# Read configuration and Environment variable to check if the debug mode is needed
#
# Globals:
#   None
# Arguments:
#  None
# Returns:
#
# Examples:
#   debug::read_env_var
#######################################
debug::read_env_var(){
  if [[ -n "${DEBUG:-}" ]] && "${DEBUG}"; then
    is_debug_enabled=false
    is_debug_asked=true
  fi

  if string::contains 'x' "$-" \
      || ( [[ -n "${FULL_DEBUG:-}" ]] && "${FULL_DEBUG}" ); then
    is_debug_enabled=true
    is_debug_asked=true
    debug::enable
  fi

  if [[ -n "${CNRGH_BASH_LIB_DEBUG:-}" ]] && "${CNRGH_BASH_LIB_DEBUG}"; then
    is_cnrgh_bash_lib_debug_asked=true
  fi
  return "${EXIT_SUCCESS}"
}


#######################################
# debug::format
# Internal function in order to format output when debug mode is enabled
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   EXIT_SUCCESS
# Examples:
#
#######################################
debug::format(){
  local    source_file
  local -i line_number="${2}"
  local -i str_size=-1
  local    func_name="${FUNCNAME[1]}"
  source_file="$(realpath "${1}")"
  # relative path
  source_file="${source_file#$(realpath "${PWD}")/}"
  str_size=${#source_file}
  if (( ${#source_file} > 15 )); then
      source_file="${source_file:0:3}…/${source_file:${str_size}-10}"
  fi
  if (( ${#func_name} > 14 )); then
      func_name="${func_name:0:4}…${func_name:${#func_name}-9}"
  fi
#  printf '\e[0;34m>\e[0;33m%-15s:%-4d:%-14s \e[0;34m->\e[0;0m ' "${source_file}" "${line_number}" "${func_name}"
  printf '\e[0;34;47m%-15s \e[1;31;47m%-4d\e[0;30;47m %-14s \e[0;0m ' "${source_file}" "${line_number}" "${func_name}"
  return "${EXIT_SUCCESS}"
}


#######################################
# debug::enable
# Turn on the debug mode
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   EXIT_SUCCESS
# Examples:
#   debug::enable
#######################################
debug::enable(){
  if "${is_debug_asked}" || "${is_cnrgh_bash_lib_debug_asked}"; then
    is_debug_enabled=true
    set -x
  fi
  return "${EXIT_SUCCESS}"
}


#######################################
# debug::disable
# Turn off the debug mode
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   EXIT_SUCCESS
# Examples:
#   debug::disable
#######################################
debug::disable(){
  if "${is_debug_asked}" || "${is_cnrgh_bash_lib_debug_asked}"; then
    set +x
    is_debug_enabled=false
  fi
  return "${EXIT_SUCCESS}"
}

# Not used
# do not trace into bash lib unless the env var CNRGH_BASH_LIB_DEBUG is true
debug::internal_switch(){
  if "${is_debug_asked}" && ! "${is_cnrgh_bash_lib_debug_asked}"; then
    if "${is_debug_enabled}"; then
      set +x
      is_debug_enabled=false
    else
      is_debug_enabled=true
      set -x
    fi
  fi
}

declare -fx debug::format
declare -rx  PS4='$(debug::format "${BASH_SOURCE}" "${LINENO}")'
debug::read_env_var

fi
