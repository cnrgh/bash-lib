# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_HANDLER} ]] || ! ${CNRGH_LIB_HANDLER}; then

declare -r CNRGH_LIB_HANDLER=true
# shellcheck source=src/output.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/output.sh'
# shellcheck source=src/file.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/file.sh'


#######################################
# handler::error:enable
# Enable error handling. If a command return an exit status other than EXIT_SUCCESS a report is
# diplayed on stderr and exit with the same exit code than the called command.
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   Exit with an exit code greater than EXIT_SUCCESS"
# Examples:
#
#######################################
handler::error::enable(){
  trap 'handler::report::error "${BASH_SOURCE[0]}" "${LINENO}" "$?"' ERR
  set -o errtrace
  
# XXX Changing locale as a side effect of a function is a real bad thing to do.
#  declare -x LANG=C;
#  declare -x LANG_ALL=C

# XXX Another bad thing to do: changing default shell separator
#  IFS=$' \t\n'
}

handler::error::disable(){
    trap - ERR
}

#######################################
# handler::report::error
# Use internally to print an error message before to exit.
# Note: if command is not launched with command::run the described command could to not be the
# failed command
#
# Globals:
#   None
# Arguments:
#   1/ file origin where error is raised
#   2/ the corresponding line number
#   3/ the exit code [default: EXIT_FAILURE]
# Returns:
#   Exit EXIT_FAILURE if command fail otherwise return EXIT_SUCCESS
# Examples:
#
#######################################
handler::report::error() {
  local -r  error_from_file="$1"
  local -ri line_no="${2}"
  local -ri exit_code="${3:-${EXIT_FAILURE}}"
  local     last_cmd=''
  local     msg
  msg="Error on $(output::style::bg cyan)$(output::style underline)${error_from_file}$(output::style reset) at line $(output::style::bg cyan)${line_no}$(output::style reset) with the exit code $(output::style::bg cyan)${exit_code}$(output::style reset)"
  if file::exists "${TMP_HISTORY_LOG}"; then
    last_cmd="$(tail -1 "${TMP_HISTORY_LOG}")"
    msg+="\nWhile running:\n\t$(output::style::bg yellow)->$(output::style reset) ${last_cmd}"
  fi
  output::error "${msg}"
  exit "${exit_code}"
}

fi
