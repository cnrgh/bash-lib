# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_OUTPUT} ]] || ! ${CNRGH_LIB_OUTPUT}; then
declare -r CNRGH_LIB_OUTPUT=true
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../output.sh'
LG_ERROR_IS_FATAL=false
source "$(dirname "${BASH_SOURCE[0]}")"'/../logging.sh'
lg_disable_color_tag_delimiters
lg_disable_color_msg
lg_set_color warning dark_yellow
lg_set_color info dark_cyan
source "$(dirname "${BASH_SOURCE[0]}")"'/../deprecate.sh'
dp_deprecation_warning "src/output.sh is deprecated," \
    "please use logging.sh and output.sh instead."

output::error(){
    dp_deprecated "lg_error" "logging.sh"
    lg_error "$@"
}


output::warning(){
    dp_deprecated "lg_warning" "logging.sh"
    lg_warning "$@"
}


output::info(){
    dp_deprecated "lg_info" "logging.sh"
    lg_info "$@"
}


# define ANSI code to style output
declare -rA STYLE_BG=( [normal,black]=$OP_FG_BLACK
                           [normal,red]=$OP_FG_RED
                           [normal,green]=$OP_FG_GREEN
                           [normal,yellow]=$OP_FG_BROWN
                           [normal,blue]=$OP_FG_BLUE
                           [normal,purple]=$OP_FG_PURPLE
                           [normal,cyan]=$OP_FG_CYAN
                           [normal,white]=$OP_FG_LIGHT_GRAY
                           [bold,black]=$OP_FG_DARK_GRAY
                           [bold,red]=$OP_FG_LIGHT_RED
                           [bold,green]=$OP_FG_LIGHT_GREEN
                           [bold,yellow]=$OP_FG_YELLOW
                           [bold,blue]=$OP_FG_LIGHT_BLUE
                           [bold,purple]=$OP_FG_LIGHT_PURPLE
                           [bold,cyan]=$OP_FG_LIGHT_CYAN
                           [bold,white]=$OP_FG_WHITE
                           [italic,black]=$OP_FG_ITALIC_BLACK
                           [italic,red]=$OP_FG_ITALIC_RED
                           [italic,green]=$OP_FG_ITALIC_GREEN
                           [italic,yellow]=$OP_FG_ITALIC_YELLOW
                           [italic,blue]=$OP_FG_ITALIC_BLUE
                           [italic,purple]=$OP_FG_ITALIC_PURPLE
                           [italic,cyan]=$OP_FG_ITALIC_CYAN
                           [italic,white]=$OP_FG_ITALIC_WHITE
                           [underline,black]=$OP_FG_UNDERLINE_BLACK
                           [underline,red]=$OP_FG_UNDERLINE_RED
                           [underline,green]=$OP_FG_UNDERLINE_GREEN
                           [underline,yellow]=$OP_FG_UNDERLINE_YELLOW
                           [underline,blue]=$OP_FG_UNDERLINE_BLUE
                           [underline,purple]=$OP_FG_UNDERLINE_PURPLE
                           [underline,cyan]=$OP_FG_UNDERLINE_CYAN
                           [underline,white]=$OP_FG_UNDERLINE_WHITE
                           [strikethrough,black]=$OP_FG_STRUCK_BLACK
                           [strikethrough,red]=$OP_FG_STRUCK_RED
                           [strikethrough,green]=$OP_FG_STRUCK_GREEN
                           [strikethrough,yellow]=$OP_FG_STRUCK_YELLOW
                           [strikethrough,blue]=$OP_FG_STRUCK_BLUE
                           [strikethrough,purple]=$OP_FG_STRUCK_PURPLE
                           [strikethrough,cyan]=$OP_FG_STRUCK_CYAN
                           [strikethrough,white]=$OP_FG_STRUCK_WHITE
                       )

declare -rA STYLE_FG=( [normal,black]=$OP_BG_BLACK
                           [normal,red]=$OP_BG_RED
                           [normal,green]=$OP_BG_GREEN
                           [normal,yellow]=$OP_BG_BROWN
                           [normal,blue]=$OP_BG_BLUE
                           [normal,purple]=$OP_BG_PURPLE
                           [normal,cyan]=$OP_BG_CYAN
                           [normal,white]=$OP_BG_LIGHT_GRAY
                           [bold,black]=$OP_BG_DARK_GRAY
                           [bold,red]=$OP_BG_LIGHT_RED
                           [bold,green]=$OP_BG_LIGHT_GREEN
                           [bold,yellow]=$OP_BG_LIGHT_YELLOW
                           [bold,blue]=$OP_BG_LIGHT_BLUE
                           [bold,purple]=$OP_BG_LIGHT_PURPLE
                           [bold,cyan]=$OP_BG_LIGHT_CYAN
                           [bold,white]=$OP_BG_LIGHT_WHITE
                           
                           # XXX The following definitions have no sense:
                           # There exists no italic, stroke, or underlined
                           # backgrounds.
                           [italic,black]='\e[3;40m'
                           [italic,red]='\e[3;41m'
                           [italic,green]='\e[3;42m'
                           [italic,yellow]='\e[3;43m'
                           [italic,blue]='\e[3;44m'
                           [italic,purple]='\e[3;45m'
                           [italic,cyan]='\e[3;46m'
                           [italic,white]='\e[3;47m'
                           [underline,black]='\e[4;40m'
                           [underline,red]='\e[4;41m'
                           [underline,green]='\e[4;42m'
                           [underline,yellow]='\e[4;43m'
                           [underline,blue]='\e[4;44m'
                           [underline,purple]='\e[4;45m'
                           [underline,cyan]='\e[4;46m'
                           [underline,white]='\e[4;47m'
                           [strikethrough,black]='\e[9;40m'
                           [strikethrough,red]='\e[9;41m'
                           [strikethrough,green]='\e[9;42m'
                           [strikethrough,yellow]='\e[9;43m'
                           [strikethrough,blue]='\e[9;44m'
                           [strikethrough,purple]='\e[9;45m'
                           [strikethrough,cyan]='\e[9;46m'
                           [strikethrough,white]='\e[9;47m' )


declare -r STYLE_RESET=$OP_RESET
declare -r STYLE_BOLD=$OP_BOLD
declare -r STYLE_ITALIC=$OP_ITALIC
declare -r STYLE_UNDERLINE=$OP_UNDERLINE
declare -r STYLE_STRIKETHROUGH=$OP_STRUCK


#######################################
# output::style
# Provides the ANSI sequence for styles: reset, bold, italic, underline, strikethrough
#
# Globals:
#   None
# Arguments:
#   1/ One of style value
# Returns:
#   Status code EXIT_SUCSESS if the given style is supported otherwise EXIT_FAILURE
# Examples:
#  echo -e "$(output:style italic)this is italic$(output::style reset) but this is not in italic"
#######################################
output::style(){

    dp_deprecation_warning "Function ${FUNCNAME[1]} is deprecated." \
        "Please use constants in output.sh instead."

    local -ru name="$1"
    local -r style=STYLE_$name

    # Empty name?
    [[ -n $name ]] || lg_fatal 'Missing style, one of these values is' \
        ' expected: reset, italic, underline, strikethrough'
    
    # Unknown style?
    [[ ! ${!style} ]] && lg_fatal "Unknown style: OP_$name."
    
    # Set style
    echo "${!style}"
    
    return 0
}


#######################################
# output::style::bg
# Provides the ANSI sequence for a background style.
# Supported mode: normal, bold, italic, underline, strikethrough
# Supported color: black, red, green, yellow, blue, purple, cyan, white
#
# Globals:
#   None
# Arguments:
#   1/ is formated such as: color,mode [default mode is normal]
# Returns:
#   Status code EXIT_SUCCESS if the combination mode,color exists otherwise EXIT_FAILURE
# Examples:
#  echo -e "$(output::style::bg green) it is green$(output::style reset)"
#  echo -e "$(output::style::bg yellow,bold) it is bold and yellow$(output::style reset)"
#######################################
output::style::bg(){

    dp_deprecation_warning "Function ${FUNCNAME[1]} is deprecated." \
        "Please use constants in output.sh instead."

  local color="$1"
  local mode="${2:-normal}"
  local -i status_code=${EXIT_FAILURE}
  local -r value="${STYLE_BG[${mode},${color}]}"
  if [[ -n "${value}" ]]; then
    echo "${value}"
    status_code=${EXIT_SUCCESS}
  else
    output::error 'Unknown mode and/or color: '"${mode},${color}"
  fi
  return "${status_code}"
}


#######################################
# output::style::fg
# Provides the ANSI sequence for a foreground style.
# Supported mode: normal, bold, italic, underline, strikethrough
# Supported color: black, red, green, yellow, blue, purple, cyan, white
#
# Globals:
#   None
# Arguments:
#   1/ is formated such as: color,mode [default mode is normal]
# Returns:
#   Status code EXIT_SUCCESS if the combination mode,color exists otherwise EXIT_FAILURE
# Examples:
#  echo -e "$(output::style::fg green) it is green$(output::style reset)"
#  echo -e "$(output::style::fg yellow,bold) it is bold and yellow$(output::style reset)"
#######################################
output::style::fg(){

    dp_deprecation_warning "Function ${FUNCNAME[1]} is deprecated." \
        "Please use constants in output.sh instead."

  local color="$1"
  local mode="${2:-normal}"
  local -i status_code=${EXIT_FAILURE}
  local -r value="${STYLE_FG[${mode},${color}]}"
  if [[ -n "${value}" ]]; then
    echo "${value}"
    status_code=${EXIT_SUCCESS}
  else
    output::error 'Unknown mode and/or color: '"${mode},${color}"
  fi
  return "${status_code}"
}


fi
