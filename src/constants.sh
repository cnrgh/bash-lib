# CNRGH Bash module to provides some constants

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_CONSTANTS} ]] || ! ${CNRGH_LIB_CONSTANTS}; then

declare -r CNRGH_LIB_CONSTANTS=true

declare -rix EXIT_SUCCESS=0
declare -rix EXIT_FAILURE=1
declare -rx  TMP_FILE_TEMPLATE='cnrgh.bash_lib.XXXXXXX'
# PID of current process
declare -rix PID="$$"

# **DEPRECATED**
## Define a temporary working directory at runtime
## to put intermediate result
## TODO sometime we would use anther root dir than ${TMPDIR}
#declare -x TMP_WORKING_DIR
#TMP_WORKING_DIR="$(mktemp -d -t "${TMP_FILE_TEMPLATE}")"
#readonly TMP_WORKING_DIR
## Define a temporary log directory at runtime
#declare -x TMP_LOG_DIR
#TMP_LOG_DIR="$(mktemp -d -t "${TMP_FILE_TEMPLATE}")"
#readonly TMP_LOG_DIR
## Define an history log file to keep the trace of command called during the pipeline
#declare -rx TMP_HISTORY_LOG="${TMP_LOG_DIR}/history"

if [[ -z "${CNRGH_BASH_LIBDIR}" ]]; then
declare -x CNRGH_BASH_LIBDIR
  CNRGH_BASH_LIBDIR="$(dirname "${BASH_SOURCE[0]}")"
  readonly CNRGH_BASH_LIBDIR
fi

fi
