# shellcheck disable=SC2148,SC1105

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_VERSION} ]] || ! ${CNRGH_LIB_VERSION}; then

declare -r CNRGH_LIB_VERSION=true
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'
# shellcheck source=src/string.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/string.sh'
# This module expect that versions are formated as follow: \d+.\d+.\d+
source "$(dirname "${BASH_SOURCE[0]}")"'/../ver.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../logging.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../deprecate.sh'
dp_deprecation_warning "src/version.sh is deprecated," \
    "please use ver.sh instead."

declare -Ar invert_sign=( ['>']='<' ['<']='>' ['>=']='<=' ['<=']='>=' ['==']='!=' ['!=']='==' )

#######################################
# version::equals
# Inform if a version is equal to another one
#
# Globals:
#   None
# Arguments:
#   1/ version 1
#   2/ version 2
# Returns:
#   EXIT_SUCCESS when versions are equal otherwise EXIT_FAILURE
# Examples:
#  if version::equals '1.0.0' '1.0.0'; then
#    do_something
# fi
#######################################
version::equals(){
    dp_deprecated "vr_cmp" "ver.sh"
    local -i status_code=${EXIT_FAILURE}
    [[ $(vr_cmp "$@") -eq 0 ]] && status_code=${EXIT_SUCCESS}
    return "${status_code}"
}

#######################################
# version::is
# Compare two versions together using one of these signs: > >= == <= < !=
#
# Globals:
#   None
# Arguments:
#   1/ version 1
#   2/ sign
#   3/ version 2
# Returns:
#   EXIT_SUCCESS when versions comapraison is true otherwise EXIT_FAILURE
# Examples:
#  if version::is '1.1.0' '>' '1.0.0'; then
#    do_something
# fi
#######################################
version::is(){
    dp_deprecated "vr_cmp" "ver.sh"
    local -i status_code=${EXIT_FAILURE}
    local -r version1="${1}"
    local -r sign="${2}"
    local -r version2="${3}"
    local -r cmp=$(vr_cmp "$version1" "$version2")

    case $sign in
        '==') [[ $cmp -eq 0 ]] && status_code=${EXIT_SUCCESS} ;;
        '!=') [[ $cmp -ne 0 ]] && status_code=${EXIT_SUCCESS} ;;
        '>=') [[ $cmp -ge 0 ]] && status_code=${EXIT_SUCCESS} ;;
        '<=') [[ $cmp -le 0 ]] && status_code=${EXIT_SUCCESS} ;;
        '>') [[ $cmp -gt 0 ]] && status_code=${EXIT_SUCCESS} ;;
        '<') [[ $cmp -lt 0 ]] && status_code=${EXIT_SUCCESS} ;;
        *) lg_fatal "Unknown operator \"$sign\"." ;;
    esac
    
    return "${status_code}"
}
fi

true
