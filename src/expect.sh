# shellcheck disable=SC2148,SC1105

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_EXPECT} ]] || ! ${CNRGH_LIB_EXPECT}; then
declare -r CNRGH_LIB_EXPECT=true
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'
# shellcheck source=src/output.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/output.sh'
# shellcheck source=src/file.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/file.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../fs.sh'
# shellcheck source=src/dictionary.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/dictionary.sh'

declare -Ax test_is_successful=()
declare     _test_output_file=''
declare     _test_error_file=''


#######################################
# expect::success
# test a feature if exit successfully as expected
#
# Globals:
#   None
# Arguments:
#   1/ file name
# Returns:
#   EXIT_SUCCESS if command is successful otherwise EXIT_FAILURE.
# Examples:
#   expect::success ls -l
#######################################
expect::success(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# > 0 )); then
    local -ar cmd=( "$@" )
    declare _test_output_file=
    declare _test_error_file=
    fs_tmp_acquire_file "bash_lib.old_lib.success.stdout" || \
        lg_fatal "Failed creating temporary file."
    # shellcheck disable=SC2119
    _test_output_file="$(fs_tmp_last_path)"
    fs_tmp_acquire_file "bash_lib.old_lib.success.stderr" || \
        lg_fatal "Failed creating temporary file."
    # shellcheck disable=SC2119
    _test_error_file="$(fs_tmp_last_path)"
    ( "${cmd[@]}" 1> "${_test_output_file}" 2> "${_test_error_file}" )
    status_code=$?

    if (( status_code == 0 )); then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The test failed with the status code: ${status_code}"
      output::error "\t└──> ${cmd[*]}"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect at least one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <a command>"
  fi
  fs_tmp_release_last 2
  return "${status_code}"
}


#######################################
# expect::failure
# test a feature if exit badly as expected
#
# Globals:
#   None
# Arguments:
#   1/ file name
# Returns:
#   EXIT_SUCCESS if command has failed otherwise EXIT_FAILURE.
# Examples:
#   expect::failure this_do_not_exists
#######################################
expect::failure(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# > 0 )); then
    local -ar cmd=( "$@" )

    # file::get_tmp  take 0 or 1 parameter false warning
    declare _test_output_file=
    declare _test_error_file=
    fs_tmp_acquire_file "bash_lib.old_lib.success.stdout" || \
        lg_fatal "Failed creating temporary file."
    # shellcheck disable=SC2119
    _test_output_file="$(fs_tmp_last_path)"
    fs_tmp_acquire_file "bash_lib.old_lib.success.stderr" || \
        lg_fatal "Failed creating temporary file."
    # shellcheck disable=SC2119
    _test_error_file="$(fs_tmp_last_path)"
    ( "${cmd[@]}" 1> "${_test_output_file}" 2> "${_test_error_file}" )
    status_code=$?

    if (( status_code != 0 )); then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The test failed with the status code: ${status_code}"
      output::error "\t└──> ${cmd[*]}"
      status_code="${EXIT_FAILURE}"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect at least one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <a command>"
  fi
  fs_tmp_release_last 2
  return "${status_code}"

}

#######################################
# expect::error_is_equal_to
# test if a feature has provided the expected result
#
# Globals:
#   None
# Arguments:
#   1/ expected result
# Returns:
#   EXIT_SUCCESS if the feature result meet the expectation otherwise EXIT_FAILURE.
# Examples:
#   expect::success echo ok
#   expect::error_is_equal_to 'ok'
#######################################
expect::error_is_equal_to(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 1 )); then
    local -r expected_result="${1}"
    local -r obtained_result="$(<"${_test_error_file}")"
    if [[ "${expected_result}" == "${obtained_result}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The expected result: «$(output::style::bg yellow)${expected_result}$(output::style reset)» is different of obtained result: «$(output::style::bg green)${obtained_result}$(output::style reset)»"
      output::error "\t└──> parameters used: $*"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect at least one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <expected value>"
  fi
  return "${status_code}"
}


#######################################
# expect::error_is_not_empty
# test if a feature has provided the expected result
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   EXIT_SUCCESS if the feature result meet the expectation otherwise EXIT_FAILURE.
# Examples:
#   expect::success this_cmd_do_not_exists
#   expect::error_is_not_empty
#######################################
expect::error_is_not_empty(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# >= 1 )); then
    local -r obtained_result="$(<"${_test_error_file}")"
    if [[ -n "${obtained_result}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The obtained result is empty: «${obtained_result}»"
      output::error "\t└──> parameters used: $*"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect at least one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]}"
  fi
  return "${status_code}"
}


#######################################
# expect::output_is_equal_to
# test if a feature has provided the expected result
#
# Globals:
#   None
# Arguments:
#   1/ expected result
# Returns:
#   EXIT_SUCCESS if the feature result meet the expectation otherwise EXIT_FAILURE.
# Examples:
#   expect::success echo ok
#   expect::output_is_equal_to 'ok'
#######################################
expect::output_is_equal_to(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 1 )); then
    local -r expected_result="${1}"
    local -r obtained_result="$(<"${_test_output_file}")"
    if [[ "${expected_result}" == "${obtained_result}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The expected result: «$(output::style::bg yellow)${expected_result}$(output::style reset)» is different of obtained result: «$(output::style::bg green)${obtained_result}$(output::style reset)»"
      output::error "\t└──> parameters used: $*"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect at least one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <expected value>"
  fi
  return "${status_code}"
}


#######################################
# expect::output_is_not_empty
# test if a feature has provided the expected result
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   EXIT_SUCCESS if the feature result meet the expectation otherwise EXIT_FAILURE.
# Examples:
#   expect::success echo ok
#   expect::output_is_not_empty
#######################################
expect::output_is_not_empty(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# >= 1 )); then
    local -r obtained_result="$(<"${_test_output_file}")"
    if [[ -n "${obtained_result}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The obtained result is empty: «${obtained_result}»"
      output::error "\t└──> parameters used: $*"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect at least one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]}"
  fi
  return "${status_code}"
}


#######################################
# expect::file_exists
# test if a file exists
#
# Globals:
#   None
# Arguments:
#   1/ path to a file
# Returns:
#   EXIT_SUCCESS if the file exists otherwise EXIT_FAILURE.
# Examples:
#   expect::file_exists 'foo.txt'
#######################################
expect::file_exists(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 1 )) && [[ -n "${1}" ]]; then
    local -r file_path="${1}"
    if [[ -f "${file_path}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "They are no file at this location: «${file_path}»"
      output::error "\t└──> parameters used: $*"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <path to a file>"
  fi
  return "${status_code}"
}

#######################################
# expect::link_exists
# test if a link exists
#
# Globals:
#   None
# Arguments:
#   1/ path to a link
# Returns:
#   EXIT_SUCCESS if the link exists otherwise EXIT_FAILURE.
# Examples:
#   expect::file_exists 'foo.txt'
#######################################
expect::link_exists(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 1 )) && [[ -n "${1}" ]]; then
    local -r link_path="${1}"
    if [[ -L "${link_path}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "They are no link at this location: «${link_path}»"
      output::error "\t└──> parameters used: $*"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <path to a link>"
  fi
  return "${status_code}"
}


#######################################
# expect::directory_exists
# test if a directory exists
#
# Globals:
#   None
# Arguments:
#   1/ path to a directory
# Returns:
#   EXIT_SUCCESS if the directory exists otherwise EXIT_FAILURE.
# Examples:
#   expect::file_exists 'foo.txt'
#######################################
expect::directory_exists(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 1 )) && [[ -n "${1}" ]]; then
    local -r directory_path="${1}"
    if [[ -d "${directory_path}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "They are no directory at this location: «${directory_path}»"
      output::error "\t└──> parameters used: $*"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <path to a directory>"
  fi
  return "${status_code}"
}


#######################################
# expect::string_is_not_empty
# test if a value is not empty
#
# Globals:
#   None
# Arguments:
#   1/ value
# Returns:
#   EXIT_SUCCESS if the value is not empty otherwise EXIT_FAILURE.
# Examples:
#   expect::string_is_not_empty 'foo.txt'
#######################################
expect::string_is_not_empty(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 1 )); then
    local -r value="${1}"
    if [[ -n "${value}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The value is empty!"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <value>"
  fi
  return "${status_code}"
}


#######################################
# expect::string_is_empty
# test if a value is empty
#
# Globals:
#   None
# Arguments:
#   1/ value
# Returns:
#   EXIT_SUCCESS if the value is empty otherwise EXIT_FAILURE.
# Examples:
#   expect::string_is_empty ''
#######################################
expect::string_is_empty(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 1 )); then
    local -r value="${1}"
    if [[ -z "${value}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The value is not empty: «${value}»!"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect one parameter!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <value>"
  fi
  return "${status_code}"
}


#######################################
# expect::string_is_equal_to
# test if a directory exists
#
# Globals:
#   None
# Arguments:
#   1/ value expected
#   2/ value obtained
# Returns:
#   EXIT_SUCCESS if values are equal otherwise EXIT_FAILURE.
# Examples:
#   expect::string_is_equal_to 'foo' "${my_var}"
#######################################
expect::string_is_equal_to(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 2 )); then
    local -r expected="${2}"
    local -r obtained="${1}"
    if [[ "${expected}" == "${obtained}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The obtained value «${obtained}» is not equal to «${expected}»!"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect two parameters!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <expected value> <obtained value>"
  fi
  return "${status_code}"
}


#######################################
# expect::string is_not_equal_to
# test if a directory exists
#
# Globals:
#   None
# Arguments:
#   1/ value expected
#   2/ value obtained
# Returns:
#   EXIT_SUCCESS if values are equal otherwise EXIT_FAILURE.
# Examples:
#   expect::string_is_not_equal_to 'foo' "${my_var}"
#######################################
expect::string_is_not_equal_to(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 2 )); then
    local -r expected="${1}"
    local -r obtained="${2}"
    if [[ "${expected}" != "${obtained}" ]]; then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The obtained value «${obtained}» is not different to «${expected}»!"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect two parameters!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]}  <expected value> <obtained value>>"
  fi
  return "${status_code}"
}

#######################################
# expect::arithmetic_is_true
# Perform an arithemetic comparison
#
# Globals:
#   None
# Arguments:
#   1/  number
#   2/  sign (quotes around sing are mandatory )
#   3/  integer
# Returns:
#   EXIT_SUCCESS if the comparison is true otherwise EXIT_FAILURE.
# Examples:
#   expect::arithmetic_is_true  0 '=='  0
#   expect::arithmetic_is_true 10 '!='  0
#   expect::arithmetic_is_true 0  '>='  0
#   expect::arithmetic_is_true 0  '<='  0
#   expect::arithmetic_is_true 10  '>'  0
#   expect::arithmetic_is_true  0  '<' 10
#   expect::arithmetic_is_true 10  '>'  0
#######################################
expect::arithmetic_is_true(){
  local -i  status_code="${EXIT_FAILURE}"
  if (( $# == 3 )) && [[ -n "${1}" && -n "${2}" && -n "${3}" ]]; then
    local -ri number1="${1}"
    local -r  sign="${2}"
    local -ri number2="${3}"
    if (( number1 "${sign}" number2 )); then
      status_code="${EXIT_SUCCESS}"
      if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
        test_is_successful["${FUNCNAME[1]}"]=true
      fi
    else
      test_is_successful["${FUNCNAME[1]}"]=false
      output::error "The expression: «${number1} ${sign} ${number2}» is false"
    fi
  else
    test_is_successful["${FUNCNAME[1]}"]=false
    output::error "${FUNCNAME[0]} expect three parameters!"
    output::error "\t└──> parameters used: $*"
    output::error "\t└──> Usage: ${FUNCNAME[0]} <number> <sign> <number>"
  fi
  return "${status_code}"
}

#######################################
# expect::report_function_status
# Print the final status of the givein tested function
#
# Globals:
#   None
# Arguments:
#
# Returns:
#   EXIT_SUCCESS if the feature result meet the expectation otherwise EXIT_FAILURE.
# Examples:
#   expect::report_function_status
#######################################
expect::report_function_status(){
  local is_ok=false
  if ! dictionary::has_key "${FUNCNAME[1]}" test_is_successful; then
    is_ok=false
  else
    is_ok="$(dictionary::get "${FUNCNAME[1]}" test_is_successful)"
  fi

  if ${is_ok}; then
     printf "%-70s [$(output::style::bg green)%s$(output::style reset)]\n" "${FUNCNAME[1]}" 'OK'
  else
    printf "%-70s [$(output::style::bg red)%s$(output::style reset)]\n" "${FUNCNAME[1]}" 'NO'
  fi
  return "$?"
}


fi
