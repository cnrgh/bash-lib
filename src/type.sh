# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_TYPE} ]] || ! ${CNRGH_LIB_TYPE}; then
declare -r CNRGH_LIB_TYPE=true
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'
source "$(dirname "${BASH_SOURCE[0]}")"'/../deprecate.sh'
dp_deprecation_warning "src/type.sh is deprecated," \
    "please use introspect.sh instead."
source "$(dirname "${BASH_SOURCE[0]}")"'/../introspect.sh'

#######################################
# type::declaration
# This function provides attributes bound to the given variable (man builtins).
# Such as : a A i n f r x t u l …
#
# Globals:
#   None
# Arguments:
#   1/ variable name
# Returns:
#   EXIT_SUCCESS if value passing from echo is successful otherwise a value greater than 0
#   
# Examples:
#   declaraction="$(type::declaration my_var)"
#######################################
type::declaration(){
    dp_deprecated "in_get_var_flags" "introspect.sh"
    in_get_var_flags "$1"
}


#######################################
# type::typeof
# This function provides the type of the variable.
# Possibe value:
#  - array
#  - hash
#  - integer
#  - function
#  - nameref
#  - string 
#
# Globals:
#   None
# Arguments:
#   1/ variable name
# Returns:
#   EXIT_SUCCESS if value passing from echo is successful otherwise a value greater than 0
#   
# Examples:
#   my_type="$(type::typeof my_var)"
#######################################
type::typeof(){
    dp_deprecated "in_typeof" "introspect.sh"
    var_type=$(in_typeof "$1")
    case $var_type in
        dict) echo "hash" ;;
        int) echo "integer" ;;
        str) echo "string" ;;
        *) echo "$var_type" ;;
    esac
}


#######################################
# type::is
# This function indicate the variable has the expected type.
#
# Globals:
#   None
# Arguments:
#   1/ variable name
# Returns:
#   EXIT_SUCCESS if the variable has the expected type otherwise EXIT_FAILURE
#   
# Examples:
#   if type::is string my_var; then
#     …
#   fi
#######################################
type::is(){
    dp_deprecated "in_is" "introspect.sh"
    in_is "$1" "$2"
}

fi
