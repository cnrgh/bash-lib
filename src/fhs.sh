# CNRGH Bash module to get directory location followinf file hierarchy standard specification

# shellcheck disable=SC2148

# if var do not exist or is not equal to true then load script file otherwise do nothing (already done)
if [[ -z ${CNRGH_LIB_FHS} ]] || ! ${CNRGH_LIB_FHS}; then
declare -r CNRGH_LIB_FHS=true
# shellcheck source=src/output.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/output.sh'
# shellcheck source=src/constants.sh
source "$(dirname "${BASH_SOURCE[0]}")"'/constants.sh'

#######################################
# fhs::prefix()
# Return by default standard prefix directory location (/usr/).
# Otherwise return the custom prefix:
#   *  -e or --executable parameter is used, the result_path will be relative to the provided executable.
#   *  -l or --local is used the result_path will be the standard prefix for non system application (/usr/local).
#   *  -u or --user-home is used the result_path will be relative to the selected home directory.
# NOTE: Parameters --executable, --local and --user are mutualy exclusives
# Globals:
#   None
# Parameters:
#   -e|--executable <executable name> executable name use to determine the bin directory
#   -l|--local                        Standard Hierarchy use by the system administrator when installing software locally.
#                                     It needs to be safe from being overwritten when the system software is updated. (/usr/local)
#   -u|--user-home  [${HOME}]         Standard bin directory for a user (default use current user home: $HOME)
# Returns:
#   Exit 1 if command fail otherwise return 0
# Examples:
#  fhs::prefix
#  fhs::prefix --local
#  fhs::prefix --user
#  fhs::prefix --user /home/foo
#  fhs::prefix --executable bwa
#######################################
fhs::prefix(){
  local -i status_code="${EXIT_FAILURE}"
  local executable_name
  local executable_path
  local executable_dir
  local user_home
  local result_path
  if (( $# == 0 )); then
    result_path="$(realpath '/usr')"
    status_code="${EXIT_SUCCESS}"
  else
    while (( $# >= 1 )); do
      case "$1" in
        -e|--executable)
          if (( $# == 2 )); then
            executable_name="$2"; shift
            if [[ "${executable_name:0:1}" == '/' || "${executable_name:0:2}" == './' ]]; then
              executable_dir="$(dirname "${executable_name}")"
              result_path="$(realpath "${executable_dir}"'/..')"
              status_code="$?"
            else
              executable_path="$(command -v "${executable_name}")"
              executable_dir="$(dirname "${executable_path}")"
              result_path="$(realpath "${executable_dir}"'/..')"
              status_code="$?"
            fi
          else
            output::error "${FUNCNAME[0]} with -e|--executable expect an executable name"
            output::error "Example: ${FUNCNAME[0]} --executable bwa "
            status_code="${EXIT_FAILURE}"
          fi
          ;;
        -l|--local)
          result_path="$(realpath '/usr/local')"
          status_code="${EXIT_SUCCESS}"
          ;;
        -u|--user)
          if (( $# == 2 )); then
            user_home="$2"; shift
          else
            user_home="${HOME}"
          fi
          result_path="$(realpath "${user_home}"'/.local')"
          status_code="$?"
          ;;
        *)
          output::error "${FUNCNAME[0]} allow only -e|--executable or -u|--user-home or -l|--local as parameters"
          output::error "\t└──> parameters used: $*"
          output::error "\t└──> Usage: ${FUNCNAME[0]} [-e|--executable <name> ] | [-u|--user-home [HOME] ] | [-l|--local ]"
          status_code="${EXIT_FAILURE}"
          ;;
      esac
      shift
    done
  fi
  echo "${result_path}"
  return "${status_code}"
}


#######################################
# fhs::bindir()
# Return by default standard bin directory location (/usr/bin).
# Otherwise return the custom bin directory:
#   * -e or --executable parameter is used, the result_path will be relative to the provided executable.
#   * -l or --local is used the result_path will be the standard bin for non system application (/usr/local/bin).
#   * -u or --user-home is used the result_path will be relative to the selected home directory (${HOME}/.local/bin).
# NOTE: Parameters --executable and --user are mutual exclusive
# Globals:
#   None
# Parameters:
#   -e|--executable <executable name> executable name use to determine the bin directory
#   -l|--local                        Standard Hierarchy use by the system administrator when installing software locally.
#                                     It needs to be safe from being overwritten when the system software is updated.
#   -u|--user-home  [${HOME}]         Standard bin directory for a user (default use current user home: $HOME)
# Returns:
#   Exit 1 if command fail otherwise return 0
# Examples:
#  fhs::bindir
#  fhs::bindir --local
#  fhs::bindir --user
#  fhs::bindir --executable bwa
#######################################
fhs::bindir(){
  local -i status_code="${EXIT_FAILURE}"
  local result_path
  local prefix
  local -i counter=0
  if (( $# == 0 )); then
    result_path="$(realpath '/usr/bin')"
    status_code="${EXIT_SUCCESS}"
  else
    while (( $# >= 1 )); do
      case "$1" in
        -p|--prefix)
          if (( $# < 2 )); then
            output::error "${FUNCNAME[0]} allow only -e|--executable or -u|--user-home or -l|--local or -p|--prefix as parameters"
            output::error "\t└──> parameters used: $*"
            output::error "\t└──> Usage: ${FUNCNAME[0]} [-e|--executable <name> ] | [-u|--user-home [HOME] ] | [-l|--local ] | [-p|--prefix <path>]"
            status_code="${EXIT_FAILURE}"
          fi
          result_path="$(realpath  "$2/bin")"
          status_code="$?"
          shift
          ;;
        *)
          prefix="$(fhs::prefix "$@")"
          counter=$#
          result_path="$(realpath "${prefix}/bin")"
          while (( counter != 1 )); do shift; ((counter--)); done
          status_code="$?"
          ;;
      esac
      shift
    done
  fi
  echo "${result_path}"
  return "${status_code}"
}


#######################################
# fhs::datadir()
# Return by default standard data directory location (/usr/share).
# Otherwise return the custom data directory:
#   * -e or --executable parameter is used, the result_path will be relative to the provided executable.
#   * -l or --local is used the result_path will be the standard bin for non system application (/usr/local/share).
#   * -u or --user-home is used the result_path will be relative to the selected home directory (${HOME}/.local/share).
# NOTE: Parameters --executable and --user are mutual exclusive
# Globals:
#   None
# Parameters:
#   -e|--executable <executable name> executable name use to determine the data directory
#   -l|--local                        Standard Hierarchy use by the system administrator when installing software locally.
#                                     It needs to be safe from being overwritten when the system software is updated.
#   -u|--user-home  [${HOME}]         Standard data directory for a user (default use current user home: $HOME)
# Returns:
#   Exit 1 if command fail otherwise return 0
# Examples:
#  fhs::datadir
#  fhs::datadir --local
#  fhs::datadir --user
#  fhs::datadir --executable bwa
#######################################
fhs::datadir(){
  local -i status_code="${EXIT_FAILURE}"
  local result_path
  local prefix
  if (( $# == 0 )); then
    result_path="$(realpath '/usr/share')"
    status_code="${EXIT_SUCCESS}"
  else
    while (( $# >= 1 )); do
      case "$1" in
        -p|--prefix)
          if (( $# < 2 )); then
            output::error "${FUNCNAME[0]} allow only -e|--executable or -u|--user-home or -l|--local or -p|--prefix as parameters"
            output::error "\t└──> parameters used: $*"
            output::error "\t└──> Usage: ${FUNCNAME[0]} [-e|--executable <name> ] | [-u|--user-home [HOME] ] | [-l|--local ] | [-p|--prefix <path>]"
            status_code="${EXIT_FAILURE}"
          fi
          result_path="$(realpath "$2/share")"
          status_code="$?"
          shift
          ;;
        *)
          prefix="$(realpath "$(fhs::prefix "$@")")"
          result_path="$(realpath "${prefix}/share")"
          # all parameters have been processed, remove them
          counter=$#
          while (( counter != 1 )); do shift; ((counter--)); done
          status_code="$?"
          ;;
      esac
      shift
    done
  fi
  echo "${result_path}"
  return "${status_code}"
}


#######################################
# fhs::confdir()
# Return by default standard data directory location (/etc).
# Otherwise return the custom conf directory:
#   * -e or --executable parameter is used, the result_path will be relative to the provided executable.
#   * -l or --local is used the result_path will be the standard bin for non system application (/usr/local/etc).
#   * -u or --user-home is used the result_path will be relative to the selected home directory (${HOME}/.config).
# NOTE: Parameters --executable and --user are mutual exclusive
# Globals:
#   None
# Parameters:
#   -e|--executable <executable name> executable name use to determine the data directory
#   -l|--local                        Standard Hierarchy use by the system administrator when installing software locally.
#                                     It needs to be safe from being overwritten when the system software is updated.
#   -u|--user-home  [${HOME}]         Standard data directory for a user (default use current user home: $HOME)
# Returns:
#   Exit 1 if command fail otherwise return 0
# Examples:
#  fhs::bindir
#  fhs::bindir --local
#  fhs::bindir --user
#  fhs::bindir --executable bwa
#######################################
fhs::confdir(){
  local -i status_code="${EXIT_FAILURE}"
  local result_path
  local user_home
  local prefix
  local result_path
  if (( $# == 0 )); then
    result_path="$(realpath '/etc')"
    status_code="${EXIT_SUCCESS}"
  else
    while (( $# >= 1 )); do
      case "$1" in
        -e|--executable|-l|--local)
          prefix="$(fhs::prefix "$@")"
          result_path="$(realpath "${prefix}"'/etc')"
          status_code="${EXIT_SUCCESS}"
          # all parameters have been processed, remove them
          counter=$#
          while (( counter != 1 )); do shift; ((counter--)); done
          ;;
        -u|--user)
          if (( $# == 2 )); then
            user_home="$2"; shift
          else
            user_home="${HOME}"
          fi
          result_path="$(realpath "${user_home}"'/.config')"
          status_code="${EXIT_SUCCESS}"
          ;;
        -p|--prefix)
          if (( $# < 2 )); then
            output::error "${FUNCNAME[0]} allow only -e|--executable or -u|--user-home or -l|--local or -p|--prefix as parameters"
            output::error "\t└──> parameters used: $*"
            output::error "\t└──> Usage: ${FUNCNAME[0]} [-e|--executable <name> ] | [-u|--user-home [HOME] ] | [-l|--local ] | [-p|--prefix <path>]"
            status_code="${EXIT_FAILURE}"
          fi
          result_path="$(realpath "$2/etc")"
          status_code="$?"
          shift
          ;;
        *)
          output::error "${FUNCNAME[0]} allow only -e|--executable or -u|--user-home or -l|--local -p|--prefix as parameters"
          output::error "\t└──> parameters used: $*"
          output::error "\t└──> Usage: ${FUNCNAME[0]} [-e|--executable <name> ] | [-u|--user-home [HOME] ] | [-l|--local ] | [-p|--prefix <path>]"
          status_code="${EXIT_FAILURE}"
          ;;
      esac
      shift
    done
  fi
  echo "${result_path}"
  return "${status_code}"
}

fi
