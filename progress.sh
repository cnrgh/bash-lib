# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is PG (ProGress):
#  * `pg_`  for the public functions.
#  * `PG_`  for the public global variables or constants.
#  * `_pg_` for the private functions.
#  * `_PG_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_PG_SOURCED ]] || return 0
    _PG_SOURCED=1
fi

source "$(dirname "${BASH_SOURCE[0]}")/bool.sh"
source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"
source "$(dirname "${BASH_SOURCE[0]}")/date.sh"
source "$(dirname "${BASH_SOURCE[0]}")/maths.sh"

declare -A _PG_STARTTIME=()
declare -A _PG_LASTTIME=()
declare -A _PG_LASTMSG=()
declare _PG_LAST_PROGRESS_TITLE=

function _pg_get_bar_percent {

    declare -ri percent="$1"
    declare -ri length="${2:-32}"

    declare -ri n=$(( length * percent / 100 ))

    # Set blocks
    declare bar=""
    declare -i i
    for ((i = 0 ; i < n ; ++i)) ; do
        bar+='#'
    done

    # Set spaces
    for ((i = n ; i < length ; ++i)) ; do
        bar+=' '
    done

    # Set brackets
    bar="[$bar]"

    # Output
    echo -n "$bar"

    return 0
}

function pg_progress_bar {

    declare -r title="$1"
    declare -ri index="$2"
    declare -ri total="$3"
    declare -ri refresh_period="$4" # In milliseconds

    # Get times
    declare -ri now=$(date +%s%N) # Get number of nanoseconds
    declare -ri last="${_PG_LASTTIME["$tag"]}" # In milleseconds
    declare -ri start_="${_PG_STARTTIME["$tag"]}" # In milliseconds

    # Respect refresh period (i.e.: make a pause)
    [[ $last -gt 0 && $((now / 1000000 - last)) -lt $refresh_period ]] && \
        return 0

    # Store start and last
    _PG_LASTTIME["$title"]=$((now / 1000000))
    [[ $start_ -eq 0 ]] && _PG_STARTTIME["$title"]=$((now / 1000000))

    # Compute left milliseconds
    declare -i left=0 # In milliseconds
    [[ $index -eq 0 ]] || \
        left="$(( (total - index) * (now / 1000000 - start) / index ))"

    # Compute left time (human readable format)
    declare left_time
    left_time="$(dt_sec2time $((left / 1000)))"

    # Compute percentage done
    declare -ir percent="$(mt_percent "$index" "$total" 0)"

    # Get bar
    declare -r bar="$(_pg_get_bar_percent "$percent")"

    # Build full message
    declare -r msg="$title $bar $percent% ($left_time)"

    # Setup end line character
    declare end="\r"
    [[ $index -eq $total ]] && end="\n"

    # Erase previous line
    # shellcheck disable=SC2119
    pg_erase_progress_line

    # Print progress bar
    echo -ne "$msg$end" >&2

    # Remember last message
    _PG_LASTMSG["$title"]="$msg"
    _PG_LAST_PROGRESS_TITLE="$title"

    return 0
}

# shellcheck disable=SC2120
function pg_erase_progress_line {
    
    declare -r title="${1:-$_PG_LAST_PROGRESS_TITLE}"
    
    if [[ -n $title ]] ; then
        declare -r last_msg="${_PG_LASTMSG[$title]}"
        declare -ri length="${#last_msg}"

        # Print spaces
        declare spaces=""
        declare -i i
        for ((i = 0 ; i < length ; ++i)) ; do
            spaces+=" "
        done
        echo -ne "$spaces\r"
    fi
        
    return 0
}

function pg_progress {

    declare -r tag="$1"
    declare -r index="$2"
    declare -r total="$3"
    shift 3
    declare -r msg="$*"

    pg_progress_bar "$msg" "$index" "$total" 1000 || return 1

    return 0
}

function pg_elapsed_time {

    declare -r tag="$1"
    declare -r index="$2"
    shift 2
    declare -r msg="$*"

    declare -r last="${_PG_LASTTIME[$tag]}"
    declare start="${_PG_STARTTIME[$tag]}"

    if [[ -z $last || $((now - last)) -gt 0 ]] ; then
        _PG_LASTTIME[$tag]=$now
        if [[ -z $start ]] ; then
            start=$now
            _PG_STARTTIME[$tag]=$start
        fi
        declare -r elapsed=$(dt_sec2time $((now - start)))
        echo -ne "$msg $index ($elapsed)\r"
    fi

    return 0
}

true
