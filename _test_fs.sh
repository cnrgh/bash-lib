# vi: ft=bash
## @title File system assertions
##
## Assertions testing files and folders.

## Tests that a path does not exists.
##
## Checks that a path does not point to any real object.
##
## @arg path path A path to check.
## @arg msg str A message to print in case of failure.
## @status Returns 1 if the path points to a real object, 0 otherwise.
function tt_expect_no_path {

    local path="$1"
    local msg="$2"

    if [[ -e $path ]] ; then
        rt_print_call_stack >&2
        echo "\"$path\" exists. $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that a path points to a folder.
##
## Checks that a path points to a real folder.
##
## @arg path path A path to check.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the path points to a real folder, 1 otherwise.
function tt_expect_folder {

    local folder="$1"
    local msg="$2"

    if [[ ! -d $folder ]] ; then
        rt_print_call_stack >&2
        echo "\"$folder\" does not exist or is not a folder. $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that a path points to a regular file.
##
## Checks that a path points to a real regular file.
##
## @arg path path A path to check.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the path points to a real regular file, 1 otherwise.
function tt_expect_file {

    local file="$1"
    local msg="$2"

    if [[ ! -f $file ]] ; then
        rt_print_call_stack >&2
        echo "\"$file\" does not exist or is not a file. $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests that a path points to a symbolic link.
##
## Checks that a path points to a symbolic link .
##
## @arg path path A path to check.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the path points to a symbolic link, 1 otherwise.
function tt_expect_symlink {

    local symlink="$1"
    local pointed_path="$2"
    local msg="$3"

    if [[ ! -h $symlink ]] ; then
        rt_print_call_stack >&2
        echo "\"$symlink\" does not exist or is not a symbolic link. $msg" >&2
        return 1
    else
        local path
        path=$(realpath "$symlink") || \
            lg_fatal "Failed getting real path of \"$symlink\"."
        local real_pointed_path
        real_pointed_path=$(realpath "$pointed_path") || \
            lg_fatal "Failed getting real path of \"$pointed_path\"."
        if [[ $path != "$real_pointed_path" ]] ; then
            rt_print_call_stack >&2
            echo "Symbolic link \"$symlink\" does not point to \"$pointed_path\" but to \"$path\". $msg" >&2
            return 1
        fi
    fi

    echo -n .
    return 0
}

## Tests that a folder is writable.
##
## Checks that a path points to a real folder and that this folder is writable.
##
## @arg path path A path to a folder.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_folder_is_writable {

    local folder="$1"
    local msg="$2"
    local file="$folder/.___bashlib_test_test_file___"

    if ! touch "$file" ; then
        rt_print_call_stack >&2
        echo "Folder \"$folder\" is not writable. $msg" >&2
        return 1
    fi

    unlink "$file"
    echo -n .
    return 0
}

## Tests if a folder contains specific files.
##
## Checks that a path points to a real folder and that this folder contains
## other files than those filtered by the provided regex pattern.
##
## @arg path path A path to a folder.
## @arg files_regex str A regular expression that excludes files from the
## search.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_other_files_in_folder {

    local folder="$1"
    local files_regex="$2"
    local msg="$3"

    # List files in folder
    files="$(find "$folder" -maxdepth 1 -type f -print0 | \
        xargs -0 -n 1 basename | grep -Ev "$files_regex")" 
    if [[ -z $files ]] ; then
        rt_print_call_stack >&2
        echo "No files, not matching \"$files_regex\", were found inside" \
            "folder \"$folder\". $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a folder and its subfolders contains specific files.
##
## Checks that a path points to a real folder and that this folder and its
## subfolders contain
## other files than those filtered by the provided regex pattern.
##
## @arg path path A path to a folder.
## @arg files_regex str A regular expression that excludes files from the
## search.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_other_files_in_tree {

    local folder="$1"
    local files_regex="$2"
    local msg="$3"

    # List files in folder
    files=$(find "$folder" -type f -print0 | xargs -0 -n 1 basename | grep -Ev "$files_regex")
    if [[ -z $files ]] ; then
        rt_print_call_stack >&2
        echo "No files, not matching \"$files_regex\", were found inside" \
            "folder tree \"$folder\". $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a folder and its subfolders contains only specific files.
##
## Checks that a path points to a real folder and that this folder and its
## subfolders contain
## only files filtered by the provided regex pattern.
##
## @arg path path A path to a folder.
## @arg files_regex str A regular expression used to filter files.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_no_other_files_in_tree {

    local folder="$1"
    local files_regex="$2"
    local msg="$3"

    # List files in folder
    files_matching=$(find "$folder" -type f -print0 | xargs -0 -n 1 basename | \
        grep -E "$files_regex")
    files_not_matching=$(find "$folder" -type f -print0 | \
        xargs -0 -n 1 basename | grep -Ev "$files_regex")
    if [[ -z $files_matching ]] ; then
        rt_print_call_stack >&2
        echo "No files matching \"$files_regex\" were found inside folder" \
            "tree \"$folder\". $msg" >&2
        return 1
    fi
    if [[ -n $files_not_matching ]] ; then
        rt_print_call_stack >&2
        echo "Files, not matching \"$files_regex\", were found inside folder" \
            "\"$folder\": $files_not_matching. $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a folder contains only specific files.
##
## Checks that a path points to a real folder and that this folder
## contains
## only files filtered by the provided regex pattern.
##
## @arg path path A path to a folder.
## @arg files_regex str A regular expression used to filter files.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_no_other_files_in_folder {

    local folder="$1"
    local files_regex="$2"
    local msg="$3"

    # List files in folder
    files_matching="$(find "$folder" -maxdepth 1 -type f -print0 | xargs -0 -n 1 basename | grep -E "$files_regex")" 
    files_not_matching="$(find "$folder" -maxdepth 1 -type f -print0 | xargs -0 -n 1 basename | grep -Ev "$files_regex")" 
    if [[ -z $files_matching ]] ; then
        rt_print_call_stack >&2
        echo "No files matching \"$files_regex\" were found inside folder" \
            "\"$folder\". $msg" >&2
        return 1
    fi
    if [[ -n $files_not_matching ]] ; then
        rt_print_call_stack >&2
        echo "Files, not matching \"$files_regex\", were found inside folder" \
            "\"$folder\". $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a folder contains at least one specific files.
##
## Checks that a path points to a real folder and that this folder
## contains
## at least one file filtered by the provided regex pattern.
##
## @arg path path A path to a folder.
## @arg files_regex str A regular expression used to filter files.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_files_in_tree {

    local folder="$1"
    local files_regex="$2"
    local msg="$3"

    # List files in folder
    files=$(find "$folder" -type f -print0 | xargs -0 -n 1 basename | grep -E "$files_regex")
    if [[ -z $files ]] ; then
        rt_print_call_stack >&2
        echo "No files matching \"$files_regex\" were found inside folder" \
            "tree \"$folder\". $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a folder contains at least one specific files.
##
## Checks that a path points to a real folder and that this folder
## contains
## at least one file filtered by the provided regex pattern.
##
## @arg path path A path to a folder.
## @arg files_regex str A regular expression used to filter files.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_files_in_folder {

    local folder="$1"
    local files_regex="$2"
    local msg="$3"

    # List files in folder
    files=$(find "$folder" -maxdepth 1 -type f -print0 | \
        xargs -0 -n 1 basename | grep -E "$files_regex")
    if [[ -z $files ]] ; then
        rt_print_call_stack >&2
        echo "No files matching \"$files_regex\" were found inside folder"\
            "\"$folder\". $msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

function _tt_expect_n_paths_in_folder {

    declare -r n="$1"
    declare -ri mode="$2" # 0:all, 1:files, 2:dirs
    declare -r folder="$3"
    declare -r msg="$4"

    # Set find flags
    declare -a flags=("-mindepth" 1 "-maxdepth" 1)
    [[ $mode -eq 1 ]] && flags+=(-type f)
    [[ $mode -eq 2 ]] && flags+=(-type d)

    # List files in folder
    n_files=$(find "$folder" "${flags[@]}" | wc -l)
    if [[ $n -ne $n_files ]] ; then
        rt_print_call_stack >&2
        echo "Found $n_files file(s) in folder \"$folder\" instead of ${n}." \
            "$msg" >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a folder contains exactly n paths.
##
## Tests if the provided path points to a folder, counts the number of 
## files or folders inside this folder and returns 0 if this number is `n`.
## Returns 1 otherwise.
##
## @arg n int The expected number of files and folders.
## @arg folder path The path to a valid folder.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_n_paths_in_folder {
    _tt_expect_n_paths_in_folder "$1" 0 "$2" "$3"
}

## Tests if a folder contains exactly n files.
##
## Tests if the provided path points to a folder, counts the number of regular
## files inside this folder and returns 0 if this number is `n`. Returns 1
## otherwise.
##
## @arg n int The expected number of files.
## @arg folder path The path to a valid folder.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_n_files_in_folder {
    _tt_expect_n_paths_in_folder "$1" 1 "$2" "$3"
}

## Tests if a folder contains exactly n folders.
##
## Tests if the provided path points to a folder, counts the number of
## folders inside this folder and returns 0 if this number is `n`. Returns 1
## otherwise.
##
## @arg n int The expected number of folders.
## @arg folder path The path to a valid folder.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_n_folders_in_folder {
    _tt_expect_n_paths_in_folder "$1" 2 "$2" "$3"
}

## Tests if two folders contain the same hierarchy of files with the same
## content exactly.
##
## Uses `diff` to compare the two folders and check that they contain the same
## hierarchy of files and folders.
##
## @arg folder1 path The path to a valid folder.
## @arg folder2 path The path to another valid folder.
## @arg msg str A message to print in case of failure.
## @status Returns 0 if the folder is writable, 1 otherwise.
function tt_expect_same_folders {

    local folder1="$1"
    local folder2="$2"

    tt_expect_folder "$folder1" || return 2
    tt_expect_folder "$folder2" || return 3

    if ! diff -r -q "$folder1" "$folder2" >/dev/null ; then
        rt_print_call_stack >&2
        echo "Folders \"$folder1\" and \"$folder2\" differ." >&2
        return 1
    fi

    echo -n .
    return 0
}
