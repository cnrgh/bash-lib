ARG DOCKER_BASH_VERSION=5.2
FROM registry.gitlab.com/cnrgh/bash-lib:${DOCKER_BASH_VERSION}

# Set local variables
ARG dir=/project

# Copy files
COPY Makefile runtests gendoc doc.css embedlibs testthat *.sh $dir/
COPY tests/ $dir/tests/
COPY src/ $dir/src/

# Test
WORKDIR $dir
ENTRYPOINT ["make"]
CMD ["check", "test", "doc"]
