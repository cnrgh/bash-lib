# vi: ft=bash
# shellcheck disable=SC1091
# The name/prefix of this module is AP (Arguments Parse):
#  * `ap_`  for the public functions.
#  * `AP_`  for the public global variables or constants.
#  * `_ap_` for the private functions.
#  * `_AP_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_AP_SOURCED ]] || return 0
    _AP_SOURCED=1
fi

_AP_INDENT="  "
_FORCE_DOUBLE_DASH=false

source "$(dirname "${BASH_SOURCE[0]}")/array.sh"
source "$(dirname "${BASH_SOURCE[0]}")/bool.sh"
source "$(dirname "${BASH_SOURCE[0]}")/logging.sh"
source "$(dirname "${BASH_SOURCE[0]}")/str.sh"
source "$(dirname "${BASH_SOURCE[0]}")/deprecate.sh"

#maj=4
#min=4
#[[ ${BASH_VERSINFO[0]} -gt $maj || ( ${BASH_VERSINFO[0]} -ge $maj \
#   && ${BASH_VERSINFO[1]} -ge $min ) ]] || \
#   lg_fatal "argparse library requests bash version $maj.$min or above."

# Declare associative arrays at global scope because of Bash 4.2 bug that
# prevents from declaring them from inside a function.
declare -gA _AP_OPTS
declare -gA _AP_ALIAS_TO_NAME
declare -gA _AP_OPT_ALIASES
declare -gA _AP_OPT_VAR
declare -gA _AP_OPT_FCT
declare -gA _AP_OPT_DESC
declare -gA _AP_OPT_TYPE
declare -gA _AP_OPT_DEFAULT
declare -gA _AP_OPT_VALUE
declare -gA _AP_OPT_NMIN # Minimum number of times the option has to set.
declare -gA _AP_OPT_NMAX # Maximum number of times the option can be set.
declare -gA _AP_OPT_NTIMES # Number of times the option has been
                           # actually set.
declare -gA _AP_OPT_ENV_VAR
declare -g  _AP_SCRIPT_NAME
declare -g  _AP_SCRIPT_VERSION
declare -g  _AP_SCRIPT_SHORT_DESC
declare -g  _AP_SCRIPT_LONG_DESC
declare -g  _AP_USE_ENV_VAR
declare -gu _AP_ENV_VAR_PREFIX
declare -ga _AP_POS_DESC
declare -ga _AP_POS_VAR
declare -ga _AP_POS_NVALUES
declare -ga _AP_POS_TYPE
declare -ga _AP_ADDITIONAL_SECTIONS

function ap_reset_args {

    _AP_SCRIPT_NAME=$(basename "$0")
    _AP_SCRIPT_VERSION=$VERSION
    _AP_SCRIPT_SHORT_DESC=
    _AP_SCRIPT_LONG_DESC=
    _AP_EXAMPLES=()
    _AP_AUTHORS=()
    _AP_ADDITIONAL_SECTIONS=()
    _AP_USE_ENV_VAR=
    _AP_ENV_VAR_PREFIX=
    _AP_POS_DESC=()
    _AP_POS_VAR=()
    _AP_POS_NVALUES=()
    _AP_POS_TYPE=()
    _AP_OPTS=()
    _AP_ALIAS_TO_NAME=()
    _AP_OPT_ALIASES=()
    _AP_OPT_VAR=()
    _AP_OPT_FCT=()
    _AP_OPT_DESC=()
    _AP_OPT_TYPE=()
    _AP_OPT_DEFAULT=()
    _AP_OPT_VALUE=()
    _AP_OPT_NMIN=()
    _AP_OPT_NMAX=()
    _AP_OPT_NTIMES=()
    _AP_OPT_ENV_VAR=()

    return 0
}

function _ap_reset_counters {

    # Reset counter of option usage
    for k in "${!_AP_OPT_NTIMES[@]}" ; do
        _AP_OPT_NTIMES[$k]=0
    done

    # Reset variables for optional arguments
    for k in "${!_AP_OPT_VAR[@]}" ; do
        local var="${_AP_OPT_VAR[$k]}"
        if [[ -n $var ]] ; then
            local def="${_AP_OPT_DEFAULT[$k]}"
            local typ="${_AP_OPT_TYPE[$k]}"
            case $typ in
                *arr) declare -ga "$var=($def)" ;;
                *) declare -g "$var=$def"
            esac
        fi
    done

    # Reset variables for positional arguments
    for k in "${!_AP_POS_VAR[@]}" ; do
        local var="${_AP_POS_VAR[$k]}"
        local nvals="${_AP_POS_NVALUES[$k]}"
        if [[ $nvals -gt 1 ]] ; then
            declare -ga "$var=()"
        else
            declare -g "$var="
        fi
    done

    return 0
}

function ap_set_script_name {
    _AP_SCRIPT_NAME="$*"
    return 0
}

function ap_set_short_description {
    _AP_SCRIPT_SHORT_DESC="$*"
    return 0
}

function ap_set_long_description {
    _AP_SCRIPT_LONG_DESC="$*"
    return 0
}

function ap_add_example {
    declare -r text="$1"
    declare -r code="$2"
    _AP_EXAMPLES+=("$text" "$code")
    return 0
}

function ap_add_author {
    declare -r author="$1"
    _AP_AUTHORS+=("$author")
    return 0
}

function ap_add_bottom_section {
    dp_deprecated "ap_add_section"
    ap_add_section "$@"
}

function ap_add_section {
    local -r title="$1"
    shift
    _AP_ADDITIONAL_SECTIONS+=("$title" "$*")
    return 0
}

function ap_set_exit_codes {
    local text
    while [[ -n "$1" ]] ; do
        if [[ -z $text ]] ; then
            text=$(printf "%3s %s" "$1" "$2")
        else
            text="$text"$(printf "\n%3s %s" "$1" "$2")
        fi
        shift 2
    done
    ap_add_section "EXIT CODES" "$text"
    return 0
}

function ap_use_env_var {
    # Enable use of environment variables to set arguments.

    local -ru prefix="$1"

    if [[ -n $prefix ]] ; then
        _AP_ENV_VAR_PREFIX="$prefix"
    else
        _AP_ENV_VAR_PREFIX="${_AP_SCRIPT_NAME//[^a-zA-Z]/_}"
    fi

    [[ $_AP_ENV_VAR_PREFIX =~ [^A-Z_] ]] && lg_fatal "Prefix for environment" \
        "variables must only contain upper ascii letters or underscores."
    
    _AP_USE_ENV_VAR=1

    return 0
}

function ap_force_double_dash {
    _FORCE_DOUBLE_DASH=true
    return 0
}

function _ap_get_env_var_name {

    local var="$1"

    declare -u envvar="${_AP_ENV_VAR_PREFIX}_$var"
    echo "$envvar"
    return 0
}

function _ap_get_longest_name {

    local names="$1"
    local longest=

    local oldifs="$IFS"
    IFS=,
    for n in $names ; do
        [[ ${#n} -gt ${#longest} ]] && longest=$n
    done
    IFS="$oldifs"

    echo "$longest"
    return 0
}

function _ap_define_name_and_aliases {

    local names="$1"

    # Parse main name and declare aliases
    local i=0
    local name=
    local oldifs="$IFS"
    IFS=,
    for n in $names ; do
        if [[ $i -eq 0 ]] ; then
            name=$n
            _AP_OPTS["$n"]=1
        else
            _AP_ALIAS_TO_NAME["$n"]=$name
            _AP_OPT_ALIASES["$name"]="${_AP_OPT_ALIASES[$name]} $n"
        fi
        ((++i))
    done
    IFS="$oldifs"
    [[ -n $name ]] || lg_fatal "No name definition for option (received"\
        "names=\"$names\")."

    return 0
}

function _ap_define_var_opt {

    local names="$1"
    local var="$2"
    local type="$3"
    local desc="$4"
    local default="$5"
    local value="$6" # For flag type, value to set if flag is enabled.
                     # For enum, the comma separated list of allowed values.
    local nmin="$7"  # Minimum of times to set the opt
    local nmax="$8"  # Maximum of times to set the opt
    local name=${names%%,*}

    # Default value
    [[ -z $nmin ]] && nmin=0
    [[ -z $nmax ]] && nmax=1

    lg_debug 1 "Defining option type=$type, var=$var, names=$names."
    # Use env var
    if [[ -n $_AP_USE_ENV_VAR ]] ; then
        local envvar
        envvar=$(_ap_get_env_var_name "$var") || \
            lg_fatal "Cannot get env var \"$var\"."
        _AP_OPT_ENV_VAR["$name"]="$envvar"
        [[ ! ${!envvar} ]] || default=${!envvar}
    fi

    if [[ $type == *arr ]] ; then
        declare -ga "$var=($default)"
    elif [[ -n $var ]] ; then
        declare -g "$var=$default"
    fi
    _ap_define_name_and_aliases "$names"
    _AP_OPT_VAR["$name"]="$var"
    _AP_OPT_DESC["$name"]="$desc"
    _AP_OPT_TYPE["$name"]="$type"
    _AP_OPT_DEFAULT["$name"]="$default"
    _AP_OPT_VALUE["$name"]="$value"
    _AP_OPT_NMIN["$name"]=$nmin
    _AP_OPT_NMAX["$name"]=$nmax
    _AP_OPT_NTIMES["$name"]=0
}

function ap_add_opt_int {

    local names="$1"
    local var="$2"
    local default="$3"
    shift 3
    local desc="$*"

    _ap_define_var_opt "$names" "$var" int "$desc" "$default"
}

function ap_add_opt_float {

    local names="$1"
    local var="$2"
    local default="$3"
    shift 3
    local desc="$*"

    _ap_define_var_opt "$names" "$var" float "$desc" "$default"
}

function ap_add_opt_enum {
    # Define enumeration option.

    local names="$1"
    local var="$2"
    local values="$3" # List of allowed values. The first one is the default.
    shift 3
    local desc="$*"
    local default=${values%%,*}

    _ap_define_var_opt "$names" "$var" enum "$desc" "$default" "$values"
}

function ap_add_opt_str {

    local names="$1"
    local var="$2"
    local default="$3"
    shift 3
    local desc="$*"

    _ap_define_var_opt "$names" "$var" str "$desc" "$default"
}

function ap_add_opt_flag {
    # Flag (empty by default and non-empty if set)

    local names="$1"
    local var="$2"
    shift 2
    local desc="$*"

    _ap_define_var_opt "$names" "$var" flag "$desc" "" 1
}

function ap_add_opt_bflag {
    # Flag (false by default and true if set)

    local names="$1"
    local var="$2"
    shift 2
    local desc="$*"

    _ap_define_var_opt "$names" "$var" flag "$desc" "false" "true"
}

function ap_add_opt_rflag {
    # Reverse flag (non-empty by default and empty if set)

    local names="$1"
    local var="$2"
    shift 2
    local desc="$*"

    _ap_define_var_opt "$names" "$var" rflag "$desc" 1 ""
}

function ap_add_opt_oflags {
    # Define opposite flags (--foo and --no-foo)

    local name="$1"
    local var="$2"
    local default="$3"
    local desc="$4"
    local rdesc="$5"

    # Check the name
    [[ $name =~ , ]] && lg_fatal "In order to define opposite flags, you need"\
        "to provide only one name. You provided \"$name\"."

    _ap_define_var_opt "$name" "$var" flag "$desc" "$default" 1
    _ap_define_var_opt "no-$name" "$var" rflag "$rdesc" "$default" ""
}

function ap_add_opt_sflag {
    # Define a string flag: a flag that sets a specific string into a
    # variable

    local names="$1"
    local var="$2"
    local value="$3"
    shift 3
    local desc="$*"

    [[ -n $value ]] || value=$(_ap_get_longest_name "$names")
    _ap_define_var_opt "$names" "$var" flag "$desc" "" "$value"
}

function ap_add_opt_sflag_arr {

    local names="$1"
    local var="$2"
    local value="$3"
    shift 3
    local desc="$*"

    [[ -n $value ]] || value=$(_ap_get_longest_name "$names")
    _ap_define_var_opt "$names" "$var" flagarr "$desc" "" "$value"
}

# Define an option flag that is a shortcut for other flags.
function ap_add_opt_shortcut_flag {

    local names="$1"
    local value="$2"
    shift 2
    local desc="$*"

    _ap_define_var_opt "$names" "" shortcutflag "$desc" "" "$value"

    return 0
}

function ap_add_opt_inc {

    local names="$1"
    local var="$2"
    shift 2
    local desc="$*"

    _ap_define_var_opt "$names" "$var" inc "$desc" 0 "" 0 0
}

function ap_add_opt_fct {

    local names="$1"
    local fct="$2"
    shift 2
    local desc="$*"
    local type=fct
    lg_debug 1 "Defining option type=$type, fct=$fct, names=$names."

    local name=${names%%,*}
    _ap_define_name_and_aliases "$names"
    _AP_OPT_FCT["$name"]="$fct"
    _AP_OPT_DESC["$name"]="$desc"
    _AP_OPT_TYPE["$name"]="$type"
}

function _ap_get_full_opt_flag {

    local opt="$1"

    echo -n "-"
    [[ ${#opt} -gt 1 ]] && echo -n "-"
    echo -n "$opt"
}

function _ap_print_allowed_values {

    local allowed_values="$1"

    if [[ -n $allowed_values ]] ; then
        local -a arr
        read -ra arr <<<"$(st_split , "${allowed_values[@]}")"
        read -ra arr <<<"($(ar_sort "${arr[@]}"))"
        allowed_values="$(st_join ', ' "${arr[@]}")"
        echo " Allowed values are: $allowed_values."
    fi

    return 0
}

function _ap_print_opt_flags {

    local opt="$1"

    # Write main flag
    # Use printf to avoid interpretation of '-e' flag or other flags by `echo`.
    printf '%s' "$(_ap_get_full_opt_flag "$opt")"

    # Write aliases
    for als in ${_AP_OPT_ALIASES["$opt"]} ; do
        echo -n ", $(_ap_get_full_opt_flag "$als")"
    done

    return 0
}

function _ap_print_type {

    local type="$1"

    case $type in
        str)   echo -n " <string>" ;;
        int)   echo -n " <integer>" ;;
        float) echo -n " <float>" ;;
        enum)  echo -n " <choice>" ;;
    esac
}

function _ap_print_arg_desc {

    local desc="$1"

    desc=${desc//  \+/ }
    fold -s -w $((80-${#_AP_INDENT}*2)) <<<"$desc" | \
        sed "s/^/$_AP_INDENT$_AP_INDENT/"
}

function _ap_print_pos_args {

    [[ ${#_AP_POS_VAR[@]} -eq 0 ]] && return 0

    echo
    echo "Positional arguments:"
    for ((i = 0 ; i < ${#_AP_POS_VAR[@]} ; ++i)) ; do

        echo # Blank line
        echo -n "$_AP_INDENT${_AP_POS_VAR[$i]}"
        local type_pos="${_AP_POS_TYPE[$i]}"
        _ap_print_type "$type_pos"
        local desc="${_AP_POS_DESC[$i]}"
        [[ $type_pos == enum ]] && \
            desc+=$(_ap_print_allowed_values "${_AP_POS_VALUES[$i]}")
        echo

        _ap_print_arg_desc "$desc"
    done
}

function _ap_print_opt_args {

    [[ ${#_AP_OPTS[@]} -eq 0 ]] && return 0

    echo
    echo "Optional arguments:"
    options=$(tr " " "\n" <<<"${!_AP_OPTS[@]}" | sort | tr "\n" " ")
    for opt in $options ; do

        echo # Blank line
        echo -n "$_AP_INDENT"
        _ap_print_opt_flags "$opt"
        type_var=${_AP_OPT_TYPE[$opt]}
        _ap_print_type "$type_var"
        echo

        # Description
        desc=${_AP_OPT_DESC[$opt]}
        [[ $type_var == enum ]] && desc+=$(_ap_print_allowed_values \
            "${_AP_OPT_VALUE[$opt]}")
        [[ $type_var == shortcutflag ]] && \
            desc+="Will be replaced with ${_AP_OPT_VALUE[$opt]}."
        default="${_AP_OPT_DEFAULT[$opt]}"
        case $type_var in
            flag)
                if bo_is_true "$default" ; then
                    desc+=" Enabled by default."
                else
                    desc+=" Disabled by default."
                    fi
                ;;
            rflag)
                if bo_is_true "$default" ; then
                    desc+=" Disabled by default."
                else
                    desc+=" Enabled by default."
                    fi
                ;;
            *) [[ -z $default ]] || desc+=" Default value is \"$default\"." ;;
        esac
        envvar="${_AP_OPT_ENV_VAR[$opt]}"
        [[ -z $envvar ]] || \
            desc+=" Can be set with environment variable $envvar."
        _ap_print_arg_desc "$desc"
    done
}

function _ap_print_usage {

    # Blank line
    echo

    # Title and script name
    echo -n "Usage: $_AP_SCRIPT_NAME" | fold -s -w 80

    # Options
    [[ ${#_AP_OPTS[@]} -gt 0 ]] && echo -n " [options]"

    # Position arguments
    if [[ ${#_AP_POS_VAR[@]} -gt 0 ]] ; then
        bo_is_true "$_FORCE_DOUBLE_DASH" && echo -n "[-- "
        for pos in "${_AP_POS_VAR[@]}" ; do
            echo -n " $pos"
        done
        bo_is_true "$_FORCE_DOUBLE_DASH" && echo -n "]"
    fi

    # New line
    echo

    return 0
}

function _ap_print_additional_sections {

    declare -r size="${#_AP_ADDITIONAL_SECTIONS[@]}"
    for ((i=0 ; i < size ; i+=2)) ; do
        declare -u title="${_AP_ADDITIONAL_SECTIONS[$i]}"
        declare text="${_AP_ADDITIONAL_SECTIONS[$((i+1))]}"
        echo
        echo "$title"
        text=${text//  \+/ }
        fold -s -w $((80-${#_AP_INDENT}*2)) <<<"$text" | \
            sed "s/^/$_AP_INDENT$_AP_INDENT/"
    done

    return 0
}

function _ap_print_authors {

    declare -r size="${#_AP_AUTHORS[@]}"
    [[ $size -eq 0 ]] && return 0

    echo
    echo "AUTHORS"
    for author in "${_AP_AUTHORS[@]}" ; do
        author=${author//  \+/ }
        fold -s -w $((80-${#_AP_INDENT})) <<<"$author" | \
            sed "s/^/$_AP_INDENT/"
    done

    return 0
}

function _ap_print_examples {

    declare -r size="${#_AP_EXAMPLES[@]}"
    [[ $size -eq 0 ]] && return 0

    echo
    echo "EXAMPLES"
    for ((i=0 ; i < size ; i+=2)) ; do
        declare text="${_AP_EXAMPLES[$i]}"
        declare code="${_AP_EXAMPLES[$((i+1))]}"
        echo
        text=${text//  \+/ }
        fold -s -w $((80-${#_AP_INDENT})) <<<"$text:" | \
            sed "s/^/$_AP_INDENT/"
        code=${code//  \+/ }
        fold -s -w $((80-${#_AP_INDENT}*2)) <<<"$code" | \
            sed "s/^/$_AP_INDENT$_AP_INDENT/"
    done

    return 0
}

function ap_print_version {
    if [[ -n $_AP_SCRIPT_VERSION ]] ; then
        echo "$_AP_SCRIPT_VERSION"
    else
        echo "Unknown version."
    fi
    exit 0
}

function ap_print_help {

    # First line
    local line="$_AP_SCRIPT_NAME"
    [[ -z $_AP_SCRIPT_VERSION ]] || line+=", version $_AP_SCRIPT_VERSION"
    [[ -z $_AP_SCRIPT_SHORT_DESC ]] || line+=", $_AP_SCRIPT_SHORT_DESC"
    [[ $line == *. ]] || line+="."
    echo "$line" | fold -s -w 80

    # Long description
    if [[ -n $_AP_SCRIPT_LONG_DESC ]] ; then
        echo
        echo "$_AP_SCRIPT_LONG_DESC" | fold -s -w 80
    fi

    # Usage
    _ap_print_usage

    # Positional arguments
    _ap_print_pos_args

    # Optional arguments
    _ap_print_opt_args

    # Additional sections
    _ap_print_additional_sections

    # Examples
    _ap_print_examples

    # Authors
    _ap_print_authors

    echo
    exit 0
}

function ap_add_verbose_opt {
    ap_add_opt_fct "v,verbose" lg_inc_level "Verbosity level."
}

function ap_add_debug_opt {
    ap_add_opt_inc "g,debug" _LG_DEBUG_LEVEL "Debug mode."
}

function ap_add_quiet_opt {
    ap_add_opt_fct "q,quiet" lg_set_quiet "Turn off all messages from logging" \
        "library, except FATAL, ERROR and WARNING."
}

function ap_add_log_opt {
    ap_add_opt_flag "l,log" LG_LOG_TO_FILE "Enable logging to file."
}

function ap_add_log_file_opt {
    ap_add_opt_str "log-file" LG_FILE "$HOME/.$_AP_SCRIPT_NAME.log" \
        "Set the file to use for logging."
}

function ap_add_log_file_size_opt {
    ap_add_opt_int "log-file-size" LG_FILE_MAX_SIZE 10000 \
        "Set the maximum size of the log file."
}

function ap_add_lock_file_opt {
    ap_add_opt_str "lock-file" OS_LOCK_FILE "$HOME/.$_AP_SCRIPT_NAME.lock" \
        "Set the file to use for locking application."
}

function ap_add_dryrun_opt {
    ap_add_opt_flag "dryrun,dry-run" OS_DRYRUN "Display what would be run,"\
        "but do not execute anything."
}

function ap_add_help_opt {
    ap_add_opt_fct "h,help" ap_print_help "Print a help message and exit."
}

function ap_add_version_opt {
    ap_add_opt_fct "version" ap_print_version "Print the version and exit."
}

function _ap_add_pos {

    local name="$1"
    local desc="$2"
    local npos="$3"
    local type="$4"
    local optional="$5"
    local values="$6"

    # TODO Check characters in name
    # TODO Check that npos >= 0
    # TODO Check type (fct, int, ...)
    [[ -z $npos ]] && npos=1
    [[ -z $type ]] && type=str
    [[ -z $optional ]] && optional=0

    # Set default value
    if [[ $npos -gt 1 ]] ; then
        declare -ga "$name=()"
    else
        declare -g "$name="
    fi

    # Set positional info
    _AP_POS_DESC+=("$desc")
    _AP_POS_VAR+=("$name")
    _AP_POS_NVALUES+=("$npos")
    _AP_POS_TYPE+=("$type")
    _AP_POS_OPTIONAL+=("$optional")
    _AP_POS_VALUES+=("$values")

    return 0
}

function ap_add_pos_one {

    local var=$1
    shift
    local desc="$*"

    _ap_add_pos "$var" "$desc" 1 str
}

function ap_add_pos_one_enum {

    local var="$1"
    local values="$2" # Comma separated values.
    shift 2
    local desc="$*"

    _ap_add_pos "$var" "$desc" 1 enum 0 "$values"
}

function ap_add_pos_one_optional {

    local var=$1
    shift
    local desc="$*"

    _ap_add_pos "$var" "$desc" 1 str 1
}

function ap_add_pos_n {

    local var=$1
    local n=$2
    shift 2
    local desc="$*"

    _ap_add_pos "$var" "$desc" "$n" str
}

function ap_add_pos_max {

    local var=$1
    shift
    local desc="$*"

    _ap_add_pos "$var" "$desc" 0 str
}

function ap_add_pos_max_enum {

    local var=$1
    local values="$2"
    shift 2
    local desc="$*"

    _ap_add_pos "$var" "$desc" 0 enum 0 "$values"
}

function _ap_check_enum_value {

    local v="$1"
    local values="$2"

    lg_debug 1 "Check if \"$v\" is in \"$values\"."
    [[ ",$values," == *",$v,"* ]]
    local status=$?
    lg_debug 1 "status=$status."

    return $status
}

function _ap_check_value {
    local opt="$1"
    local t="$2"
    local v="$3"
    local values="$4"
    [[ $t != int || $v =~ ^-?[0-9]+$ ]] || lg_fatal "Option" \
        "$(_ap_get_full_opt_flag "$opt")" \
        " expects an integer value, not \"$v\"."
    [[ $t != float || $v =~ ^-?[0-9.]+$ ]] || lg_fatal "Option" \
        "$(_ap_get_full_opt_flag "$opt")" \
        " expects a float value, not \"$v\"."
    [[ $t != enum ]] || _ap_check_enum_value "$v" "$values" || \
        lg_fatal "Value \"$v\" is not allowed for option" \
        "$(_ap_get_full_opt_flag "$opt")."
    return 0
}

function _ap_get_opt_name {
    local origopt="$1"

    # Get option name
    local opt=${origopt#--}
    opt=${opt#-}
    
    echo -n "$opt"
    return 0
}

function _ap_get_opt_real_name {
    local opt="$1"

    # Convert alias to real name
    [[ -z ${_AP_ALIAS_TO_NAME[$opt]} ]] || opt=${_AP_ALIAS_TO_NAME[$opt]}
    echo -n "$opt"

    return 0
}

function _ap_opt_is_defined {
    local opt="$1"
    [[ -n ${_AP_OPTS[$opt]} || -n ${_AP_ALIAS_TO_NAME[$opt]} ]]
}

function _ap_get_opt_type {
    local opt="$1"
    echo -n "${_AP_OPT_TYPE[$opt]}"
    return 0
}

function _ap_process_opt {

    declare -i nshift=0
    declare opt="$1"
    declare var_type="$2"
    shift 2

    # Function
    if [[ $var_type == fct ]] ; then
        ${_AP_OPT_FCT[$opt]} # Call function
        
    # Incrementing integer
    elif [[ $var_type == inc ]] ; then
        local var_name=${_AP_OPT_VAR[$opt]}
        [[ -n $var_name ]] || declare -g "$var_name=0"
        eval "((++$var_name))"

    # Array
    elif [[ $var_type == *arr ]] ; then
        local v="$1"
        nshift=1
        [[ $var_type == flagarr ]] && v="${_AP_OPT_VALUE[$opt]}" && nshift=0
        local base_type=${var_type%arr}
        _ap_check_value "$opt" "$base_type" "$v" "${_AP_OPT_VALUE[$opt]}"
        local var_name=${_AP_OPT_VAR[$opt]}
        eval "$var_name+=(\"$v\")"

    # Enum, Integer & String
    elif [[ " enum flag rflag int float str " == *" $var_type "* ]] ; then
        local v="$1"
        nshift=1
        [[ " flag rflag " == *" $var_type "* ]] && v="${_AP_OPT_VALUE[$opt]}"\
            && nshift=0
        _ap_check_value "$opt" "$var_type" "$v" "${_AP_OPT_VALUE[$opt]}"
        local var_name=${_AP_OPT_VAR[$opt]}
        declare -g "$var_name=$v"

    else
        lg_fatal "Unknown type $var_type for argument "\
            "$(_ap_get_full_opt_flag "$opt")."

    fi

    return $nshift
}

function _ap_opt_inc_use_count {
    # Increment counter of use for this option
    declare -r opt="$1"
    [[ ${_AP_OPT_NMAX[$opt]} -eq 0 || \
        ${_AP_OPT_NTIMES[$opt]} -le ${_AP_OPT_NMAX[$opt]} ]] || \
        lg_fatal "You have used too many times the option" \
            "\"$(_ap_get_full_opt_flag "$opt")\"."
    return 0
}
function _ap_print_debug_msgs {

    local args="$1"

    lg_debug 1 "Arguments: $args"

    # Print variables of optional arguments
    for var in $(tr " " "\n" <<< "${_AP_OPT_VAR[@]}" | sort | uniq) ; do
        lg_debug 1 "$var=${!var}"
    done

    # Print variables of positional arguments
    for var in $(tr " " "\n" <<< "${_AP_POS_VAR[@]}" | sort | uniq) ; do
        eval "lg_debug 1 \"$var=\${${var}[@]}\""
    done

    return 0
}

function _ap_read_pos_args {

    for ((i = 0 ; i < ${#_AP_POS_VAR[@]} ; ++i)) ; do

        local var=${_AP_POS_VAR[$i]}
        local nvals=${_AP_POS_NVALUES[$i]}
        local type=${_AP_POS_TYPE[$i]}
        local optional=${_AP_POS_OPTIONAL[$i]}
        local values=${_AP_POS_VALUES[$i]}

        # Read values
        if [[ $nvals -eq 1 ]] ; then
            declare -g "$var="
        else
            declare -ga "$var=()"
        fi
        local j=0
        lg_debug 1 "var=$var"
        lg_debug 1 "type=$type"
        lg_debug 1 "nvals=$nvals"
        while [[ $1 != '' && ( $nvals -eq 0 || $j -lt $nvals ) ]] ; do

            lg_debug 1 "j=$j"
            # Check enum value
            if [[ $type == enum ]] \
                && ! _ap_check_enum_value "$1" "$values" ; then
                [[ $nvals == 0 ]] && break
                lg_fatal "Value \"$1\" is not allowed for positional argument"\
                "$var."
            fi

            # Set value
            if [[ $nvals -eq 1 ]] ; then
                eval "$var=\"$1\""
            else
                eval "$var+=(\"$1\")"
            fi

            # Pass to next value
            shift
            ((++j))
        done

        # Check number of read values
        if [[ $optional -eq 0 && $nvals -gt 0 && $j -lt $nvals ]] ; then
            if [[ $nvals -eq 1 ]] ; then
                lg_fatal "You must set a value for positional argument $var."
            else
                lg_fatal "You must set $nvals values for positional argument"\
                    "$var."
            fi
        fi
    done
    [[ -z "$*" ]] || lg_fatal "Forbidden remaining arguments: $*."
}

function ap_read_args {

    local args="$*" # Save arguments for debugging purpose
    _ap_reset_counters

    # Read optional arguments
    declare opt_name
    declare -i nshift
    while true ; do
        lg_debug 1 "Left options: $*"
        case "$1" in

            # Try to process this option
            -?|--*)
                opt_name=$(_ap_get_opt_name "$1")
                if _ap_opt_is_defined "$opt_name" ; then
                    opt_name=$(_ap_get_opt_real_name "$opt_name")
                    opt_type=$(_ap_get_opt_type "$opt_name")
                    if [[ $opt_type == shortcutflag ]] ; then
                        nshift=1
                        # shellcheck disable=SC2086
                        set -- $1 ${_AP_OPT_VALUE[$opt_name]} "${@:2}"
                    else
                        _ap_process_opt "$opt_name" "$opt_type" "${@:2}"
                        nshift=$? # Status is the number of shifts to apply
                        nshift+=1
                    fi
                    _ap_opt_inc_use_count "$opt_name"
                    [[ $nshift -gt 0 ]] && shift $nshift && continue
                fi
                ;;& # Go on to next case

            # Quit loop for reading remaining arguments as positional arguments
            --)
                shift
                break
                ;;

            # Handle unknown option
            -|--*|-?) lg_fatal "Illegal option $1." ;;

            # Split short options joined together
            -[^-]*)
                local opts="$1"
                # shellcheck disable=SC2001
                opts="$(sed 's/\([^-]\)/ -\1/g' <<<"${opts/#-/}")"
                local -a split_opt
                read -ra split_opt <<<"$opts"
                set -- "${split_opt[@]}" "${@:2}"
                ;;

            # Remaining arguments are positional arguments
            *)
                [[ -n "$1" ]] && bo_is_true "$_FORCE_DOUBLE_DASH" && \
                    lg_fatal "Forbidden argument \"$1\"."
                break
                ;;
        esac
    done

    # Read positional arguments
    _ap_read_pos_args "$@"

    # Debug messages
    _ap_print_debug_msgs "$args"

    return 0
}

ap_reset_args
true
