# vi: ft=bash
## @title Execution assertions
##
## Assertions testing the execution of a command.

## Runs a command n times, and expect one success.
##
## Tries no more than n times to execute a command up to success.
##
## @arg n int [>=0] The number of tries.
## @arg cmd str ... The command and its arguments.
## @stderr A message is printed in case of failure of the command.
## @status Returns 0 in case of success, 1 otherwise.
function tt_expect_success_in_n_tries {

    local n=$1
    shift
    local cmd="$*"

    # Try to run the command
    for ((i = 0 ; i < n ; ++i)) ; do
        ( "$@" >&2 )
        err=$?
        [[ $err == 0 ]] && break
    done

    # Failure
    if [[ $err -gt 0 ]] ; then
        rt_print_call_stack >&2
        echo "Command \"$cmd\" failed $n times." >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests the value of `$?`
##
## Checks the status of last command (`$?`). Returns success if `$?` is
## non-zero, failure otherwise.
##
## @stderr An error message is printed in case `$?` is zero.
## @status Returns 1 if `$?` is 0, 1 otherwise.
function tt_expect_failure_last_cmd {

    local -r status=$?

    if [[ $status -eq 0 ]] ; then
        rt_print_call_stack >&2
        echo "Last command was successful while waiting for failure." >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests the value of `$?`
##
## Checks the status of last command (`$?`). Returns success if `$?` is zero,
## failure otherwise.
##
## @stderr An error message is printed in case `$?` is non-zero.
## @status Returns 0 if `$?` is 0, 1 otherwise.
function tt_expect_success_last_cmd {

    local -r status=$?

    if [[ $status -gt 0 ]] ; then
        rt_print_call_stack >&2
        echo "Last command failed with status $status." >&2
        return 1
    fi

    echo -n .

    return 0
}

## Tests if a command succeeds.
##
## Runs the passed command with its arguments and checks its returned status.
## If the status is 0 returns 0, otherwise returns 1.
##
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command fails.
## @status Returns 0 if the command succeeds and 1 otherwise.
function tt_expect_success {

    local cmd="$*"

    ( "$@" >&2 )
    local status=$?

    if [[ $status -gt 0 ]] ; then
        rt_print_call_stack >&2
        echo "Command \"$cmd\" failed with status $status." >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a command fails.
##
## Runs the passed command with its arguments and checks its returned status.
## If the status is non-zero returns 0, otherwise returns 1.
##
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command succeeds.
## @status Returns 0 if the command fails and 1 otherwise.
function tt_expect_failure {

    local cmd="$*"

    ( "$@" >&2 )

    # shellcheck disable=SC2181
    if [ $? -eq 0 ] ; then
        rt_print_call_stack >&2
        echo "Command \"$cmd\" was successful while expecting failure." >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a command returns a specific status.
##
## Runs the passed command with its arguments and checks its returned status.
## If the status is the expected status returns 0, otherwise returns 1.
##
## @arg cmd str ... The command and its arguments.
## @stderr An error message is printed in case the command returns an unexpected
## status.
## @status Returns 0 if the command returns the expected status and 1 otherwise.
function tt_expect_status {

    local expected_status="$1"
    shift
    local cmd="$*"

    ( "$@" >&2 )
    local actual_status=$?

    if [[ $actual_status -ne $expected_status ]] ; then
        rt_print_call_stack >&2
        echo "Command \"$cmd\" failed with status $actual_status, but " \
            "expected status $expected_status." >&2
        return 1
    fi

    echo -n .
    return 0
}

function _tt_expect_output_nlines_op {

    declare -ir fd="$1"
    declare -r op="$2"
    declare -ir expected_nb_lines="$3"
    shift 3
    declare cmd="$*"
    declare tmpfile
    fs_tmp_acquire_file "$PROGNAME.nlines" || \
        lg_fatal "Failed creating temporary file."
    tmpfile="$(fs_tmp_last_path)"

    # Run command
    declare -i status=
    case $fd in
        1) ( "$@" >"$tmpfile" ) ; status=$? ;;
        2) ( "$@" 2>"$tmpfile" ) ; status=$? ;;
        *) echo -e "Unknown file descriptor \"$fd\"." >&2 ; return 10 ;;
    esac

    # Get number of lines
    declare -i nlines
    nlines=$(awk 'END { print NR }' "$tmpfile") || \
        lg_fatal "Failed running awk on \"$tmpfile\"."
    fs_tmp_release_last

    # Check command status
    if [[ $status -ne 0 ]] ; then
        rt_print_call_stack >&2
        echo "Command \"$cmd\" failed with status $status." >&2
        return 1
    fi

    # Test number of lines
    case $op in
        eq|ne|ge|gt|le|lt)
            if ! test "$nlines" "-$op" "$expected_nb_lines" ; then
                rt_print_call_stack >&2
                echo "Output of \"$cmd\" contains $nlines lines." \
                    "Expression \"$nlines -$op $expected_nb_lines\" is false." \
                    >&2
                return 2
            fi
            ;;
        *) echo -e "Unknown comparison operator \"$op\"." >&2 ; return 20 ;;
    esac

    echo -n .
    return 0
}
