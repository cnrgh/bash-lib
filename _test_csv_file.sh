# vi: ft=bash
## @title CSV file assertions
##
## Assertions testing the content of CSV files or comparing CSV files.

## Tests if a CSV contains certain column names.
##
## Reads the CSV header of a file, and checks that it contains all column
## names passed as argument.
##
## @arg file path The path to a valid CSV file.
## @arg sep str The column separator used inside the CSV file.
## @arg cols str A space separated list of column names.
## @stderr Prints an error message if the test fails.
## @status Returns 0 if the test succeeds, 1 otherwise. 
function tt_expect_csv_has_columns {

    local file=$1
    local sep=$2
    local expected_cols=$3

    # Get columns
    cols=$(tf_csv_get_col_names "$file $sep" 0 1)

    # Loop on all expected columns
    for c in $expected_cols ; do
        if [[ " $cols " != *" $c "* && " $cols " != *" \"$c\" "* ]] ; then
            rt_print_call_stack >&2
            echo "Column \"$c\" cannot be found inside columns of file \"$file\"." >&2
            echo "Columns of file \"$file\" are: $cols." >&2
            return 1
        fi
    done

    echo -n .
    return 0
}

## Tests if a CSV does not contain certain column names.
##
## Reads the CSV header of a file, and checks that it does not contain
## any column name passed as argument.
##
## @arg file path The path to a valid CSV file.
## @arg sep str The column separator used inside the CSV file.
## @arg cols str A space separated list of column names.
## @stderr Prints an error message if the test fails.
## @status Returns 0 if the test succeeds, 1 otherwise. 
function tt_expect_csv_not_has_columns {

    local file=$1
    local sep=$2
    local expected_cols=$3

    # Get columns
    cols=$(tf_csv_get_col_names "$file $sep" 0 1)

    # Loop on all expected columns
    for c in $expected_cols ; do
        if [[ " $cols " == *" $c "* || " $cols " == *" \"$c\" "* ]] ; then
            rt_print_call_stack >&2
            echo "Column \"$c\" has been found inside columns of file \"$file\"." >&2
            echo "Columns of file \"$file\" are: $cols." >&2
            return 1
        fi
    done

    echo -n .
    return 0
}

## Tests if two CSV files have both a column with the same name.
##
## Reads both CSV files, checks that they both define the same column name.
##
## @arg col str The column name.
## @arg file1 path The path to a valid CSV file.
## @arg file2 path The path to another valid CSV file.
## @arg sep str The column separator used inside the CSV file.
## @status Returns 0 if the test was successful, 1 otherwise.
function tt_expect_csv_same_col_names {

    local file1=$1
    local file2=$2
    local sep=$3
    local nbcols=$4
    local remove_quotes=$5

    cols1=$(tf_csv_get_col_names "$file1" "$sep" "$nbcols" "$remove_quotes")
    cols2=$(tf_csv_get_col_names "$file2" "$sep" "$nbcols" "$remove_quotes")
    if [[ $cols1 != "$cols2" ]] ; then
        rt_print_call_stack >&2
        echo "Column names of files \"$file1\" and \"$file2\" are different." >&2
        [[ -n $nbcols ]] && echo "Comparison on the first $nbcols columns only." >&2
        echo "Columns of file \"$file1\" are: $cols1." >&2
        echo "Columns of file \"$file2\" are: $cols2." >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if two CSV files contain the same column.
##
## Reads both CSV files, checks that they both define the same column name and
## checks that the both columns contain the exact same values.
##
## @arg col str The column name.
## @arg file1 path The path to a valid CSV file.
## @arg file2 path The path to another valid CSV file.
## @arg sep str The column separator used inside the CSV file.
## @status Returns 0 if the test was successful, 1 otherwise.
function tt_expect_csv_identical_col_values {

    local col=$1
    local file1=$2
    local file2=$3
    local sep=$4

    col1=$(tf_csv_get_col_index "$file1" "$sep" "$col")
    tt_expect_num_gt "$col1" 0 "\"$file1\" does not contain column $col."
    col2=$(tf_csv_get_col_index "$file2" "$sep" "$col")
    tt_expect_num_gt "$col2" 0 "\"$file2\" does not contain column $col."
    ncols_file1=$(tf_csv_get_nb_cols "$file1" "$sep")
    ((col2 = col2 + ncols_file1))
    ident=$(paste "$file1" "$file2" | \
        awk 'BEGIN{FS="'"$sep"'";eq=1}{if ($'"$col1"' != $'"$col2"') {eq=0}}END{print eq}')
    if [[ $ident -ne 1 ]] ; then
        rt_print_call_stack >&2
        echo "Files \"$file1\" and \"$file2\" do not have the same values in column \"$col\"." >&2
        return 1
    fi

    echo -n .
    return 0
}

## Tests if a CSV file contains float values close to a reference value.
##
## Reads the CSV file, checks that it defines the column name and
## checks that the column contains only float values close to a reference value
## using a tolerance.
##
## @arg file path The path to a valid CSV file.
## @arg sep str The column separator used inside the CSV file.
## @arg col str The column name.
## @arg val float The reference float value.
## @arg tol float The float tolerance.
## @status Returns 0 if the test was successful, 1 otherwise.
function tt_expect_csv_float_col_equals {

    local file=$1
    local sep=$2
    local col=$3
    local val=$4
    local tol=$5

    col_index=$(tf_csv_get_col_index "$file" "$sep" "$col")
    ident=$(awk 'function abs(v) { return v < 0 ? -v : v }BEGIN{FS="'"$sep"'";eq=1}{if (NR > 1 && abs($'"$col_index"' - '"$val"') >'"$tol"') {eq=0}}END{print eq}' "$file")

    [[ $ident -eq 1 ]] || return 1

    echo -n .
    return 0
}
