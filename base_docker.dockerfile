ARG DOCKER_BASH_VERSION=5.2
FROM bash:${DOCKER_BASH_VERSION}

# Install dependencies
RUN apk add coreutils make shellcheck pipx tidyhtml npm
ENV PIPX_HOME=/opt/pipx
ENV PIPX_BIN_DIR=/usr/local/bin
RUN pipx install shyaml
