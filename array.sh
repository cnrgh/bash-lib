# vi: ft=bash
# shellcheck disable=SC1091
## @title Array module
##
## Functions for working on lists of elements.
##
## The name/prefix of this module is AR (ARray):
##  * `ar_`  for the public functions.
##  * `AR_`  for the public global variables or constants.
##  * `_ar_` for the private functions.
##  * `_AR_` for the private global variables or constants.

# Include guard
if [[ -z $_BASH_LIB_EMBEDDED ]] ; then
    [[ -z $_AR_SOURCED ]] || return 0
    _AR_SOURCED=1
fi

## Finds the index of a value.
##
## Searches for the value inside all the passed arguments, and returns its index
## on stdout.
##
## @arg value str The value to search for.
## @arg list+ str ... The values of the list.
## @status Returns 0.
## @stdout int The index of the found value, if it was found. Otherwise returns
## -1.
function ar_index {

    local -r value="$1"
    shift

    local index=-1
    local i=0
    for v in "$@" ; do
        if [[ "$v" == "$value" ]] ; then
            index=$i
            break
        fi
        ((++i))
    done

    echo "$index"
    return 0
}

## Tests if a value is inside a list of values.
##
## Searches for a value inside all the passed arguments.
##
## @arg value str The value to search for.
## @arg list+ str ... The values of the list.
## @status Returns 0 if the value is found, 1 otherwise.
function ar_contains {

    local -r value="$1"
    shift

    for v in "$@" ; do
        [[ "$v" == "$value" ]] && return 0
    done

    return 1
}

## Tests if a list of values is empty.
##
## Returns 1 if any argument is passed.
##
## @arg list+ str ... Arguments.
## @status Returns 0 if no argument is passed, 1 otherwise.
## @examples
## declare -a x=(a b c)
## if ar_is_empty "${x[@]}" ; then
##     echo "Array is empty."
## else
##     echo "Array is not empty."
## fi
function ar_is_empty {

    [[ $# -eq 0 ]] && return 0

    return 1
}

## Moves an element to first position.
##
## Searches for a value inside a list and moves that value to the first
## position.
##
## @arg value str The value to search and move.
## @arg list+ str ... A list of values.
## @stdout Returns the elements separated by spaces, with the wanted value at
## first positon if it exists.
## @status Returns 0.
## @examples
## declare -a x=(a b c)
## ar_move_first b "${x[@]}"
function ar_move_first {

    local -r value="$1"
    shift

    local sep=""
    ar_contains "$value" "$@" && echo -n "$value" && sep="${IFS:0:1}"
    for x in "$@" ; do
        [[ $x != "$value" ]] && echo -n "$sep$x" && sep="${IFS:0:1}"
    done

    return 0
}

## Removes an element from a list.
##
## Searches for a value inside a list and removes all its occurrences.
##
## @arg value str The value to search and remove.
## @arg list+ str ... A list of values.
## @stdout Returns the elements separated by spaces, with the wanted value 
## removed.
## @examples
## declare -a x=(a b c)
## ar_remove b "${x[@]}"
function ar_remove {

    local -r value="$1"
    shift

    local sep=""
    for x in "$@" ; do
        [[ $x != "$value" ]] && echo -n "$sep$x" && sep="${IFS:0:1}"
    done

    return 0
}

## Sorts a list of values.
##
## Sorts all arguments in ASCII order.
##
## @arg list+ str ... A list of values.
## @stdout Returns the sorted elements separated by spaces.
## @examples
## declare -a x=(c b a)
## ar_sort "${x[@]}"
function ar_sort {
    printf '%s\n' "$@" | sort | tr "\n" " " | sed 's/ $//'
    return 0
}

true
